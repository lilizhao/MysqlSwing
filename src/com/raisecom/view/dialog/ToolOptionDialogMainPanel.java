package com.raisecom.view.dialog;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

import com.raisecom.model.menu.MenuDataModel;
import com.raisecom.util.BorderUtils;
import com.raisecom.util.SwingConstants;
import com.raisecom.view.custom.CheckListItem;
import com.raisecom.view.custom.JAutoCompleteComboBox;
import com.raisecom.view.custom.JCheckList;
import com.raisecom.view.custom.MultilineLabel;

public class ToolOptionDialogMainPanel extends JPanel{

	private static final long serialVersionUID = -3304270034713466707L;
	
	private ToolOptionDialog toolOptionDialog;
	
	//界面panel部分
	private CardLayout cardLayout;
	private JPanel centerPanel;
	private JPanel southPanel;
	
	private JTree optionTree;
	private JPanel optionConfigPanel;
	
	//常规面板界面组件
	private JCheckBox reducedToTaskBarCheckBox;
	private JCheckBox allowsRepeatedOpenFormCheckBox;
	private JCheckBox allowsRepeatRunNavicatCheckBox;
	private JCheckBox clickRefreshCheckBox;
	private JCheckBox displayFunctionWizardCheckBox;
	private JCheckBox closeWhenPromptedToSaveCheckBox;
	
	//停靠面板界面组件
	private JCheckBox userDockCheckBox;
	private JCheckBox dockedOpenWindowsCheckBox;
	private JRadioButton toMainWindowRadioButton;
	private JRadioButton toDockWindowRadioButton;
	private JCheckBox followLastDockedStyleCheckBox;
	
	
	//代码附加选项
	private JCheckBox useAutoCompleteCodeCheckBox;
	private JLabel useAutoCompleteCodeDelayLabel;
	private JSlider useAutoCompleteCodeSlider;
	
	private JCheckBox useAutoCompleteWordCheckBox;
	private JLabel useAutoCompleteWordDelayLabel;
	private JSlider useAutoCompleteWordSlider;
	
	private JCheckBox useSyntaxHighlightCheckBox;
	private JLabel useSyntaxHighlightLimitLabel;
	private JTextField useSyntaxHighlightTextField;
	
	//自动保存面板组件
	private JCheckBox useAutoSaveCheckBox;
	private JLabel autoSaveIntervalLabel;
	private JTextField autoSaveIntervalTextField;
	
	//外观界面组件
	private JCheckBox showToolbarTitleCheckBox;
	
	//字体界面组件
	private JLabel gridFontLabel;
	private JAutoCompleteComboBox gridFontComboBox;
	private JAutoCompleteComboBox gridFontSizeComboBox;
	private JLabel editorFontLabel;
	private JAutoCompleteComboBox editorFontComboBox;
	private JAutoCompleteComboBox editorFontSizeComboBox;
	private JLabel commandLineFontLabel;
	private JAutoCompleteComboBox commandLineFontComboBox;
	private JAutoCompleteComboBox commandLineFontSizeComboBox;
	
	//颜色界面组件
	private JLabel color1Label;
	private JTextField color1TextField;
	private JCheckBox useThreeColorCheckBox;
	private JLabel color2Label;
	private JTextField color2TextField;
	private JLabel color3Label;
	private JTextField color3TextField;
	private JLabel titleColorLabel;
	private JTextField titleColorTextField;
	private JLabel borderColorLabel;
	private JTextField borderColorTextField;
	private JButton defaultGridButton;
	
	private JLabel commonColorLabel;
	private JTextField commonColorTextField;
	private JLabel keyWordLabel;
	private JTextField keyWordTextField;
	private JLabel notationLabel;
	private JTextField notationTextField;
	private JLabel StringLabel;
	private JTextField stringTextField;
	private JLabel digitalLabel;
	private JTextField digitalTextField;
	private JButton defaultWordButton;
	
	//主窗口组件部分
	private JCheckBox displayTableHintsCheckBox;
	private JCheckBox displayObjectConnectionTreeCheckBox;
	private JCheckBox useCustomConnectionSortCheckBox;
	
	//编辑器组件部分
	private JCheckBox displayLineNumbersCheckBox;
	private JCheckBox useCodeFoldCheckBox;
	private JCheckBox useParenthesesToHighlightCheckBox;
	private JLabel positionWidthLabel;
	private JTextField positionWidthTextField;
	
	
	//数据&网格组件部分
	private JCheckBox primaryKeyWarnCheckBox;
	private JCheckBox displayGridTextBlobdataCheckBox;
	private JCheckBox limitRecordCheckBox;
	private JTextField recordPerPageTextField;
	private JLabel recordPerPageLabel;
	private JTextField editorForeignKeyTextField;
	private JLabel editorForeignKeyLabel;
	private JCheckBox synchCurrentRecordCheckBox;
	private JLabel rowHeightLabel;
	private JTextField rowHeightTextField;
	private JLabel columnWidthLabel;
	private JTextField columnWidthTextField;
	
	//显示格式界面组件
	private JLabel integerLabel;
	private JTextField integerTextField;
	private JLabel floatLabel;
	private JTextField floatTextField;
	private JLabel dateLabel;
	private JTextField dateTextField;
	private JLabel timeLabel;
	private JTextField timeTextField;
	private JLabel timeDateLabel;
	private JTextField timeDateTextField;
	private JLabel exampleLabel;
	private JLabel exportLabel;
	
	//模型界面组件
	private JCheckBox highlightObjectsCheckBox;
	private JCheckBox highlightRelationshipCheckBox;
	private JCheckBox guessTypeFieldCheckBox;
	
	//其他界面组件
	private JSlider processPrioritySlider;
	private JLabel noticeLabel;
	private JLabel pathLogFileSaveLabel;
	private JTextField pathLogFileSaveTextField;
	private JButton pathLogFileSaveButton;
	private JLabel setFileSavePathLabel;
	private JTextField setFileSavePathTextField;
	private JButton setFileSavePathButton;
	
	//文件关联部分
	private MultilineLabel fileAssactionLabel;
	@SuppressWarnings("rawtypes")
	private JCheckList fileAssactionList;
	private JButton allSelectButton;
	private JButton allDiselectButton;
	
	
	//southPanel组件部分
	private JButton okButton;
	private JButton cancelButton;

	public ToolOptionDialogMainPanel(ToolOptionDialog toolOptionDialog) {
		this.toolOptionDialog = toolOptionDialog;
		
		initCenterPanel();
		initSouthPanel();
		addComponentPanel();
		initListeners();
	}

	/**
	 * 功能描述：初始化选项配置界面
	 */
	private void initCenterPanel() {
		centerPanel = new JPanel();
		centerPanel.setLayout(new GridBagLayout());
		
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("root");
		optionTree = new JTree(root);
		
		initTree(optionTree,root);
		
		JScrollPane optionScrollPane = new JScrollPane(optionTree,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		optionScrollPane.setSize(new Dimension(300, 450));
		
		optionConfigPanel = new JPanel();
		initOptionConfigPanel(optionConfigPanel);
		
		centerPanel.add(optionScrollPane,new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
				new Insets(15, 10, 10, 5), 0, 0));
		centerPanel.add(optionConfigPanel,new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));
		
	}
	
	/**
	 * 初始化选项配置面板
	 * @param optionConfigPanel2 
	 */
	@SuppressWarnings("rawtypes")
	private void initOptionConfigPanel(JPanel optionConfigPanel) {
		cardLayout = new CardLayout(5,5);
		optionConfigPanel.setLayout(cardLayout);
		
		JPanel commonPanel = new JPanel();
		JPanel dockPanel = new JPanel();
		JPanel additionalOptionPanel = new JPanel();
		JPanel autoSavePanel = new JPanel();
		JPanel outwardPanel = new JPanel();
		JPanel fontPanel = new JPanel();
		JPanel colorPanel = new JPanel();
		JPanel mainWindowPanel = new JPanel();
		JPanel editorPanel = new JPanel();
		JPanel dataGridPanel = new JPanel();
		JPanel displayFormatPanel = new JPanel();
		JPanel modelPanel = new JPanel();
		JPanel otherPanel = new JPanel();
		JPanel fileAssociationPanel = new JPanel();
		
		
		//绘制常规面板界面组件
		commonPanel.setBorder(BorderUtils.createEmsBorder("常规"));
		commonPanel.setLayout(new BorderLayout());
		
		JPanel northCommonPanel = new JPanel();
		northCommonPanel.setLayout(new GridBagLayout());
		
		reducedToTaskBarCheckBox = new JCheckBox("缩小到任务栏",true);
		allowsRepeatedOpenFormCheckBox = new JCheckBox("允许重复打开表单",false);
		allowsRepeatRunNavicatCheckBox = new JCheckBox("允许重复运行Navicat",false);
		clickRefreshCheckBox = new JCheckBox("点击时刷新",true);
		displayFunctionWizardCheckBox = new JCheckBox("显示函数向导",true);
		closeWhenPromptedToSaveCheckBox = new JCheckBox("在关闭前提示保存新建的查询或设置文件",false);
		
		northCommonPanel.add(reducedToTaskBarCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northCommonPanel.add(allowsRepeatedOpenFormCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(4, 5, 0, 5), 0, 0));
		northCommonPanel.add(allowsRepeatRunNavicatCheckBox,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(4, 5, 0, 5), 0, 0));
		northCommonPanel.add(clickRefreshCheckBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(4, 5, 0, 5), 0, 0));
		northCommonPanel.add(displayFunctionWizardCheckBox,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(4, 5, 0, 5), 0, 0));
		northCommonPanel.add(closeWhenPromptedToSaveCheckBox,new GridBagConstraints(0, 5, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(4, 5, 0, 5), 0, 0));
		
		commonPanel.add(northCommonPanel,BorderLayout.NORTH);
		
		//绘制停靠面板组件
		dockPanel.setLayout(new BorderLayout());
		dockPanel.setBorder(BorderUtils.createEmsBorder("停靠"));
		
		userDockCheckBox = new JCheckBox("使用停靠",true);
		dockedOpenWindowsCheckBox = new JCheckBox("停靠打开的窗口",true);
		toMainWindowRadioButton = new JRadioButton("到主窗口",false);
		toDockWindowRadioButton = new JRadioButton("到停靠窗口",true);
		followLastDockedStyleCheckBox = new JCheckBox("遵循最后的停靠样式",false);
		
		ButtonGroup dockGroup = new ButtonGroup();
		dockGroup.add(toMainWindowRadioButton);
		dockGroup.add(toDockWindowRadioButton);
		
		JPanel northDockPanel = new JPanel();
		northDockPanel.setLayout(new GridBagLayout());

		northDockPanel.add(userDockCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northDockPanel.add(dockedOpenWindowsCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 20, 0, 5), 0, 0));
		northDockPanel.add(toMainWindowRadioButton,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 35, 0, 5), 0, 0));
		northDockPanel.add(toDockWindowRadioButton,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 35, 0, 5), 0, 0));
		northDockPanel.add(followLastDockedStyleCheckBox,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 20, 0, 5), 0, 0));
		
		dockPanel.add(northDockPanel,BorderLayout.NORTH);
		
		//绘制代码附加选项界面组件
		additionalOptionPanel.setLayout(new GridLayout(3, 1));
		
		JPanel autoCompleteCodePanel = new JPanel();
		JPanel autoCompleteWordPanel = new JPanel();
		JPanel syntaxHighlightPanel = new JPanel();
		
		autoCompleteCodePanel.setLayout(new GridBagLayout());
		autoCompleteCodePanel.setBorder(BorderUtils.createEmsBorder("自动完成代码"));
		
		useAutoCompleteCodeCheckBox = new JCheckBox("使用自动完成代码",true);
		useAutoCompleteCodeDelayLabel = new JLabel("延迟");
		useAutoCompleteCodeSlider = new JSlider(0, 100, 0);
		useAutoCompleteCodeSlider.setMajorTickSpacing(10);
		useAutoCompleteCodeSlider.setMinorTickSpacing(5);
		useAutoCompleteCodeSlider.setPaintTicks(true);
		
		Hashtable<Integer, JLabel> useAutoCompleteCodeLabelTable = new Hashtable<Integer, JLabel>();
		for (int i = 0; i <= 100; i += 100) {
			useAutoCompleteCodeLabelTable.put(new Integer(i),new JLabel(String.valueOf(i / 100.0)));
		}
		
		useAutoCompleteCodeSlider.setLabelTable(useAutoCompleteCodeLabelTable);
		useAutoCompleteCodeSlider.setPaintLabels(true);
		
		autoCompleteCodePanel.add(useAutoCompleteCodeCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		autoCompleteCodePanel.add(useAutoCompleteCodeDelayLabel,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 25, 0, 5), 0, 0));
		autoCompleteCodePanel.add(useAutoCompleteCodeSlider,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 25, 0, 5), 0, 0));
		
		
		autoCompleteWordPanel.setLayout(new GridBagLayout());
		autoCompleteWordPanel.setBorder(BorderUtils.createEmsBorder("自动完成单词"));
		
		useAutoCompleteWordCheckBox = new JCheckBox("使用自动完成单词",true);
		useAutoCompleteWordDelayLabel = new JLabel("延迟");
		useAutoCompleteWordSlider = new JSlider(0, 100, 0);
		useAutoCompleteWordSlider.setMajorTickSpacing(10);
		useAutoCompleteWordSlider.setMinorTickSpacing(5);
		useAutoCompleteWordSlider.setPaintTicks(true);
		
		Hashtable<Integer, JLabel> useAutoCompleteWordLabelTable = new Hashtable<Integer, JLabel>();
		for (int i = 0; i <= 100; i += 100) {
			useAutoCompleteWordLabelTable.put(new Integer(i),new JLabel(String.valueOf(i / 100.0)));
		}
		
		useAutoCompleteWordSlider.setLabelTable(useAutoCompleteWordLabelTable);
		useAutoCompleteWordSlider.setPaintLabels(true);
		
		autoCompleteWordPanel.add(useAutoCompleteWordCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		autoCompleteWordPanel.add(useAutoCompleteWordDelayLabel,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 25, 0, 5), 0, 0));
		autoCompleteWordPanel.add(useAutoCompleteWordSlider,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 25, 0, 5), 0, 0));
		
		syntaxHighlightPanel.setLayout(new GridBagLayout());
		syntaxHighlightPanel.setBorder(BorderUtils.createEmsBorder("语法高亮显示"));
		
		useSyntaxHighlightCheckBox = new JCheckBox("使用语法高亮显示",true);
		useSyntaxHighlightLimitLabel = new JLabel("应用语法高亮显示当语句大小小于  (MB):");
		useSyntaxHighlightTextField = new JTextField("10", 15);
		
		syntaxHighlightPanel.add(useSyntaxHighlightCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		syntaxHighlightPanel.add(useSyntaxHighlightLimitLabel,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 25, 0, 5), 0, 0));
		syntaxHighlightPanel.add(useSyntaxHighlightTextField,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 25, 0, 5), 0, 0));
		
		additionalOptionPanel.add(autoCompleteCodePanel);
		additionalOptionPanel.add(autoCompleteWordPanel);
		additionalOptionPanel.add(syntaxHighlightPanel);
		
		//绘制自动保存界面组件
		autoSavePanel.setLayout(new BorderLayout());
		autoSavePanel.setBorder(BorderUtils.createEmsBorder("自动保存"));
		
		JPanel northAutoSavePanel = new JPanel();
		northAutoSavePanel.setLayout(new GridBagLayout());
		
		useAutoSaveCheckBox = new JCheckBox("使用自动保存",true);
		autoSaveIntervalLabel = new JLabel("自动保存间隔 (s):");
		autoSaveIntervalTextField = new JTextField("30",20);
		
		northAutoSavePanel.add(useAutoSaveCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northAutoSavePanel.add(autoSaveIntervalLabel,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 25, 0, 0), 0, 0));
		northAutoSavePanel.add(autoSaveIntervalTextField,new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 0, 0, 5), 0, 0));
		
		autoSavePanel.add(northAutoSavePanel,BorderLayout.NORTH);
		
		
		//绘制外观界面组件
		outwardPanel.setLayout(new BorderLayout());
		outwardPanel.setBorder(BorderUtils.createEmsBorder("常规"));
		
		JPanel northOutwardPanel = new JPanel();
		northOutwardPanel.setLayout(new GridBagLayout());
		
		showToolbarTitleCheckBox = new JCheckBox("显示工具栏标题",true);
		
		northOutwardPanel.add(showToolbarTitleCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		
		
		outwardPanel.add(northOutwardPanel,BorderLayout.NORTH);
		
		//绘制字体界面组件
		fontPanel.setLayout(new BorderLayout());
		fontPanel.setBorder(BorderUtils.createEmsBorder("字体"));
		
		JPanel northFontPanel = new JPanel();
		northFontPanel.setLayout(new GridBagLayout());
		
		Vector<String> allSystemFont = SwingConstants.getAllSystemFont();
		Vector<String> allSystemFontSize = SwingConstants.getAllSystemFontSize();
		
		gridFontLabel = new JLabel("网格字体:");
		gridFontComboBox = new JAutoCompleteComboBox(allSystemFont);
		gridFontComboBox.setSelectedItem("宋体");
		gridFontComboBox.setPrototypeDisplayValue("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		gridFontSizeComboBox = new JAutoCompleteComboBox(allSystemFontSize);
		gridFontSizeComboBox.setSelectedItem("9");
		editorFontLabel = new JLabel("编辑器字体:");
		editorFontComboBox = new JAutoCompleteComboBox(allSystemFont);
		editorFontComboBox.setSelectedItem("Courier New");
		editorFontComboBox.setPrototypeDisplayValue("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		editorFontSizeComboBox = new JAutoCompleteComboBox(allSystemFontSize);
		editorFontSizeComboBox.setSelectedItem("10");
		commandLineFontLabel = new JLabel("命令行字体:");
		commandLineFontComboBox = new JAutoCompleteComboBox(allSystemFont);
		commandLineFontComboBox.setSelectedItem("Courier New");
		commandLineFontComboBox.setPrototypeDisplayValue("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		commandLineFontSizeComboBox = new JAutoCompleteComboBox(allSystemFontSize);
		commandLineFontSizeComboBox.setSelectedItem("10");
		
		northFontPanel.add(gridFontLabel,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northFontPanel.add(gridFontComboBox,new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northFontPanel.add(gridFontSizeComboBox,new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 25), 0, 0));
		northFontPanel.add(editorFontLabel,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northFontPanel.add(editorFontComboBox,new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northFontPanel.add(editorFontSizeComboBox,new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 25), 0, 0));
		northFontPanel.add(commandLineFontLabel,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northFontPanel.add(commandLineFontComboBox,new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northFontPanel.add(commandLineFontSizeComboBox,new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 25), 0, 0));
		
		fontPanel.add(northFontPanel,BorderLayout.NORTH);
		
		colorPanel.setLayout(new GridLayout(2,1));
		
		JPanel upperColorPanel = new JPanel();
		JPanel lowerColorPanel = new JPanel();
		
		upperColorPanel.setLayout(new GridBagLayout());
		upperColorPanel.setBorder(BorderUtils.createEmsBorder("网格颜色"));
		
		JPanel color1Panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel color2Panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel color3Panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel titleColorPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel borderColorPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel defaultGridPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		color1Label = new JLabel("颜色 1:  ");
		color1TextField = new JTextField(3);
		color1TextField.setEditable(false);
		color1TextField.setBackground(new Color(255,255,255));
		useThreeColorCheckBox = new JCheckBox("使用三种颜色",true);
		
		color1Panel.add(color1Label);
		color1Panel.add(color1TextField);
		color1Panel.add(useThreeColorCheckBox);
		
		color2Label = new JLabel("颜色 2:  ");
		color2TextField = new JTextField(3);
		color2TextField.setEditable(false);
		color2TextField.setBackground(new Color(238,238,238));
		
		color2Panel.add(color2Label);
		color2Panel.add(color2TextField);
		
		color3Label = new JLabel("颜色 3:  ");
		color3TextField = new JTextField(3);
		color3TextField.setEditable(false);
		color3TextField.setBackground(new Color(238,246,255));
		
		color3Panel.add(color3Label);
		color3Panel.add(color3TextField);
		
		titleColorLabel = new JLabel("标题颜色:");
		titleColorTextField = new JTextField(3);
		titleColorTextField.setEditable(false);
		titleColorTextField.setBackground(new Color(185,182,165));
		
		titleColorPanel.add(titleColorLabel);
		titleColorPanel.add(titleColorTextField);
		
		borderColorLabel = new JLabel("边界颜色:");
		borderColorTextField = new JTextField(3);
		borderColorTextField.setEditable(false);
		borderColorTextField.setBackground(new Color(0,0,0));
		
		borderColorPanel.add(borderColorLabel);
		borderColorPanel.add(borderColorTextField);
		
		defaultGridButton = new JButton("默认");
		
		defaultGridPanel.add(defaultGridButton);
		
		
		upperColorPanel.add(color1Panel,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		upperColorPanel.add(color2Panel,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		upperColorPanel.add(color3Panel,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		upperColorPanel.add(titleColorPanel,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		upperColorPanel.add(borderColorPanel,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		upperColorPanel.add(defaultGridPanel,new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		
		lowerColorPanel.setLayout(new GridBagLayout());
		lowerColorPanel.setBorder(BorderUtils.createEmsBorder("文字颜色"));
		
		JPanel commonColorPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel keyWordPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel notationPanel = new JPanel(new FlowLayout(FlowLayout.LEFT)); 
		JPanel stringPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel digitalPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel defaultWordPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		commonColorLabel = new JLabel("常规:    ");
		commonColorTextField = new JTextField(3);
		commonColorTextField.setEditable(false);
		commonColorTextField.setBackground(new Color(0,0,0));
		
		commonColorPanel.add(commonColorLabel);
		commonColorPanel.add(commonColorTextField);
		
		keyWordLabel = new JLabel("关键字:  ");
		keyWordTextField =new JTextField(3);
		keyWordTextField.setEditable(false);
		keyWordTextField.setBackground(new Color(0,0,255));
		
		keyWordPanel.add(keyWordLabel);
		keyWordPanel.add(keyWordTextField);
		
		notationLabel = new JLabel("注释:    ");
		notationTextField = new JTextField(3);
		notationTextField.setEditable(false);
		notationTextField.setBackground(new Color(192,192,192));
		
		notationPanel.add(notationLabel);
		notationPanel.add(notationTextField);
		
		StringLabel = new JLabel("字符串:  ");
		stringTextField = new JTextField(3);
		stringTextField.setEditable(false);
		stringTextField.setBackground(new Color(128,0,0));
		
		stringPanel.add(StringLabel);
		stringPanel.add(stringTextField);
		
		digitalLabel = new JLabel("数字:    ");
		digitalTextField = new JTextField(3);
		digitalTextField.setEditable(false);
		digitalTextField.setBackground(new Color(0,128,0));
		
		digitalPanel.add(digitalLabel);
		digitalPanel.add(digitalTextField);
		
		defaultWordButton = new JButton("默认");
		
		defaultWordPanel.add(defaultWordButton);
		
		lowerColorPanel.add(commonColorPanel,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		lowerColorPanel.add(keyWordPanel,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		lowerColorPanel.add(notationPanel,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		lowerColorPanel.add(stringPanel,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		lowerColorPanel.add(digitalPanel,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		lowerColorPanel.add(defaultWordPanel,new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		
		
		colorPanel.add(upperColorPanel);
		colorPanel.add(lowerColorPanel);
		
		
		//主窗口部分
		mainWindowPanel.setLayout(new BorderLayout());
		mainWindowPanel.setBorder(BorderUtils.createEmsBorder("常规"));
		
		JPanel northMainWindowPanel = new JPanel();
		northMainWindowPanel.setLayout(new GridBagLayout());
		
		displayTableHintsCheckBox = new JCheckBox("显示表提示",false);
		displayObjectConnectionTreeCheckBox = new JCheckBox("在连接树中显示对象",true);
		useCustomConnectionSortCheckBox = new JCheckBox("使用自定义连接排序",false);
		
		northMainWindowPanel.add(displayTableHintsCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northMainWindowPanel.add(displayObjectConnectionTreeCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northMainWindowPanel.add(useCustomConnectionSortCheckBox,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		
		
		mainWindowPanel.add(northMainWindowPanel,BorderLayout.NORTH);
		
		
		//编辑器界面部分
		editorPanel.setLayout(new BorderLayout());
		editorPanel.setBorder(BorderUtils.createEmsBorder("编辑器"));
		
		JPanel northEditorPanel = new JPanel();
		northEditorPanel.setLayout(new GridBagLayout());
		
		displayLineNumbersCheckBox = new JCheckBox("显示行号",true);
		useCodeFoldCheckBox = new JCheckBox("使用代码折叠",true);
		useParenthesesToHighlightCheckBox = new JCheckBox("使用括号高亮显示",true);
		
		JPanel positionWidthPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		positionWidthLabel = new JLabel("定位宽度                   ");
		positionWidthTextField = new JTextField("2",15);
		
		positionWidthPanel.add(positionWidthLabel);
		positionWidthPanel.add(positionWidthTextField);
		
		northEditorPanel.add(displayLineNumbersCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northEditorPanel.add(useCodeFoldCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northEditorPanel.add(useParenthesesToHighlightCheckBox,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		northEditorPanel.add(positionWidthPanel,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(4, 5, 0, 5), 0, 0));
		
		editorPanel.add(northEditorPanel,BorderLayout.NORTH);
		
		//数据&网格界面组件部分
		dataGridPanel.setLayout(new BorderLayout());
		dataGridPanel.setBorder(BorderUtils.createEmsBorder("常规"));
		
		JPanel northDataGridPanel = new JPanel();
		northDataGridPanel.setLayout(new GridBagLayout());
		
		primaryKeyWarnCheckBox = new JCheckBox("显示主键警告",false);
		displayGridTextBlobdataCheckBox = new JCheckBox("在网格中显示 TEXT 和 Blob 栏位的数据",true);
		limitRecordCheckBox = new JCheckBox("限制记录",true);

		JPanel recordPerPagePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		recordPerPageTextField = new JTextField("1000",6);
		recordPerPageLabel = new JLabel("条记录 (每页)");
		
		recordPerPagePanel.add(recordPerPageTextField);
		recordPerPagePanel.add(recordPerPageLabel);
		
		JPanel editorForeignKeyPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		editorForeignKeyTextField = new JTextField("100",6);
		editorForeignKeyLabel = new JLabel("外键编辑器中每页的记录条数");
		
		editorForeignKeyPanel.add(editorForeignKeyTextField);
		editorForeignKeyPanel.add(editorForeignKeyLabel);
		
		synchCurrentRecordCheckBox = new JCheckBox("同步当前的记录",true);
		
		JPanel rowHeightPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		rowHeightLabel = new JLabel("行高:       ");
		rowHeightTextField = new JTextField("17",10);
		
		rowHeightPanel.add(rowHeightLabel);
		rowHeightPanel.add(rowHeightTextField);
		
		JPanel columnWidthPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		columnWidthLabel = new JLabel("列宽度:     ");
		columnWidthTextField = new JTextField("150",10);
		
		columnWidthPanel.add(columnWidthLabel);
		columnWidthPanel.add(columnWidthTextField);
		
		northDataGridPanel.add(primaryKeyWarnCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(3, 5, 0, 5), 0, 0));
		northDataGridPanel.add(displayGridTextBlobdataCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(3, 5, 0, 5), 0, 0));
		northDataGridPanel.add(limitRecordCheckBox,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(3, 5, 0, 5), 0, 0));
		northDataGridPanel.add(recordPerPagePanel,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(3, 25, 0, 5), 0, 0));
		northDataGridPanel.add(editorForeignKeyPanel,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(3, 25, 0, 5), 0, 0));
		northDataGridPanel.add(synchCurrentRecordCheckBox,new GridBagConstraints(0, 5, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(3, 5, 0, 5), 0, 0));
		northDataGridPanel.add(rowHeightPanel,new GridBagConstraints(0, 6, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(3, 5, 0, 5), 0, 0));
		northDataGridPanel.add(columnWidthPanel,new GridBagConstraints(0, 7, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(3, 5, 0, 5), 0, 0));
		
		dataGridPanel.add(northDataGridPanel,BorderLayout.NORTH);
		
		//显示格式界面绘制
		displayFormatPanel.setLayout(new BorderLayout());
		displayFormatPanel.setBorder(BorderUtils.createEmsBorder("显示格式"));
		
		JPanel northDisplayFormatPanel = new JPanel();
		northDisplayFormatPanel.setLayout(new GridBagLayout());
		
		JPanel integerPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		integerLabel = new JLabel("整数:      ");
		integerTextField = new JTextField(30);
		
		integerPanel.add(integerLabel);
		integerPanel.add(integerTextField);
		
		JPanel floatPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		floatLabel = new JLabel("浮点数:    ");
		floatTextField = new JTextField(30);
		
		floatPanel.add(floatLabel);
		floatPanel.add(floatTextField);
		
		JPanel datePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		dateLabel = new JLabel("日期:      ");
		dateTextField = new JTextField(30);
		
		datePanel.add(dateLabel);
		datePanel.add(dateTextField);
		
		JPanel timePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		timeLabel = new JLabel("时间:      ");
		timeTextField = new JTextField(30);
		
		JPanel timeDatePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		timeDateLabel = new JLabel("时间日期:  ");
		timeDateTextField = new JTextField(30);
		
		timeDatePanel.add(timeDateLabel);
		timeDatePanel.add(timeDateTextField);
		
		timePanel.add(timeLabel);
		timePanel.add(timeTextField);
		
		exampleLabel = new JLabel("范例:");
		exportLabel = new JLabel("输出:");
		
		
		northDisplayFormatPanel.add(integerPanel,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		northDisplayFormatPanel.add(floatPanel,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		northDisplayFormatPanel.add(datePanel,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		northDisplayFormatPanel.add(timePanel,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		northDisplayFormatPanel.add(timeDatePanel,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		northDisplayFormatPanel.add(exampleLabel,new GridBagConstraints(0, 5, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(13,10, 0, 5), 0, 0));
		northDisplayFormatPanel.add(exportLabel,new GridBagConstraints(0, 6, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(13,10, 0, 5), 0, 0));
		
		displayFormatPanel.add(northDisplayFormatPanel,BorderLayout.NORTH);
		
		//模型界面组件绘制
		modelPanel.setLayout(new BorderLayout());
		modelPanel.setBorder(BorderUtils.createEmsBorder("常规"));
		
		JPanel northModelPanel = new JPanel();
		northModelPanel.setLayout(new GridBagLayout());
		
		highlightObjectsCheckBox = new JCheckBox("高亮显示对象",true);
		highlightRelationshipCheckBox = new JCheckBox("高亮显示关系",true);
		guessTypeFieldCheckBox = new JCheckBox("猜测栏位类型",true);
		
		northModelPanel.add(highlightObjectsCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(4, 5, 0, 5), 0, 0));
		northModelPanel.add(highlightRelationshipCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(4, 25, 0, 5), 0, 0));
		northModelPanel.add(guessTypeFieldCheckBox,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(4, 5, 0, 5), 0, 0));
		
		modelPanel.add(northModelPanel,BorderLayout.NORTH);
		
		otherPanel.setLayout(new BorderLayout());
		
		JPanel processPriorityPanel = new JPanel();
		processPriorityPanel.setLayout(new GridBagLayout());
		processPriorityPanel.setBorder(BorderUtils.createEmsBorder("进程优先权"));
		
		
		processPrioritySlider = new JSlider(0, 6, 0);
		processPrioritySlider.setMinorTickSpacing(1);
		processPrioritySlider.setPaintTicks(true);
		
		processPrioritySlider.setValue(3);
		
		noticeLabel = new JLabel("注意：不推荐使用最高");
		
		Hashtable<Integer, JLabel> processPriorityLabelTable = new Hashtable<Integer, JLabel>();
		processPriorityLabelTable.put(Integer.valueOf(0), new JLabel("最低"));
		processPriorityLabelTable.put(Integer.valueOf(3), new JLabel("常规"));
		processPriorityLabelTable.put(Integer.valueOf(6), new JLabel("最高"));
		
		processPrioritySlider.setLabelTable(processPriorityLabelTable);
		processPrioritySlider.setPaintLabels(true);
		
		processPriorityPanel.add(processPrioritySlider,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		processPriorityPanel.add(noticeLabel,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 20, 5), 0, 0));
		

		JPanel filePathPanel = new JPanel();
		filePathPanel.setLayout(new BorderLayout());
		filePathPanel.setBorder(BorderUtils.createEmsBorder("文件路径"));
		
		JPanel northFilePathPanel = new JPanel();
		northFilePathPanel.setLayout(new GridBagLayout());
		
		pathLogFileSaveLabel = new JLabel("日志文件保存路径:");
		pathLogFileSaveTextField = new JTextField();
		pathLogFileSaveButton = new JButton("..");
		setFileSavePathLabel = new JLabel("设置文件保存路径 (MySQL):");
		setFileSavePathTextField = new JTextField();
		setFileSavePathButton = new JButton("..");
		
		northFilePathPanel.add(pathLogFileSaveLabel,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(10, 5, 0, 5), 0, 0));
		northFilePathPanel.add(pathLogFileSaveTextField,new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(10, 5, 0, 5), 0, 0));
		northFilePathPanel.add(pathLogFileSaveButton,new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(10, 5, 0, 5), 0, 0));
		northFilePathPanel.add(setFileSavePathLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(10, 5, 0, 5), 0, 0));
		northFilePathPanel.add(setFileSavePathTextField,new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(10, 5, 0, 5), 0, 0));
		northFilePathPanel.add(setFileSavePathButton,new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(10, 5, 0, 5), 0, 0));
		
		filePathPanel.add(northFilePathPanel,BorderLayout.NORTH);
		
		otherPanel.add(processPriorityPanel,BorderLayout.NORTH);
		otherPanel.add(filePathPanel,BorderLayout.CENTER);
		
		fileAssociationPanel.setLayout(new GridBagLayout());
		fileAssociationPanel.setBorder(BorderUtils.createEmsBorder("文件关联"));
		
		fileAssactionLabel = new MultilineLabel("文件关联用来让 Navicat 代开它保存的文件。例如，" +
				"一个 .npt 文件(数据传输设备文件) 会以数据传输窗口打开、而一个 .npi (导入向导设置文件) " +
				"会以默认的导入向导打开。");
		
		fileAssactionList = new JCheckList(getCheckListItemData());
		JScrollPane fileAssactionPane = new JScrollPane(fileAssactionList,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);;
		allSelectButton = new JButton("全选");
		allDiselectButton = new JButton("全部取消选择");
		
		fileAssociationPanel.add(fileAssactionLabel,new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 
				new Insets(5, 5, 0, 5), 0, 0));
		fileAssociationPanel.add(fileAssactionPane,new GridBagConstraints(0, 1, 1, 3, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, 
				new Insets(5, 5, 5, 5), 0, 0));
		fileAssociationPanel.add(allSelectButton,new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		fileAssociationPanel.add(allDiselectButton,new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		
		optionConfigPanel.add(commonPanel,"常规");
		optionConfigPanel.add(dockPanel,"停靠");
		optionConfigPanel.add(additionalOptionPanel,"代码附加选项");
		optionConfigPanel.add(autoSavePanel,"自动保存");
		optionConfigPanel.add(outwardPanel,"外观");
		optionConfigPanel.add(fontPanel,"字体");
		optionConfigPanel.add(colorPanel,"颜色");
		optionConfigPanel.add(mainWindowPanel,"主窗口");
		optionConfigPanel.add(editorPanel,"编辑器");
		optionConfigPanel.add(dataGridPanel,"数据 & 网格");
		optionConfigPanel.add(displayFormatPanel,"显示格式");
		optionConfigPanel.add(modelPanel,"模型");
		optionConfigPanel.add(otherPanel,"其他");
		optionConfigPanel.add(fileAssociationPanel,"文件关联");
		
	}

	private CheckListItem[] getCheckListItemData() {
		MenuDataModel menuDataModel = new MenuDataModel();
		Vector<String> listData = menuDataModel.getOptionFileAssactionnVectorData();
		
		CheckListItem[] checkListItems = new CheckListItem[listData.size()];
		for(int i = 0 ;i < listData.size(); i++){
			checkListItems[i] = new CheckListItem(listData.get(i));
			checkListItems[i].setSelected(true);
		}
		return checkListItems;
	}

	/**
	 * 功能描述：初始化树组件
	 * @param optionTree 
	 * @param root
	 */
	@SuppressWarnings("unchecked")
	private void initTree(JTree optionTree, DefaultMutableTreeNode root) {
		try {
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(ToolOptionDialogMainPanel.class.getResourceAsStream("/config/optiontree/tree.xml"));
			Element rootElement = document.getRootElement();
			List<Element> nodeEles = XPath.selectNodes(rootElement, "/option/root/node");
			getAllChilrenTreeNode(root, nodeEles);
			
			optionTree.setRootVisible(false);
			optionTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
			optionTree.setBorder(new EmptyBorder(new Insets(0, 5, 10, 35)));
			optionTree.setShowsRootHandles(true);
			
			DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer)optionTree.getCellRenderer();
			renderer.setOpenIcon(null);
		    renderer.setClosedIcon(null);
		    renderer.setLeafIcon(null);
//			BasicTreeUI ui = (BasicTreeUI)optionTree.getUI();
//		    ui.setExpandedIcon(null);
//		    ui.setCollapsedIcon(null);
			
			expandAll(optionTree,new TreePath(root),true);
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 加载所有树节点
	 * @param root
	 * @param nodeEles
	 */
	@SuppressWarnings("unchecked")
	private void getAllChilrenTreeNode(DefaultMutableTreeNode root,
			List<Element> nodeEles) {
		ListIterator<Element> listIterNodeEles = nodeEles.listIterator();
		while(listIterNodeEles.hasNext()){
			Element nextListNodeEle = listIterNodeEles.next();
			String nodeName = nextListNodeEle.getAttributeValue("name");
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(nodeName);
			root.add(node);
			
			List<Element> childEles = nextListNodeEle.getChildren();
			if(childEles!=null){
				getAllChilrenTreeNode(node,childEles);
			}
		}
	}
	
	 /**
     * 完全展开或关闭一个树,用于递规执行
     * @param tree JTree
     * @param parent 父节点
     * @param expand 为true则表示展开树,否则为关闭整棵树
     */
    @SuppressWarnings("rawtypes")
	private void expandAll(JTree tree, TreePath parent, boolean expand) {
        TreeNode node = (TreeNode) parent.getLastPathComponent();
        if (node.getChildCount() >= 0) {
            for (Enumeration e = node.children(); e.hasMoreElements(); ) {
                TreeNode n = (TreeNode) e.nextElement();
                TreePath path = parent.pathByAddingChild(n);
                expandAll(tree, path, expand);
            }
        }

        if (expand) {
            tree.expandPath(parent);
        } else {
            tree.collapsePath(parent);
        }
    }

	/**
	 * 功能描述：初始化按钮Panel
	 */
	private void initSouthPanel() {
		southPanel = new JPanel();
		southPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		okButton = new JButton("确定");
		cancelButton = new JButton("取消");
		
		southPanel.add(okButton);
		southPanel.add(cancelButton);
		
	}
	
	/**
	 * 功能描述：添加界面组件
	 */
	private void addComponentPanel() {
		setLayout(new BorderLayout());
		
		add(centerPanel,BorderLayout.CENTER);
		add(southPanel,BorderLayout.SOUTH);
	}

	/**
	 * 功能描述：为界面组件添加监听
	 */
	private void initListeners() {
//		optionTree.addMouseListener(new MouseAdapter() {
//			public void mousePressed(MouseEvent e) {
//				TreePath selPath = optionTree.getPathForLocation(e.getX(),e.getY());  
//				if(selPath!=null){
//					cardLayout.show(optionConfigPanel, selPath.getLastPathComponent().toString().trim());
//				}
//			}
//		});
		optionTree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
//				String select = e.getPath().getPathComponent(e.getPath().getPathCount()-1).toString();
				TreePath selPath = e.getPath();
				cardLayout.show(optionConfigPanel, selPath.getLastPathComponent().toString().trim());
			}
		});
		allSelectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setJCheckListValue(true);
			}
		});
		allDiselectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setJCheckListValue(false);
			}
		});
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				toolOptionDialog.dispose();
			}
		});
		
	}
	
	private void setJCheckListValue(boolean flag) {
		CheckListItem[] checkListItemDatas = fileAssactionList.getCheckListItemData();
		for(CheckListItem checkListItemData:checkListItemDatas){
			checkListItemData.setSelected(flag);
		}
		fileAssactionList.updateUI();
	}
	
}










