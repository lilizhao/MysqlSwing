package com.raisecom.view.tree;

import javax.swing.ImageIcon;

import com.raisecom.util.ImageUtils;

public class FunctionNode extends ConnectionNode{

	public FunctionNode(String nodeName) {
		super(nodeName);
	}

	public ImageIcon getImageIcon() {
		return ImageUtils.FUNCTION_NODE_IMAGE;
	}

}
