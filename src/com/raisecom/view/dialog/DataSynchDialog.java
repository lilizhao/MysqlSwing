package com.raisecom.view.dialog;

import java.net.URL;

import javax.swing.JDialog;

import com.raisecom.util.SwingConstants;

public class DataSynchDialog extends JDialog{

	private static final long serialVersionUID = 3608156666513363227L;

	public DataSynchDialog() {
		URL resource = this.getClass().getResource("/image/dialog/datasynch.png");
		setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		
		DataSynchDialogMainPanel dataSynchDialogMainPanel = new DataSynchDialogMainPanel(this);
		
		setContentPane(dataSynchDialogMainPanel);
		
		setModal(false);
		
		setTitle("����ͬ��");
		setSize(690,596);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	
}
