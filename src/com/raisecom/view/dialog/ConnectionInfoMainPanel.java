package com.raisecom.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.raisecom.model.velocity.ConnectionInfoBean;
import com.raisecom.model.velocity.VelocityGenerator;
import com.raisecom.util.ClipBoard;
import com.raisecom.util.OperationUtils;
import com.raisecom.view.tree.ConnectionNode;
import com.raisecom.view.tree.ConnectionTree;

public class ConnectionInfoMainPanel extends JPanel{

	private static final long serialVersionUID = 3608156666513363227L;
	
	private ConnectionTree connTree;
	private ConnectionInfoDialog connectionInfoDialog;
	
	//面板组件
	private JTabbedPane tabbedPane;
	private JPanel btnPanel;
	
	//存储的连接属性元素
	private Element connectNameEle;
	private ConnectionInfoBean connectionInfoBean;
	
	//常规面板组件
	private JLabel connectionNameLabel;
	private JLabel connectionNameValueLabel;
	private JLabel hostLabel;
	private JLabel hostValueLabel;
	private JLabel portLabel;
	private JLabel portValueLabel;
	private JLabel userNameLabel;
	private JLabel userNameValueLabel;
	private JLabel savePathLabel;
	private JLabel savePathValueLabel;
	private JLabel codeLabel;
	private JLabel codeValueLabel;
	private JLabel stateLabel;
	private JLabel stateValueLabel;
	private JLabel serverVersionLabel;
	private JLabel serverVersionValueLabel;
	private JLabel communicateProtocolLabel; 
	private JLabel communicateProtocolValueLabel;
	private JLabel infoLabel;
	private JLabel infoValueLabel;
	private JLabel sshHostIpLabel;
	private JLabel sshHostIpValueLabel;
	private JLabel httpChannelLabel;
	private JLabel httpChannelValueLabel;
	
	//底部面板组件
	private JButton copyToClipboardButton;
	private JButton closeButton;
	
	public ConnectionInfoMainPanel(ConnectionTree connTree, ConnectionInfoDialog connectionInfoDialog) {
		this.connTree = connTree;
		this.connectionInfoDialog = connectionInfoDialog;
		
		initConnData();
		initTabPanel();
		initBtnPanel();
		addComponent();
		initListerner();
	}

	/**
	 * 初始化连接属性元素
	 */
	private void initConnData() {
		try {
			SAXBuilder saxBuilder = new SAXBuilder();
			Document document = saxBuilder.build(System.getProperty("user.dir")+"\\config\\connections.ncx");
			
			Element rootElement = document.getRootElement();
			
			String connectionName = connTree.getSelectionPath().getLastPathComponent().toString();
			
			@SuppressWarnings("unchecked")
			Iterator<Element> iterConnectionNames = rootElement.getChildren().iterator();
			while(iterConnectionNames.hasNext()){
				Element nextConnectionEle = iterConnectionNames.next();
				
				String connectionEleName = nextConnectionEle.getAttributeValue("ConnectionName");
				
				if(connectionEleName.equals(connectionName)){
					connectNameEle = nextConnectionEle;
					
					initConnectionInfoBean(connectNameEle);
				}
			}
			
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 初始化连接信息Bean
	 * @param connectNameEle
	 */
	private void initConnectionInfoBean(Element connectNameEle) {
		connectionInfoBean = new ConnectionInfoBean();
		
		//General Information
		connectionInfoBean.setConnectionName(connectNameEle.getAttributeValue("ConnectionName"));
		connectionInfoBean.setHostName(connectNameEle.getAttributeValue("Host"));
		connectionInfoBean.setPort(connectNameEle.getAttributeValue("Port"));
		connectionInfoBean.setUserName(connectNameEle.getAttributeValue("UserName"));
		connectionInfoBean.setSavePassword(Boolean.valueOf(connectNameEle.getAttributeValue("SavePassword")));
		
		//Advanced Information
		connectionInfoBean.setSavePath(connectNameEle.getAttributeValue("SettingsSavePath"));
		connectionInfoBean.setCode(connectNameEle.getAttributeValue("Encoding"));
		connectionInfoBean.setKeepConnectionInterval(connectNameEle.getAttributeValue("KeepaliveInterval"));
		connectionInfoBean.setUseMysqlCharSet(Boolean.valueOf(connectNameEle.getAttributeValue("MySQLCharacterSet")));
		connectionInfoBean.setUseAutoCompression(Boolean.valueOf(connectNameEle.getAttributeValue("Compression")));
		connectionInfoBean.setUseAutoConnection(Boolean.valueOf(connectNameEle.getAttributeValue("AutoConnect")));
		connectionInfoBean.setUseAdvanceConnection(false);
		
		//SSL Information
		connectionInfoBean.setUseSSL(Boolean.valueOf(connectNameEle.getAttributeValue("SSL")));
		connectionInfoBean.setUseAuthen(Boolean.valueOf(connectNameEle.getAttributeValue("SSL_Authen")));
		connectionInfoBean.setClientKey(connectNameEle.getAttributeValue("SSL_ClientKey"));
		connectionInfoBean.setClientCert(connectNameEle.getAttributeValue("SSL_ClientCert"));
		connectionInfoBean.setCaCert(connectNameEle.getAttributeValue("SSL_CACert"));
		
		//SSH Information
		connectionInfoBean.setUseSSH(Boolean.valueOf(connectNameEle.getAttributeValue("SSH")));
		connectionInfoBean.setSshHost(connectNameEle.getAttributeValue("SSH_Host"));
		connectionInfoBean.setSshPort(connectNameEle.getAttributeValue("SSH_Port"));
		connectionInfoBean.setSshUserName(connectNameEle.getAttributeValue("SSH_UserName"));
		connectionInfoBean.setAuthenMethod(connectNameEle.getAttributeValue("SSH_AuthenMethod"));
		connectionInfoBean.setSshSavePassword(Boolean.valueOf(connectNameEle.getAttributeValue("SSH_SavePassword")));
		
		//HTTP Information
		connectionInfoBean.setUseHttpChannel(Boolean.valueOf(connectNameEle.getAttributeValue("HTTP")));
		connectionInfoBean.setHttpChannelAddress(connectNameEle.getAttributeValue("HTTP_URL"));
		connectionInfoBean.setUseHttpBase64Code(false);
		connectionInfoBean.setUseHttpPasswordAuthen(false);
		connectionInfoBean.setHttpUserName(connectNameEle.getAttributeValue("HTTP_PA_UserName"));
		connectionInfoBean.setHttpSavePassword(Boolean.valueOf(connectNameEle.getAttributeValue("HTTP_PA_SavePassword")));
		connectionInfoBean.setHttpUseCertAuthen(false);
		connectionInfoBean.setHttpClientKey(connectNameEle.getAttributeValue("HTTP_CA_ClientKey"));
		connectionInfoBean.setHttpClientCert(connectNameEle.getAttributeValue("HTTP_CA_ClientCert"));
		connectionInfoBean.setHttpCaCert(connectNameEle.getAttributeValue("HTTP_CA_CACert"));
		connectionInfoBean.setUseHttpProxy(Boolean.valueOf(connectNameEle.getAttributeValue("HTTP_Proxy")));
		connectionInfoBean.setHttpProxyServerHost(connectNameEle.getAttributeValue("HTTP_Proxy_Host"));
		connectionInfoBean.setHttpProxyServerPort(connectNameEle.getAttributeValue("HTTP_Proxy_Port"));
		connectionInfoBean.setHttpProxyServerUserName(connectNameEle.getAttributeValue("HTTP_Proxy_UserName"));
		connectionInfoBean.setHttpProxyServerSavePassword(Boolean.valueOf(connectNameEle.getAttributeValue("HTTP_Proxy_SavePassword")));
		
		//Other Information
		connectionInfoBean.setServerVersion("N/A");
		connectionInfoBean.setCommunicationProtocol("N/A");
		connectionInfoBean.setServerInformation("N/A");
	}

	/**
	 * 初始化组件监听，为组件添加响应事件
	 */
	private void initListerner() {
		copyToClipboardButton.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				String vmpath = System.getProperty("user.dir") +"\\config";
				String vmName = "conninfo.vm"; 
				
				@SuppressWarnings("rawtypes")
				VelocityGenerator generator = new VelocityGenerator(vmpath, vmName, "connInfoBean");
				generator.setT(connectionInfoBean);
				String connectionInfoStr = generator.generateStringByVM();
				
				ClipBoard.setStringClipboard(connectionInfoStr);
			}
		});
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connectionInfoDialog.dispose();
			}
		});
	}

	/**
	 * 添加界面组件到面板
	 */
	private void addComponent() {
		this.setLayout(new BorderLayout());
		
		add(tabbedPane, BorderLayout.CENTER);
		add(btnPanel, BorderLayout.SOUTH);
	}

	/**
	 * 初始化Btn按钮面板
	 */
	private void initBtnPanel() {
		btnPanel = new JPanel();
		btnPanel.setLayout(new GridBagLayout());
		
		copyToClipboardButton = new JButton("复制到剪贴板");
		copyToClipboardButton.setBorder(new EmptyBorder(new Insets(5, 40, 5, 40)));
		closeButton = new JButton("关闭");
		closeButton.setBorder(new EmptyBorder(new Insets(5, 25, 5, 25)));
		
		btnPanel.add(copyToClipboardButton, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 7, 5, 5), 0, 0));
		btnPanel.add(closeButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(2, 5, 5, 7), 0, 0));
	}
	
	/**
	 * 初始化Tab界面面板
	 */
	private void initTabPanel() {
		tabbedPane = new JTabbedPane();
		tabbedPane.setBorder(new EmptyBorder(new Insets(7, 7, 7, 7)));
		
		JPanel commonPanel = new JPanel();
		commonPanel.setBackground(new Color(251, 251, 251));
		commonPanel.setLayout(new BorderLayout());
		
		JPanel northCommonPanel = new JPanel();
		northCommonPanel.setOpaque(false);
		northCommonPanel.setLayout(new GridBagLayout());
		
		connectionNameLabel = new JLabel("连接名:");
		connectionNameValueLabel = new JLabel(connectNameEle.getAttributeValue("ConnectionName"));
		hostLabel = new JLabel("主机或 IP 地址:");
		hostValueLabel = new JLabel(connectNameEle.getAttributeValue("Host"));
		portLabel = new JLabel("端口:");
		portValueLabel = new JLabel(connectNameEle.getAttributeValue("Port"));
		userNameLabel = new JLabel("用户名:");
		userNameValueLabel = new JLabel(connectNameEle.getAttributeValue("UserName"));
		savePathLabel = new JLabel("设置保存路径:");
		savePathValueLabel = new JLabel("C:\\");
		codeLabel = new JLabel("编码:");
		codeValueLabel = new JLabel(connectNameEle.getAttributeValue("Encoding"));
		stateLabel = new JLabel("状态:");
		stateValueLabel = new JLabel("没有连接");
		ConnectionNode selectTreeNodeObj = (ConnectionNode) ((DefaultMutableTreeNode)connTree.getSelectionPath().getLastPathComponent()).getUserObject();
		if(OperationUtils.OPENSTATE.equals(selectTreeNodeObj.getState())){
			stateValueLabel.setText("已连接");
		}
		
		serverVersionLabel = new JLabel("服务器版本:");
		serverVersionValueLabel = new JLabel("5.1.62-community");
		communicateProtocolLabel = new JLabel("通信协议:");
		communicateProtocolValueLabel = new JLabel("10");
		infoLabel = new JLabel("信息:");
		infoValueLabel = new JLabel("127.0.0.1 via TCP/IP");
		sshHostIpLabel = new JLabel("SSH 主机名:");
		sshHostIpValueLabel = new JLabel("N/A");
		httpChannelLabel = new JLabel("HTTP通道:");
		httpChannelValueLabel = new JLabel("N/A");
		
		
		northCommonPanel.add(connectionNameLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 2, 5), 0, 0));
		northCommonPanel.add(connectionNameValueLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 100, 2, 200), 0, 0));
		northCommonPanel.add(hostLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 2, 5), 0, 0));
		northCommonPanel.add(hostValueLabel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 100, 2, 200), 0, 0));
		northCommonPanel.add(portLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 2, 5), 0, 0));
		northCommonPanel.add(portValueLabel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 100, 2, 200), 0, 0));
		northCommonPanel.add(userNameLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 2, 5), 0, 0));
		northCommonPanel.add(userNameValueLabel, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 100, 2, 200), 0, 0));
		northCommonPanel.add(savePathLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 2, 5), 0, 0));
		northCommonPanel.add(savePathValueLabel, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 100, 2, 200), 0, 0));
		northCommonPanel.add(codeLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 2, 5), 0, 0));
		northCommonPanel.add(codeValueLabel, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 100, 2, 200), 0, 0));
		northCommonPanel.add(stateLabel, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 2, 5), 0, 0));
		northCommonPanel.add(stateValueLabel, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 100, 2, 200), 0, 0));
		northCommonPanel.add(serverVersionLabel, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 2, 5), 0, 0));
		northCommonPanel.add(serverVersionValueLabel, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 100, 2, 100), 0, 0));
		northCommonPanel.add(communicateProtocolLabel, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 2, 5), 0, 0));
		northCommonPanel.add(communicateProtocolValueLabel, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 100, 2, 200), 0, 0));
		northCommonPanel.add(infoLabel, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 2, 5), 0, 0));
		northCommonPanel.add(infoValueLabel, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 100, 2, 100), 0, 0));
		northCommonPanel.add(sshHostIpLabel, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 2, 5), 0, 0));
		northCommonPanel.add(sshHostIpValueLabel, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 100, 2, 100), 0, 0));
		northCommonPanel.add(httpChannelLabel, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 2, 5), 0, 0));
		northCommonPanel.add(httpChannelValueLabel, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 100, 2, 100), 0, 0));
		
		commonPanel.add(northCommonPanel, BorderLayout.NORTH);
		
		tabbedPane.add(" 常规 ", commonPanel);
	}
	
}

  







