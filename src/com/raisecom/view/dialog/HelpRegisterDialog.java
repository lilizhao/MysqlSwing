package com.raisecom.view.dialog;

import java.net.URL;

import javax.swing.JDialog;

import com.raisecom.util.SwingConstants;

public class HelpRegisterDialog extends JDialog{

	private static final long serialVersionUID = 3608156666513363227L;

	public HelpRegisterDialog() {
		URL resource = this.getClass().getResource("/image/dialog/register.png");
		setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		
		HelpRegisterDialogMainPanel helpRegisterDialogMainPanel = new HelpRegisterDialogMainPanel(this);
		
		setContentPane(helpRegisterDialogMainPanel);
		
		setModal(true);
		
		setTitle("ע��");
		setSize(407,234);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	
}
