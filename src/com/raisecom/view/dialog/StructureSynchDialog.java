package com.raisecom.view.dialog;

import java.net.URL;

import javax.swing.JDialog;

import com.raisecom.util.SwingConstants;

public class StructureSynchDialog extends JDialog{

	private static final long serialVersionUID = 3608156666513363227L;

	public StructureSynchDialog() {
		URL resource = this.getClass().getResource("/image/dialog/datasynch.png");
		setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		
		StructureSynchDialogMainPanel structureSynchDialogMainPanel = new StructureSynchDialogMainPanel(this);
		
		setContentPane(structureSynchDialogMainPanel);
		
		setModal(false);
		
		setTitle("�ṹͬ��");
		setSize(768,674);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	
}
