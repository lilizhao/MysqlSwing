package com.raisecom.control.toolaction;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JRadioButton;

public class ToolActionListener<T> extends MouseAdapter{
	private String mouseenteredimage = "";
	private String mouseexitedimage = "";
	private T jButton = null;


	

	public ToolActionListener(String mouseenteredimage,
			String mouseexitedimage, T jButton) {
		this.mouseenteredimage = mouseenteredimage;
		this.mouseexitedimage = mouseexitedimage;
		this.jButton = jButton;
	}


	public void mouseEntered(MouseEvent e) {
		URL url = this.getClass().getResource(mouseenteredimage);
		setImageIcon(url);
	}

	public void mouseExited(MouseEvent e) {
		URL url = this.getClass().getResource(mouseexitedimage);
		setImageIcon(url);
	}

	private void setImageIcon(URL url) {
		if(jButton instanceof JButton){
			((JButton) jButton).setIcon(new ImageIcon(url.getPath()));
		}else if(jButton instanceof JRadioButton && ((JRadioButton)jButton).isSelected()==false){
			((JRadioButton) jButton).setIcon(new ImageIcon(url.getPath()));
		}
	}


	

}
