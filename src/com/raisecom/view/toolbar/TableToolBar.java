package com.raisecom.view.toolbar;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JToolBar;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

import com.raisecom.control.toolaction.ToolActionListener;
import com.raisecom.control.toolaction.ToolGeneralActionListener;
import com.raisecom.control.toolaction.ToolRadioActionListener;
import com.raisecom.model.data.ToolBarRadioBtnBean;

public class TableToolBar extends JToolBar {

	private static final long serialVersionUID = 4346886236771423773L;
	
	private static Map<String, ToolBarRadioBtnBean> mapToolBarRadioBtnBean;
	
	public static Map<String, ToolBarRadioBtnBean> getMapToolBarRadioBtnBean() {
		return mapToolBarRadioBtnBean;
	}

	@SuppressWarnings("unchecked")
	public TableToolBar() {
		mapToolBarRadioBtnBean = new LinkedHashMap<String, ToolBarRadioBtnBean>();
		setFloatable(false);
		
		ToolGeneralActionListener toolGeneralActionListener = new ToolGeneralActionListener();
		try {
			ButtonGroup buttonGroup = new ButtonGroup();

			SAXBuilder saxBuilder = new SAXBuilder();
			Document document = saxBuilder.build(this.getClass().getResource("/config/tool/tabletoolbar.xml"));
			Element rootElement = document.getRootElement();
			List<Element> buttonEles = XPath.selectNodes(rootElement,"/buttons/tool/button");
			Iterator<Element> iterButtonEles = buttonEles.iterator();
			while (iterButtonEles.hasNext()) {
				ToolBarRadioBtnBean barRadioBtnBean = new ToolBarRadioBtnBean();
				
				Element nextButtonEle = iterButtonEles.next();
				String buttonType = nextButtonEle.getAttributeValue("type");
				String actioncommand = nextButtonEle.getAttributeValue("actioncommand");
				String initimage = nextButtonEle.getAttributeValue("initimage");
				String mouseenteredimage = nextButtonEle.getAttributeValue("mouseenteredimage");
				String mouseexitedimage = nextButtonEle.getAttributeValue("mouseexitedimage");
//			    String selectedimage = nextButtonEle.getAttributeValue("selectedimage");
				String menuName = nextButtonEle.getAttributeValue("name");
				
				if ("|".equals(menuName)) {
					addSeparator();
				} else {
					if ("radio".equals(buttonType)) {
						URL url = this.getClass().getResource(initimage);
						JRadioButton jRadioButton = new JRadioButton(new ImageIcon(url.getPath()));
						ToolActionListener toolRadioActionListener = new ToolActionListener(mouseenteredimage,mouseexitedimage,jRadioButton);
						
						jRadioButton.setActionCommand(actioncommand);
							
						buttonGroup.add(jRadioButton);
						
						jRadioButton.addActionListener(new ToolRadioActionListener(rootElement,buttonGroup));
						jRadioButton.addActionListener(toolGeneralActionListener);
						jRadioButton.addMouseListener(toolRadioActionListener);
						
						barRadioBtnBean.setActionCommand(actioncommand);
						barRadioBtnBean.setjRadioButton(jRadioButton);
						barRadioBtnBean.setIsSelectedImage(mouseenteredimage);
						barRadioBtnBean.setIsNotSelectedImage(mouseexitedimage);
						
						mapToolBarRadioBtnBean.put(actioncommand, barRadioBtnBean);
						
						add(jRadioButton);
					} else {
						URL url = this.getClass().getResource(initimage);
						JButton jButton = new JButton(new ImageIcon(url.getPath()));
						jButton.setActionCommand(actioncommand);
						
						jButton.addActionListener(toolGeneralActionListener);
						jButton.addMouseListener(new ToolActionListener(mouseenteredimage,mouseexitedimage,jButton));
						add(jButton);
					}
				}
			}
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
