package com.raisecom.view.frame;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;

import javax.swing.JFrame;
import javax.swing.UIManager;

import com.raisecom.model.data.ConnectionDataContext;
import com.raisecom.view.menubar.MainMenuBar;
import com.raisecom.view.panel.MainPanel;
import com.raisecom.view.panel.SouthRootPanel;
import com.raisecom.view.toolbar.MainToolBar;
import com.raisecom.view.tree.ConnectionTree;

/**
 * 程序入口类
 * 
 * @author lilz-2686
 * 
 */
public class MainFrame extends JFrame {

	private static final long serialVersionUID = 5761324784842767310L;

	private ConnectionDataContext dataContext;
	private MainMenuBar mainMenuBar;
	private MainToolBar mainToolBar;
	private ConnectionTree connTree;
	private MainPanel mainPanel;
	private SouthRootPanel rootPanel;

	private static MainFrame mainFrame = new MainFrame();

	public static MainFrame getMainFrame() {
		return mainFrame;
	}

	private MainFrame() throws HeadlessException {
		// initSplashWindow();
		initData();
		// depressedSplashWindow();
		setLookAndFeel();
		initViewComponent();
		initFrame();
		// showUI();
	}

	/**
	 * 初始化连接数据
	 */
	private void initData() {
		dataContext = new ConnectionDataContext();
	}

	/**
	 * 设置界面风格
	 */
	private void setLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 初始化界面组件
	 */
	private void initViewComponent() {
		mainMenuBar = new MainMenuBar("/config/menu/menu.xml");
		mainToolBar = MainToolBar.getMainToolBar();
		connTree = new ConnectionTree(dataContext);
		mainPanel = new MainPanel(connTree);
		rootPanel = new SouthRootPanel(connTree);
	}

	/**
	 * 初始化主Frame
	 */
	private void initFrame() {

		String lastModifiedStr = getLastCompile();
		setTitle("Navicat for MySQL " + lastModifiedStr);
		setSize(970, 680);

		setLayout(new BorderLayout());

		setJMenuBar(mainMenuBar);
		add(mainToolBar, BorderLayout.NORTH);
		add(mainPanel, BorderLayout.CENTER);
		add(rootPanel, BorderLayout.SOUTH);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// pack();
	}

	/**
	 * 功获得最后一次的编译时间
	 * 
	 * @return
	 */
	private String getLastCompile() {
		File jarFile = new File("MysqlSwing.jar");
		long lastModified = jarFile.lastModified();
		if (lastModified == 0) {
			lastModified = System.currentTimeMillis();
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		String lastModifiedStr = simpleDateFormat.format(Long.parseLong(String
				.valueOf(lastModified)));
		return lastModifiedStr;
	}

	/**
	 * 显示功能界面
	 */
	private void showUI() {
		// setResizable(false);
		// frame初始化到屏幕中央
		int pw = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		int w = this.getWidth();
		int ph = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		int h = this.getHeight();
		setLocation((pw - w) / 2, (ph - h) / 2);

		URL url = this.getClass().getResource("/image/frame/title.png");
		setIconImage(Toolkit.getDefaultToolkit().getImage(url));
		setVisible(true);
	}

	public static void main(String[] args) {
		// 获取开始时间
		long startTime = System.currentTimeMillis();

		mainFrame.showUI();

		// 获取结束时间
		long endTime = System.currentTimeMillis();
		System.out.println("工具启动用时： " + (endTime - startTime) * 1.0 / 1000
				+ "s");
	}

}
