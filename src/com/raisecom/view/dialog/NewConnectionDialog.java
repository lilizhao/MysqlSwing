package com.raisecom.view.dialog;

import java.net.URL;

import javax.swing.JDialog;

import com.raisecom.util.SwingConstants;
import com.raisecom.view.tree.ConnectionNode;

public class NewConnectionDialog extends JDialog{

	private static final long serialVersionUID = -2815998979954050153L;
	
	public NewConnectionDialog() {
		NewConnectionDialogMainPanel newConnectionDialogMainPanel = new NewConnectionDialogMainPanel(this);
		setContentPane(newConnectionDialogMainPanel);

		setModal(true);
		
		setTitle("新建连接");
		URL resource = this.getClass().getResource("/image/dialog/openconnection.png");
		setIconImage(SwingConstants.getImageIcon(resource.getPath()));
//		setSize((int)(Toolkit.getDefaultToolkit().getScreenSize().getWidth()*43/120),
//				(int)(Toolkit.getDefaultToolkit().getScreenSize().getHeight()*43/60));
		setSize(487,553);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public NewConnectionDialog(ConnectionNode connectionNode, String operateType) {
		
		NewConnectionDialogMainPanel newConnectionDialogMainPanel = new NewConnectionDialogMainPanel(this, connectionNode , operateType);
		setContentPane(newConnectionDialogMainPanel);

		setModal(true);
		
		setTitle("新建连接");
		URL resource = this.getClass().getResource("/image/dialog/openconnection.png");
		setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		setSize(487,553);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	
}
