package com.raisecom.view.frame;

import java.awt.BorderLayout;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class CommonFrame extends JFrame{
	
	private static final long serialVersionUID = 4571605135029567184L;
	
	private JPanel contentPane;

	public CommonFrame() {
		initViewComponent();
		showUI();
		
	}

	/**
	 * 初始化界面组件
	 */
	private void initViewComponent() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}
	
	/**
	 * 显示界面
	 */
	private void showUI() {
		setSize(642, 483);
        initViewLocation();
		setVisible(true);
	}

	/**
	 * 初始化界面所在位置
	 */
	private void initViewLocation() {
		int pw = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        int w = this.getWidth();
        int ph = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        int h = this.getHeight();
        setLocation((pw - w) / 2, (ph - h)/2);
	}
	

}
