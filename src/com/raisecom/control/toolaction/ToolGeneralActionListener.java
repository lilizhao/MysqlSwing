package com.raisecom.control.toolaction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JRadioButton;

import com.raisecom.view.dialog.NewConnectionDialog;

/**
 * 功能描述：为所有的工具栏菜单项添加响应动作
 * @author lilz-2686
 *
 */
public class ToolGeneralActionListener implements ActionListener{

	public void actionPerformed(ActionEvent ae) {
		String actionCommand = null;
		
		Object source = ae.getSource(); 
		if(source instanceof JButton){
			JButton jButton = (JButton)source;
			actionCommand = jButton.getActionCommand();
		}else if(source instanceof JRadioButton){
			JRadioButton jButton = (JRadioButton)source;
			actionCommand = jButton.getActionCommand();
		}
		
		if("buttons.tool.connection".equals(actionCommand)){
			new NewConnectionDialog();
		}else{

		}
	}

	
}
