package com.raisecom.view.dialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.jdom.Content;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

import com.raisecom.util.SwingConstants;
import com.raisecom.view.custom.MyFileFilter;

public class ImportConnectionDialog extends JDialog{

	private static final long serialVersionUID = -2815998979954050153L;

	public ImportConnectionDialog() {

		setModal(true);
		
		URL resource = this.getClass().getResource("/image/dialog/openconnection.png");
		setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		
		ImportConnectionFileChooser connectionFileChooser = new ImportConnectionFileChooser();
		connectionFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		connectionFileChooser.setAcceptAllFileFilterUsed(false);
		connectionFileChooser.setFileFilter(new MyFileFilter(".ncx", "*.ncx"));
		connectionFileChooser.showOpenDialog(this);
		
		File selectedFile = connectionFileChooser.getSelectedFile();
		
		try {
			SAXBuilder builder = new SAXBuilder();
			if(selectedFile!=null){
				Document document = builder.build(selectedFile);
				Element rootElement = document.getRootElement();
				@SuppressWarnings("unchecked")
				List<Element> selectNodesEle = XPath.selectNodes(rootElement, "/Connections/Connection");
				Iterator<Element> iterSelectNodes = selectNodesEle.iterator();
				
				String targetFilePath = System.getProperty("user.dir")+"\\config\\connections.ncx";
				Document targetDocument = builder.build(targetFilePath);
				Element targetRootElement = targetDocument.getRootElement();
				
				while(iterSelectNodes.hasNext()){
					Element nextSelectEle = iterSelectNodes.next();
					String nextConnectName = nextSelectEle.getAttributeValue("ConnectionName");
					
					Element repeatEles = (Element) XPath.selectSingleNode(targetRootElement, "/Connections/Connection[@ConnectionName='"+nextConnectName+"']");
					if(repeatEles!=null){
						Object[] options = {"替换","跳过","取消"};
						int n = JOptionPane.showOptionDialog(this,
								"连接 "+ nextConnectName +" 已存在。你要替换现有的连接?",
								"确认",JOptionPane.YES_NO_CANCEL_OPTION,
								JOptionPane.QUESTION_MESSAGE,null,options,options[2]);
						if (n == JOptionPane.YES_OPTION){
							targetRootElement.removeContent(repeatEles);
							addNewElementForConnection(targetFilePath, targetDocument, targetRootElement, nextSelectEle);  
						}
					}else{
						addNewElementForConnection(targetFilePath, targetDocument, targetRootElement, nextSelectEle);  
					}
				}
			}
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		setVisible(false);
	}

	private void addNewElementForConnection(String targetFilePath,
			Document targetDocument, Element targetRootElement,
			Element nextSelectEle) throws FileNotFoundException, IOException {
		targetRootElement.addContent(((Content) nextSelectEle.clone()).detach());
		
		FileOutputStream out = new FileOutputStream(targetFilePath);
		XMLOutputter outputter = new XMLOutputter();        
		Format f = Format.getPrettyFormat();    
		outputter.setFormat(f);     
		outputter.output(targetDocument, out);    
		out.close();
	}
	
	
	
}
