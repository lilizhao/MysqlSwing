package com.raisecom.view.dialog;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.raisecom.util.BorderUtils;
import com.raisecom.util.SwingConstants;
import com.raisecom.view.custom.CheckListItem;
import com.raisecom.view.custom.JCheckList;

/**
 * 选择列主面板
 * @author Administrator
 *
 */
public class SelectColumnMainPanel extends JPanel{

	private static final long serialVersionUID = -4208137703316093695L;
	
	private SelectColumnDialog selectColumnDialog;
	
	private CardLayout cardLayout;
	private JList selectList;
	private JPanel selectTreePanel;
	
	//选择列面板
	private JPanel selectColumnPanel;
	private JPanel userSelectColumnPanel;
	private JPanel tableSelectColumnPanel;
	private JPanel viewSelectColunPanel;
	private JPanel functionSelectColunPanel;
	private JPanel eventSelectColunPanel;
	private JPanel searchSelectColunPanel;
	private JPanel reportSelectColunPanel;
	private JPanel backupSelectColunPanel;
	private JPanel planSelectColunPanel;
	private JPanel btnPanel;
	
	//选择树节点[用户]时的列列表
	@SuppressWarnings("rawtypes")
	private JCheckList userSelectColumnList;
	private JButton userMoveUpButton;
	private JButton userMoveDownButton;
	private JButton userAllSelectButton;
	private JButton userAllDisSelectButton;
	
//	//选择树节点[表]时的列列表
	@SuppressWarnings("rawtypes")
	private JCheckList tableSelectColumnList;
	private JButton tableMoveUpButton;
	private JButton tableMoveDownButton;
	private JButton tableAllSelectButton;
	private JButton tableAllDisSelectButton;
	
	//选择树节点[视图]时的列列表
	@SuppressWarnings("rawtypes")
	private JCheckList viewSelectColumnList;
	private JButton viewMoveUpButton;
	private JButton viewMoveDownButton;
	private JButton viewAllSelectButton;
	private JButton viewAllDisSelectButton;
	
	//选择树节点[视图]时的列列表
	@SuppressWarnings("rawtypes")
	private JCheckList functionSelectColumnList;
	private JButton functionMoveUpButton;
	private JButton functionMoveDownButton;
	private JButton functionAllSelectButton;
	private JButton functionAllDisSelectButton;
	
	//选择树节点[事件]时的列列表
	@SuppressWarnings("rawtypes")
	private JCheckList eventSelectColumnList;
	private JButton eventMoveUpButton;
	private JButton eventMoveDownButton;
	private JButton eventAllSelectButton;
	private JButton eventAllDisSelectButton;
	
	//选择树节点[查询]时的列列表
	@SuppressWarnings("rawtypes")
	private JCheckList searchSelectColumnList;
	private JButton searchMoveUpButton;
	private JButton searchMoveDownButton;
	private JButton searchAllSelectButton;
	private JButton searchAllDisSelectButton;
	
	//选择树节点[报表]时的列列表
	@SuppressWarnings("rawtypes")
	private JCheckList reportSelectColumnList;
	private JButton reportMoveUpButton;
	private JButton reportMoveDownButton;
	private JButton reportAllSelectButton;
	private JButton reportAllDisSelectButton;
	
	//选择树节点[报表]时的列列表
	@SuppressWarnings("rawtypes")
	private JCheckList backupSelectColumnList;
	private JButton backupMoveUpButton;
	private JButton backupMoveDownButton;
	private JButton backupAllSelectButton;
	private JButton backupAllDisSelectButton;
	
	//选择树节点[报表]时的列列表
	@SuppressWarnings("rawtypes")
	private JCheckList planSelectColumnList;
	private JButton planMoveUpButton;
	private JButton planMoveDownButton;
	private JButton planAllSelectButton;
	private JButton planAllDisSelectButton;
	
	//初始化全选按钮/取消全选按钮
	private Map<JButton, JCheckList> allSelectButtonMap;
	private Map<JButton, JCheckList> allDisSelectButtonMap;
	
	//底部面板组件
	private JButton okBtn;
	private JButton cancelBtn;
	
	public SelectColumnMainPanel(SelectColumnDialog selectColumnDialog) {
		this.selectColumnDialog = selectColumnDialog;
		
		initSelectTreePanel();
		initSelectColumnPanel();
		initBtnPanel();
		addViewComponent();
		addAllSelectBtnMap();
		initListener();
	}

	/**
	 * 初始化全选按钮/取消全选按钮
	 */
	private void addAllSelectBtnMap() {
		allSelectButtonMap = new LinkedHashMap<JButton, JCheckList>();
		allDisSelectButtonMap = new LinkedHashMap<JButton, JCheckList>();
		
		allSelectButtonMap.put(userAllSelectButton, userSelectColumnList);
		allSelectButtonMap.put(tableAllSelectButton, tableSelectColumnList);
		allSelectButtonMap.put(viewAllSelectButton, viewSelectColumnList);
		allSelectButtonMap.put(functionAllSelectButton, functionSelectColumnList);
		allSelectButtonMap.put(eventAllSelectButton, eventSelectColumnList);
		allSelectButtonMap.put(searchAllSelectButton, searchSelectColumnList);
		allSelectButtonMap.put(reportAllSelectButton, reportSelectColumnList);
		allSelectButtonMap.put(backupAllSelectButton, backupSelectColumnList);
		allSelectButtonMap.put(planAllSelectButton, planSelectColumnList);
		
		allDisSelectButtonMap.put(userAllDisSelectButton, userSelectColumnList);
		allDisSelectButtonMap.put(tableAllDisSelectButton, tableSelectColumnList);
		allDisSelectButtonMap.put(viewAllDisSelectButton, viewSelectColumnList);
		allDisSelectButtonMap.put(functionAllDisSelectButton, functionSelectColumnList);
		allDisSelectButtonMap.put(eventAllDisSelectButton, eventSelectColumnList);
		allDisSelectButtonMap.put(searchAllDisSelectButton, searchSelectColumnList);
		allDisSelectButtonMap.put(reportAllDisSelectButton, reportSelectColumnList);
		allDisSelectButtonMap.put(backupAllDisSelectButton, backupSelectColumnList);
		allDisSelectButtonMap.put(planAllDisSelectButton, planSelectColumnList);
	}

	/**
	 * 初始化选择列面板
	 */
	private void initSelectColumnPanel() {
		cardLayout = new CardLayout(5,5);
		selectColumnPanel = new JPanel();
		selectColumnPanel.setLayout(cardLayout);
		
		initUserSelectColumnPanel();
		initTableSelectColumnPanel();
		initViewSelectColunPanel();
		initFunctionSelectColunPanel();
		initEventSelectColunPanel();
		initSearchSelectColunPanel();
		initReportSelectColunPanel();
		initBackupSelectColunPanel();
		initPlanSelectColunPanel();
		
		selectColumnPanel.add(userSelectColumnPanel,"    用户");
		selectColumnPanel.add(tableSelectColumnPanel,"    表");
		selectColumnPanel.add(viewSelectColunPanel,"    视图");
		selectColumnPanel.add(functionSelectColunPanel,"    函数");
		selectColumnPanel.add(eventSelectColunPanel,"    事件");
		selectColumnPanel.add(searchSelectColunPanel,"    查询");
		selectColumnPanel.add(reportSelectColunPanel,"    报表");
		selectColumnPanel.add(backupSelectColunPanel,"    备份");
		selectColumnPanel.add(planSelectColunPanel,"    计划");
	}

	/**
	 * 初始化计划选择面板
	 */
	private void initPlanSelectColunPanel() {
		planSelectColunPanel = new JPanel();
		planSelectColunPanel.setLayout(new GridBagLayout());
		planSelectColunPanel.setBorder(BorderUtils.createEmsBorder("列"));
		
		List<String> selectColumns = SwingConstants.getSelectColumns().get("    计划");
		planSelectColumnList = new JCheckList(getCheckListItemData(selectColumns));
		JScrollPane selectColumnScrollPane = new JScrollPane(planSelectColumnList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		selectColumnScrollPane.getViewport().setPreferredSize(new Dimension(200, 0));
		planMoveUpButton = new JButton("上移");
		planMoveDownButton = new JButton("下移");
		planAllSelectButton = new JButton("全选");
		planAllDisSelectButton = new JButton("全部取消选择");
		
		planSelectColunPanel.add(selectColumnScrollPane, new GridBagConstraints(0, 0, 1, 5, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		planSelectColunPanel.add(planMoveUpButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		planSelectColunPanel.add(planMoveDownButton, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		planSelectColunPanel.add(planAllSelectButton, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		planSelectColunPanel.add(planAllDisSelectButton, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
	}

	/**
	 * 初始化备份选择面板
	 */
	private void initBackupSelectColunPanel() {
		backupSelectColunPanel = new JPanel();
		backupSelectColunPanel.setLayout(new GridBagLayout());
		backupSelectColunPanel.setBorder(BorderUtils.createEmsBorder("列"));
		
		List<String> selectColumns = SwingConstants.getSelectColumns().get("    备份");
		backupSelectColumnList = new JCheckList(getCheckListItemData(selectColumns));
		JScrollPane selectColumnScrollPane = new JScrollPane(backupSelectColumnList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		selectColumnScrollPane.getViewport().setPreferredSize(new Dimension(200, 0));
		backupMoveUpButton = new JButton("上移");
		backupMoveDownButton = new JButton("下移");
		backupAllSelectButton = new JButton("全选");
		backupAllDisSelectButton = new JButton("全部取消选择");
		
		backupSelectColunPanel.add(selectColumnScrollPane, new GridBagConstraints(0, 0, 1, 5, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		backupSelectColunPanel.add(backupMoveUpButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		backupSelectColunPanel.add(backupMoveDownButton, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		backupSelectColunPanel.add(backupAllSelectButton, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		backupSelectColunPanel.add(backupAllDisSelectButton, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
	}

	/**
	 * 初始化报表选择面板
	 */
	private void initReportSelectColunPanel() {
		reportSelectColunPanel = new JPanel();
		reportSelectColunPanel.setLayout(new GridBagLayout());
		reportSelectColunPanel.setBorder(BorderUtils.createEmsBorder("列"));
		
		List<String> selectColumns = SwingConstants.getSelectColumns().get("    报表");
		reportSelectColumnList = new JCheckList(getCheckListItemData(selectColumns));
		JScrollPane selectColumnScrollPane = new JScrollPane(reportSelectColumnList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		selectColumnScrollPane.getViewport().setPreferredSize(new Dimension(200, 0));
		reportMoveUpButton = new JButton("上移");
		reportMoveDownButton = new JButton("下移");
		reportAllSelectButton = new JButton("全选");
		reportAllDisSelectButton = new JButton("全部取消选择");
		
		reportSelectColunPanel.add(selectColumnScrollPane, new GridBagConstraints(0, 0, 1, 5, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		reportSelectColunPanel.add(reportMoveUpButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		reportSelectColunPanel.add(reportMoveDownButton, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		reportSelectColunPanel.add(reportAllSelectButton, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		reportSelectColunPanel.add(reportAllDisSelectButton, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
	}

	/**
	 * 初始化查询选择面板
	 */
	private void initSearchSelectColunPanel() {
		searchSelectColunPanel = new JPanel();
		searchSelectColunPanel.setLayout(new GridBagLayout());
		searchSelectColunPanel.setBorder(BorderUtils.createEmsBorder("列"));
		
		List<String> selectColumns = SwingConstants.getSelectColumns().get("    查询");
		searchSelectColumnList = new JCheckList(getCheckListItemData(selectColumns));
		JScrollPane selectColumnScrollPane = new JScrollPane(searchSelectColumnList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		selectColumnScrollPane.getViewport().setPreferredSize(new Dimension(200, 0));
		searchMoveUpButton = new JButton("上移");
		searchMoveDownButton = new JButton("下移");
		searchAllSelectButton = new JButton("全选");
		searchAllDisSelectButton = new JButton("全部取消选择");
		
		searchSelectColunPanel.add(selectColumnScrollPane, new GridBagConstraints(0, 0, 1, 5, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		searchSelectColunPanel.add(searchMoveUpButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		searchSelectColunPanel.add(searchMoveDownButton, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		searchSelectColunPanel.add(searchAllSelectButton, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		searchSelectColunPanel.add(searchAllDisSelectButton, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
	}

	/**
	 * 初始化事件选择面板
	 */
	private void initEventSelectColunPanel() {
		eventSelectColunPanel = new JPanel();
		eventSelectColunPanel.setLayout(new GridBagLayout());
		eventSelectColunPanel.setBorder(BorderUtils.createEmsBorder("列"));
		
		List<String> selectColumns = SwingConstants.getSelectColumns().get("    事件");
		eventSelectColumnList = new JCheckList(getCheckListItemData(selectColumns));
		JScrollPane selectColumnScrollPane = new JScrollPane(eventSelectColumnList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		selectColumnScrollPane.getViewport().setPreferredSize(new Dimension(200, 0));
		eventMoveUpButton = new JButton("上移");
		eventMoveDownButton = new JButton("下移");
		eventAllSelectButton = new JButton("全选");
		eventAllDisSelectButton = new JButton("全部取消选择");
		
		eventSelectColunPanel.add(selectColumnScrollPane, new GridBagConstraints(0, 0, 1, 5, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		eventSelectColunPanel.add(eventMoveUpButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		eventSelectColunPanel.add(eventMoveDownButton, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		eventSelectColunPanel.add(eventAllSelectButton, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		eventSelectColunPanel.add(eventAllDisSelectButton, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
	}

	/**
	 * 初始化函数选择面板
	 */
	private void initFunctionSelectColunPanel() {
		functionSelectColunPanel = new JPanel();
		functionSelectColunPanel.setLayout(new GridBagLayout());
		functionSelectColunPanel.setBorder(BorderUtils.createEmsBorder("列"));
		
		List<String> selectColumns = SwingConstants.getSelectColumns().get("    函数");
		functionSelectColumnList = new JCheckList(getCheckListItemData(selectColumns));
		JScrollPane selectColumnScrollPane = new JScrollPane(functionSelectColumnList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		selectColumnScrollPane.getViewport().setPreferredSize(new Dimension(200, 0));
		functionMoveUpButton = new JButton("上移");
		functionMoveDownButton = new JButton("下移");
		functionAllSelectButton = new JButton("全选");
		functionAllDisSelectButton = new JButton("全部取消选择");
		
		functionSelectColunPanel.add(selectColumnScrollPane, new GridBagConstraints(0, 0, 1, 5, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		functionSelectColunPanel.add(functionMoveUpButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		functionSelectColunPanel.add(functionMoveDownButton, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		functionSelectColunPanel.add(functionAllSelectButton, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		functionSelectColunPanel.add(functionAllDisSelectButton, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
	}

	/**
	 * 初始化视图选择列面板
	 */
	private void initViewSelectColunPanel() {
		viewSelectColunPanel = new JPanel();
		viewSelectColunPanel.setLayout(new GridBagLayout());
		viewSelectColunPanel.setBorder(BorderUtils.createEmsBorder("列"));
		
		List<String> selectColumns = SwingConstants.getSelectColumns().get("    视图");
		viewSelectColumnList = new JCheckList(getCheckListItemData(selectColumns));
		JScrollPane selectColumnScrollPane = new JScrollPane(viewSelectColumnList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		selectColumnScrollPane.getViewport().setPreferredSize(new Dimension(200, 0));
		viewMoveUpButton = new JButton("上移");
		viewMoveDownButton = new JButton("下移");
		viewAllSelectButton = new JButton("全选");
		viewAllDisSelectButton = new JButton("全部取消选择");
		
		viewSelectColunPanel.add(selectColumnScrollPane, new GridBagConstraints(0, 0, 1, 5, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		viewSelectColunPanel.add(viewMoveUpButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		viewSelectColunPanel.add(viewMoveDownButton, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		viewSelectColunPanel.add(viewAllSelectButton, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		viewSelectColunPanel.add(viewAllDisSelectButton, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
	}

	/**
	 * 初始化表选择列面板
	 */
	private void initTableSelectColumnPanel() {
		tableSelectColumnPanel = new JPanel();
		tableSelectColumnPanel.setLayout(new GridBagLayout());
		tableSelectColumnPanel.setBorder(BorderUtils.createEmsBorder("列"));
		
		List<String> selectColumns = SwingConstants.getSelectColumns().get("    表");
		tableSelectColumnList = new JCheckList(getCheckListItemData(selectColumns));
		JScrollPane selectColumnScrollPane = new JScrollPane(tableSelectColumnList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		selectColumnScrollPane.getViewport().setPreferredSize(new Dimension(200, 0));
		tableMoveUpButton = new JButton("上移");
		tableMoveDownButton = new JButton("下移");
		tableAllSelectButton = new JButton("全选");
		tableAllDisSelectButton = new JButton("全部取消选择");
		
		tableSelectColumnPanel.add(selectColumnScrollPane, new GridBagConstraints(0, 0, 1, 5, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		tableSelectColumnPanel.add(tableMoveUpButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		tableSelectColumnPanel.add(tableMoveDownButton, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		tableSelectColumnPanel.add(tableAllSelectButton, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		tableSelectColumnPanel.add(tableAllDisSelectButton, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
	}

	/**
	 * 初始化底部按钮面板
	 */
	private void initBtnPanel() {
		btnPanel = new JPanel();
		btnPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		okBtn = new JButton("确定");
		cancelBtn = new JButton("取消");
		
		btnPanel.add(okBtn);
		btnPanel.add(cancelBtn);
	}

	/**
	 * 初始化用户选择列面板
	 */
	@SuppressWarnings("rawtypes")
	private void initUserSelectColumnPanel() {
		userSelectColumnPanel = new JPanel();
		userSelectColumnPanel.setLayout(new GridBagLayout());
		userSelectColumnPanel.setBorder(BorderUtils.createEmsBorder("列"));
		
		List<String> selectColumns = SwingConstants.getSelectColumns().get("    用户");
		userSelectColumnList = new JCheckList(getCheckListItemData(selectColumns));
		JScrollPane selectColumnScrollPane = new JScrollPane(userSelectColumnList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		selectColumnScrollPane.getViewport().setPreferredSize(new Dimension(200, 0));
		userMoveUpButton = new JButton("上移");
		userMoveDownButton = new JButton("下移");
		userAllSelectButton = new JButton("全选");
		userAllDisSelectButton = new JButton("全部取消选择");
		
		userSelectColumnPanel.add(selectColumnScrollPane, new GridBagConstraints(0, 0, 1, 5, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		userSelectColumnPanel.add(userMoveUpButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		userSelectColumnPanel.add(userMoveDownButton, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		userSelectColumnPanel.add(userAllSelectButton, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		userSelectColumnPanel.add(userAllDisSelectButton, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
	}
	
	/**
	 * 初始化列表项目
	 * @return
	 */
	private CheckListItem[] getCheckListItemData(List<String> listData) {
		CheckListItem[] checkListItems = new CheckListItem[listData.size()];
		for(int i = 0 ;i < listData.size(); i++){
			checkListItems[i] = new CheckListItem(listData.get(i));
			checkListItems[i].setSelected(true);
		}
		return checkListItems;
		
	}

	/**
	 * 初始化选择列树滚动面板
	 */
	private void initSelectTreePanel() {
		selectTreePanel = new JPanel(new BorderLayout());
		selectList = new JList();
		selectList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		selectList.setListData(SwingConstants.getSelectColumns().keySet().toArray());
		
		JScrollPane jSelectTreeScrollPane = new JScrollPane(selectList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JViewport selectTreeViewport = jSelectTreeScrollPane.getViewport();
		selectTreeViewport.setPreferredSize(new Dimension(150, 0));
		
		selectTreePanel.add(jSelectTreeScrollPane, BorderLayout.CENTER); 
	}

	/**
	 * 添加事件监听
	 */
	private void initListener() {
		selectList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent lse) {
				cardLayout.show(selectColumnPanel, selectList.getSelectedValue().toString());
			}
		});
		
		Iterator<Entry<JButton, JCheckList>> iterAllSelectButtonMap = allSelectButtonMap.entrySet().iterator();
		while (iterAllSelectButtonMap.hasNext()) {
			final Entry<JButton, JCheckList> nextAllSelectButtonMap = iterAllSelectButtonMap.next();
			nextAllSelectButtonMap.getKey().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setListValue(nextAllSelectButtonMap.getValue(), true);
				}
			});
		}
		
		Iterator<Entry<JButton, JCheckList>> iterAllDisSelectButtonMap = allDisSelectButtonMap.entrySet().iterator();
		while (iterAllDisSelectButtonMap.hasNext()) {
			final Entry<JButton, JCheckList> nextAllDisSelectButtonMap = iterAllDisSelectButtonMap.next();
			nextAllDisSelectButtonMap.getKey().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setListValue(nextAllDisSelectButtonMap.getValue(), false);
				}
			});
		}
		
		okBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectColumnDialog.dispose();
			}
		});
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectColumnDialog.dispose();
			}
		});
	}

	/**
	 * 添加面板组件到界面
	 */
	private void addViewComponent() {
		setLayout(new GridBagLayout());
		
		add(selectTreePanel, new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
				new Insets(10, 10, 5, 5), 0, 0));
		add(selectColumnPanel, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 0, 0));
		add(btnPanel, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0));
	}
	
	/**
	 * 带复选框的全选和取消全选响应
	 * @param jCheckList
	 * @param flag
	 */
	@SuppressWarnings("rawtypes")
	private void setListValue(JCheckList jCheckList, boolean flag) {
		CheckListItem[] checkListItemDatas = jCheckList.getCheckListItemData();
		for(CheckListItem checkListItemData:checkListItemDatas){
			checkListItemData.setSelected(flag);
		}
		jCheckList.updateUI();
	}


}















