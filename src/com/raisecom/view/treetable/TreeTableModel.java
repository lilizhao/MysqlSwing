package com.raisecom.view.treetable;

import org.jdesktop.swingx.treetable.AbstractTreeTableModel;

public class TreeTableModel extends AbstractTreeTableModel {
	private TreeNode connRoot;

	public TreeTableModel() {
		connRoot = new TreeNode("root");
		
		

		TreeNode baseConfig = new TreeNode("基本配置");
		TreeNode rfc1213 = new TreeNode("RFC1213");
		
		rfc1213.getChildren().add(new TreeNode("系统描述"));
		rfc1213.getChildren().add(new TreeNode("联系人"));
		rfc1213.getChildren().add(new TreeNode("系统位置"));
		rfc1213.getChildren().add(new TreeNode("系统名称"));
		baseConfig.getChildren().add(rfc1213);

		baseConfig.getChildren().add(new TreeNode("系统信息"));
		baseConfig.getChildren().add(new TreeNode("系统时间"));
		baseConfig.getChildren().add(new TreeNode("风暴抑制"));
		connRoot.getChildren().add(baseConfig);
	}

	public int getColumnCount() {
		return 1;
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
		case 0:
			return "连接";
		default:
			return "Unknown";
		}
	}

	public Object getValueAt(Object node, int column) {
		TreeNode treenode = (TreeNode) node;
		switch (column) {
		case 0:
			return treenode.getName();
		default:
			return "Unknown";
		}
	}

	public Object getChild(Object node, int index) {
		TreeNode treenode = (TreeNode) node;
		return treenode.getChildren().get(index);
	}

	public int getChildCount(Object parent) {
		TreeNode treenode = (TreeNode) parent;
		return treenode.getChildren().size();
	}

	public int getIndexOfChild(Object parent, Object child) {
		TreeNode treenode = (TreeNode) parent;
		for (int i = 0; i > treenode.getChildren().size(); i++) {
			if (treenode.getChildren().get(i) == child) {
				return i;
			}
		}

		return 0;
	}

	public boolean isLeaf(Object node) {
		TreeNode treenode = (TreeNode) node;
		if (treenode.getChildren().size() > 0) {
			return false;
		}
		return true;
	}

	public Object getRoot() {
		return connRoot;
	}

	public TreeNode getConnRoot() {
		return connRoot;
	}

	
	
}
