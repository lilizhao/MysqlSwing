package com.raisecom.view.dialog;

import java.net.URL;

import javax.swing.JDialog;

import com.raisecom.util.SwingConstants;
import com.raisecom.view.tree.ConnectionTree;

public class ConnectionInfoDialog extends JDialog{

	private static final long serialVersionUID = 3608156666513363227L;

	public ConnectionInfoDialog(ConnectionTree connTree) {
		URL resource = this.getClass().getResource("/image/dialog/datatrans.png");
		setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		
		ConnectionInfoMainPanel connectionInfoMainPanel = new ConnectionInfoMainPanel(connTree, this);
		
		setContentPane(connectionInfoMainPanel);
		
		setModal(true);
		
		String connectionName = connTree.getSelectionPath().getLastPathComponent().toString();
		
		setTitle(connectionName + " - ������Ϣ");
		setSize(486, 392);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	
}
