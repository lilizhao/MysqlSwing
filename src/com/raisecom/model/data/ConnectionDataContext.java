package com.raisecom.model.data;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

public class ConnectionDataContext {
	
	private Map<String, ServiceContext> connectionDataContext = new HashMap<String, ServiceContext>();;
	
//	private ServiceContext serviceContext;

	public ConnectionDataContext() {
		try {
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(System.getProperty("user.dir")+"\\config\\connections.ncx");
			Element rootEle = document.getRootElement();
			
			@SuppressWarnings("unchecked")
			List<Element> allConnEles = XPath.selectNodes(rootEle, "/Connections/Connection");
			
			Iterator<Element> iterAllConnEles = allConnEles.iterator();
			while(iterAllConnEles.hasNext()){
				Element nextEle = iterAllConnEles.next();
				String connectionName = nextEle.getAttributeValue("ConnectionName");
				
				ServiceContext serviceContext = new ServiceContext(nextEle);
				
				connectionDataContext.put(connectionName, serviceContext);
			}
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Map<String, ServiceContext> getConnectionDataContext() {
		return connectionDataContext;
	}
	
	/**
	 * 为新连接名增加新的连接服务
	 * @param connectionName
	 * @param serviceContext
	 */
	public void addConnectionDataContext(String connectionName,ServiceContext serviceContext){
		connectionDataContext.put(connectionName, serviceContext);
	}
	
	/**
	 * 删除指定的连接服务
	 * @param connectionName
	 */
	public void deleteConnectionDataContext(String connectionName){
		connectionDataContext.remove(connectionName);
	}
	
	/**
	 * 获取指定连接名的服务上下文
	 * @param connectionName
	 * @return
	 */
	public ServiceContext getServiceContextByConnectionName(String connectionName){
		return connectionDataContext.get(connectionName);
	}

}





















