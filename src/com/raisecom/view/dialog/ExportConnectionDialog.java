package com.raisecom.view.dialog;

import javax.swing.JDialog;

public class ExportConnectionDialog extends JDialog{  

	private static final long serialVersionUID = 3608156666513363227L;

	public ExportConnectionDialog() {
		
		ExportConnectionDialogMainPanel exportConnectionDialogMainPanel = new ExportConnectionDialogMainPanel(this);
		
		setContentPane(exportConnectionDialogMainPanel);
		
		setModal(true);
		
		setTitle("��������");
		setSize(406,435);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	
}
