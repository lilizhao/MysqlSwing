package com.raisecom.view.frame;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.tree.TreePath;

import com.raisecom.view.menubar.MainMenuBar;
import com.raisecom.view.panel.SearchCreateTabPanel;
import com.raisecom.view.toolbar.TableToolBar;
import com.raisecom.view.tree.ConnectionTree;

public class ExtraFrame extends JFrame {

	private static final long serialVersionUID = 2074791180970655033L;

	private ConnectionTree connTree;

	public ExtraFrame(ConnectionTree connTree) {
		this.connTree = connTree;

		initViewComponent();
		initFrame();
		showUI();
	}

	/**
	 * 初始化全部组件
	 */
	private void initViewComponent() {
		setLayout(new BorderLayout());
		
		setJMenuBar(new MainMenuBar("/config/menu/tablemenu.xml"));
		
		add(new TableToolBar(), BorderLayout.NORTH);
		add(new SearchCreateTabPanel(connTree), BorderLayout.CENTER);
	}
	
	/**
	 * 显示UI
	 */
	private void showUI() {
		URL url = this.getClass().getResource("/image/frame/title.png");
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(url));
		this.setSize(768, 594);

		int pw = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		int w = this.getWidth();
		int ph = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		int h = this.getHeight();
		setLocation((pw - w) / 2, (ph - h) / 2);

		this.setVisible(true);
	}

	private void initFrame() {
		TreePath selectedNodePath = connTree.getSelectionPaths()[0];
		String selectedRootNode = selectedNodePath.getPath()[1].toString();
		String selectedDataBaseNode = selectedNodePath.getPath()[2].toString();
		String selectedNodeType = selectedNodePath.getPath()[3].toString();
		String selectedLeafNode = selectedNodePath.getLastPathComponent()
				.toString();

		String title = selectedLeafNode + " @" + selectedDataBaseNode + " ("
				+ selectedRootNode + ") - " + selectedNodeType;

		setTitle(title);

		this.addWindowListener(new ExtraWindowsAdapter(this));
	}
	
	
	private class ExtraWindowsAdapter extends WindowAdapter {
		private ExtraFrame frame;

		public ExtraWindowsAdapter(ExtraFrame extraFrame) {
			this.frame = extraFrame;
		}

		public void windowClosed(WindowEvent e) {
			this.frame.dispose();
		}
	}

}
