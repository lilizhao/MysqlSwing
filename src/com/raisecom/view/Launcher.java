package com.raisecom.view;

import com.raisecom.view.frame.MainFrame;

/**
 * 程序发起类
 * @author lilz-2686
 *
 */
public class Launcher {
	
	public static void main(String[] args) {
		//获取开始时间
		long startTime=System.currentTimeMillis();  
		
		MainFrame.main(null);
		
		 //获取结束时间
		long endTime=System.currentTimeMillis();
		System.out.println("工具启动用时： " + (endTime-startTime)*1.0/1000 + "s");	
	}
}


















