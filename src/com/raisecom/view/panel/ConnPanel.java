package com.raisecom.view.panel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.IOException;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

import com.raisecom.view.tree.ConnectionNameNode;
import com.raisecom.view.tree.ConnectionTree;

/**
 * 功能描述：连接属性面板
 * @author lilz-2686
 *
 */
public class ConnPanel extends JPanel{
	
	private static final long serialVersionUID = -2080340629571068814L;
	
	private JScrollPane scrollPane;
	private JPanel titlePanel;
	
	private JLabel connectionLabel;
	
	public ConnPanel(ConnectionTree connTree) {
		initTitlePanel();
		initConnTree(connTree);
		
		initViewComponent();
	}


	/**
	 * 初始化Title Panel界面
	 */
	private void initTitlePanel() {
		titlePanel = new JPanel();
		titlePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		connectionLabel = new JLabel("连接");
		
		titlePanel.add(connectionLabel);
	}

	/**
	 * 初始化组件树面板
	 * @param connTree
	 */
	private void initConnTree(ConnectionTree connTree) {
		
		DefaultMutableTreeNode rootNode=new DefaultMutableTreeNode("root"); 
		addConnectLeafForTree(rootNode);
		
		((DefaultTreeModel)connTree.getModel()).setRoot(rootNode);
		connTree.setEditable(false);
		connTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		connTree.putClientProperty("JTree.lineStyle" , "Horizontal");  
		connTree.setRootVisible(false);
	    
//	    BasicTreeUI ui = (BasicTreeUI)tree.getUI();
//	    ui.setExpandedIcon(null);
//	    ui.setCollapsedIcon(null);
	    
		scrollPane = new JScrollPane();
	    scrollPane.setViewportView(connTree);
	}
	
	/**
	 * 功能绘制
	 */
	private void initViewComponent() {
		setLayout(new BorderLayout());
		
		add(titlePanel,BorderLayout.NORTH);
		add(scrollPane,BorderLayout.CENTER);
	}
	
	/**
	 * 添加所有的连接节点
	 * @param rootNode
	 */
	private void addConnectLeafForTree(DefaultMutableTreeNode rootNode) {
		String connectionName;
		
		try {
			SAXBuilder saxBuilder = new SAXBuilder();
			Document document = saxBuilder.build(System.getProperty("user.dir") + "\\config\\connections.ncx");
			Element rootElement = document.getRootElement();
			@SuppressWarnings("unchecked")
			List<Element> allConnectionEles = XPath.selectNodes(rootElement, "/Connections/Connection");
			ListIterator<Element> listIterEles = allConnectionEles.listIterator();
			while(listIterEles.hasNext()){
				Element nextConnEle = listIterEles.next();
				connectionName = nextConnEle.getAttributeValue("ConnectionName");
				ConnectionNameNode connectionNameNode = new ConnectionNameNode(connectionName);
				
				DefaultMutableTreeNode connectionNameTreeNode = new DefaultMutableTreeNode(connectionNameNode);
				
				
				rootNode.add(connectionNameTreeNode);
			}
			
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

//	class MouseHandle extends MouseAdapter{
//	      public void mousePressed(MouseEvent e){
//	         try{
//	           JTree tree=(JTree)e.getSource();
//	           int rowLocation=tree.getRowForLocation(e.getX(),e.getY());
//	           TreePath treepath=tree.getPathForRow(rowLocation); 
//	           DefaultMutableTreeNode parentNode=(DefaultMutableTreeNode)(treepath.getLastPathComponent());
//	           nodeName=parentNode.toString();    
//	           
//	           DefaultTreeModel treeModel=(DefaultTreeModel)tree.getModel();
//	           treeModel.insertNodeInto(new DefaultMutableTreeNode("test"), parentNode, parentNode.getChildCount());
//	           
//	           System.out.println(nodeName);
//	         }catch(NullPointerException ne){}
//	      } 
//	    }

}
