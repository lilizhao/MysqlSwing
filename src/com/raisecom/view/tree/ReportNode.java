package com.raisecom.view.tree;

import javax.swing.ImageIcon;

import com.raisecom.util.ImageUtils;

public class ReportNode extends ConnectionNode{

	public ReportNode(String nodeName) {
		super(nodeName);
	}

	public ImageIcon getImageIcon() {
		return ImageUtils.REPORT_NODE_IMAGE;
	}

}
