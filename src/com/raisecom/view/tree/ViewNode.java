package com.raisecom.view.tree;

import javax.swing.ImageIcon;

import com.raisecom.util.ImageUtils;

public class ViewNode extends ConnectionNode{

	public ViewNode(String nodeName) {
		super(nodeName);
	}

	public ImageIcon getImageIcon() {
		return ImageUtils.VIEW_NODE_IMAGE;
	}

}
