package com.raisecom.view.dialog;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.jdom.Content;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

import com.raisecom.model.menu.MenuDataModel;
import com.raisecom.util.FileOrDirectoryChooser;
import com.raisecom.view.custom.CheckListItem;
import com.raisecom.view.custom.JCheckList;
import com.raisecom.view.custom.MyFileFilter;

public class ExportConnectionDialogMainPanel extends JPanel{
	
	private static final long serialVersionUID = -1261206938111837768L;
	
	private ExportConnectionDialog exportConnectionDialog = null;
	
	private JLabel connectionLabel = null;
	@SuppressWarnings("rawtypes")
	private JCheckList connectionList = null;
	private JButton allSelectButton = null;
	private JButton allDiselectButton = null;
	private JLabel connectionExportToLabel = null;
	private JTextField connectionExportToTextField = null;
	private JButton connectionExportToBroswerButton = null;
	private JButton okButton = null;
	private JButton cancelBtn = null;
	

	public ExportConnectionDialogMainPanel(ExportConnectionDialog exportConnectionDialog) {
		this.exportConnectionDialog = exportConnectionDialog;
		
		initComponentPanel();
		initLinsteners();
	}

	@SuppressWarnings("rawtypes")
	private void initComponentPanel() {
		setLayout(new GridBagLayout());
		
		connectionLabel = new JLabel("连接:");
		connectionList = new JCheckList(getCheckListItemData());
		connectionList.setBorder(new EmptyBorder(new Insets(0, 5, 5, 5)));
		allSelectButton = new JButton("全选");
		allDiselectButton = new JButton("全部取消选择");
		connectionExportToLabel = new JLabel("导出到:");
		connectionExportToTextField = new JTextField(System.getProperty("user.dir")+"\\profile\\connections.ncx");
		connectionExportToBroswerButton = new JButton("..");
		okButton = new JButton("确定");
		cancelBtn = new JButton("取消");
		
		add(connectionLabel,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		
		JScrollPane listScrollPane = new JScrollPane(connectionList,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		listScrollPane.setBorder(BorderFactory.createEtchedBorder());
		
		add(listScrollPane,new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));
		
		JPanel selectPanel = new JPanel();
		selectPanel.setLayout(new GridLayout(1, 3));
		selectPanel.add(allSelectButton);
		selectPanel.add(allDiselectButton);
		
		add(selectPanel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		add(connectionExportToLabel,new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		
		JPanel exportPanel = new JPanel(new GridBagLayout());
		exportPanel.add(connectionExportToTextField,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 0, 0, 5), 0, 0));
		exportPanel.add(connectionExportToBroswerButton,new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		
		add(exportPanel,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 20, 5), 0, 0));
		
		JPanel btnPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		btnPanel.add(okButton);
		btnPanel.add(cancelBtn);
		
		add(btnPanel,new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0));
		
	}


	/**
	 * 初始化列表项目
	 * @return
	 */
	private CheckListItem[] getCheckListItemData() {
		MenuDataModel menuDataModel = new MenuDataModel();
		
		Vector<String> listData = menuDataModel.getConecntionNameVectorData();
		
		CheckListItem[] checkListItems = new CheckListItem[listData.size()];
		for(int i = 0 ;i < listData.size(); i++){
			checkListItems[i] = new CheckListItem(listData.get(i));
			checkListItems[i].setSelected(true);
		}
		return checkListItems;
		
	}

	private void initLinsteners() {
		allSelectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setListValue(connectionList,true);
			}
			
		});
		allDiselectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setListValue(connectionList,false);
			}
		});
		connectionExportToBroswerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FileOrDirectoryChooser folderChooser = new FileOrDirectoryChooser(connectionExportToTextField.getText(),
						JFileChooser.FILES_ONLY,connectionExportToTextField);
				folderChooser.setAcceptAllFileFilterUsed(false);
				folderChooser.addChoosableFileFilter(new MyFileFilter(".ncx", "*.ncx"));
				folderChooser.showOpenDialog(exportConnectionDialog);
			}
		});
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				String filePath = connectionExportToTextField.getText();
				File saveToFile = new File(filePath);
				if(!saveToFile.getParentFile().exists()){
					JOptionPane.showMessageDialog(exportConnectionDialog,
							"系统找不到指定路径！","",JOptionPane.ERROR_MESSAGE);
				}else{
					try {
						//如果文件不存在就创建
						if(!saveToFile.exists()){
							saveToFile.createNewFile();
						}
						Element rootEle = new Element("Connections");
						rootEle.setAttribute("Ver", "1.1");
						
						SAXBuilder builder = new SAXBuilder();
						Document document = builder.build(System.getProperty("user.dir")+"\\config\\connections.ncx");
						Element rootElement = document.getRootElement();
						
						CheckListItem[] checkListItemDatas = connectionList.getCheckListItemData();
						for(CheckListItem checkListItemData:checkListItemDatas){
							if(checkListItemData.isSelected()){
								String connectionName = checkListItemData.toString();
								Element selectSingleEle = (Element) XPath.selectSingleNode(rootElement, "/Connections/Connection[@ConnectionName='"+connectionName+"']");
								rootEle.addContent(((Content) selectSingleEle.clone()).detach());
							}
						}
						
						FileOutputStream out = new FileOutputStream(saveToFile);
						Document doc = new Document(rootEle);    
					    XMLOutputter outputter = new XMLOutputter();        
					    //如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的    
					    Format f = Format.getPrettyFormat();    
					    outputter.setFormat(f);     
					    outputter.output(doc, out);    
					    out.close();  
					} catch (IOException e) {
						e.printStackTrace();
					} catch (JDOMException e) {
						e.printStackTrace();
					} finally {
						exportConnectionDialog.dispose();
					}
				}
			}
		});
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exportConnectionDialog.dispose();
			}
		});
	}
	
	/**
	 * 带复选框的全选和取消全选响应
	 * @param jCheckList
	 * @param flag
	 */
	@SuppressWarnings("rawtypes")
	private void setListValue(JCheckList jCheckList, boolean flag) {
		CheckListItem[] checkListItemDatas = jCheckList.getCheckListItemData();
		for(CheckListItem checkListItemData:checkListItemDatas){
			checkListItemData.setSelected(flag);
		}
		jCheckList.updateUI();
	}

}






