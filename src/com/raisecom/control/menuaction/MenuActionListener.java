package com.raisecom.control.menuaction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JMenuItem;

import com.raisecom.view.dialog.DataSynchDialog;
import com.raisecom.view.dialog.DataTransDialog;
import com.raisecom.view.dialog.ExportConnectionDialog;
import com.raisecom.view.dialog.HelpRegisterDialog;
import com.raisecom.view.dialog.ImportConnectionDialog;
import com.raisecom.view.dialog.NewConnectionDialog;
import com.raisecom.view.dialog.SelectColumnDialog;
import com.raisecom.view.dialog.StructureSynchDialog;
import com.raisecom.view.dialog.ToolOptionDialog;
import com.raisecom.view.frame.CommonFrame;
import com.raisecom.view.frame.MainFrame;

/**
 * 功能描述：为所有的菜单子项添加响应动作
 * 
 * @author lilz-2686
 * 
 */
public class MenuActionListener implements ActionListener {

	public void actionPerformed(ActionEvent ae) {
		Object source = ae.getSource(); 
		if(source instanceof JMenuItem){
			JMenuItem jMenuItem = (JMenuItem)source;
			String actionCommand = jMenuItem.getActionCommand();
			
			if ("frame.menu.file.newconnection".equals(actionCommand)){
				new NewConnectionDialog();
			} else if ("frame.menu.file.exportconnection".equals(actionCommand)){
				new ExportConnectionDialog();
			} else if ("frame.menu.file.importconnection".equals(actionCommand)){
				new ImportConnectionDialog();
			} else if ("frame.menu.file.close".equals(actionCommand)){
				MainFrame.getMainFrame().dispose();
			} else if ("frame.menu.file.exit".equals(actionCommand)){
				System.exit(0);
			} else if ("frame.menu.view.selectcolumn".equals(actionCommand)) {
				new SelectColumnDialog();
			} else if ("frame.menu.favourite.clear1".equals(actionCommand)){
				System.out.println("frame.menu.favourite.clear1");
			} else if ("frame.menu.favourite.clear2".equals(actionCommand)){
				System.out.println("frame.menu.favourite.clear2");
			} else if ("frame.menu.favourite.clear3".equals(actionCommand)){
				System.out.println("frame.menu.favourite.clear3");
			} else if ("frame.menu.favourite.clear4".equals(actionCommand)){
				System.out.println("frame.menu.favourite.clear4");
			} else if ("frame.menu.favourite.clear5".equals(actionCommand)){
				System.out.println("frame.menu.favourite.clear5");
			} else if ("frame.menu.favourite.clear6".equals(actionCommand)){
				System.out.println("frame.menu.favourite.clear6");
			} else if ("frame.menu.favourite.clear7".equals(actionCommand)){
				System.out.println("frame.menu.favourite.clear7");
			} else if ("frame.menu.favourite.clear8".equals(actionCommand)){
				System.out.println("frame.menu.favourite.clear8");
			} else if ("frame.menu.favourite.clear9".equals(actionCommand)){
				System.out.println("frame.menu.favourite.clear9");
			} else if ("frame.menu.favourite.clear0".equals(actionCommand)){
				System.out.println("frame.menu.favourite.clear0");
			} else if ("frame.menu.tool.datatrans".equals(actionCommand)){
				new DataTransDialog();
			} else if ("frame.menu.tool.datasynch".equals(actionCommand)){
				new DataSynchDialog();
			} else if ("frame.menu.tool.structuresynch".equals(actionCommand)){
				new StructureSynchDialog();
			} else if ("frame.menu.tool.command".equals(actionCommand)){
				System.out.println("frame.menu.tool.command");
			} else if ("frame.menu.tool.history".equals(actionCommand)){
				new CommonFrame();
			} else if ("frame.menu.tool.servermonitor".equals(actionCommand)){
//				new ServerMonitorFrame();
			} else if ("frame.menu.tool.option".equals(actionCommand)){
				new ToolOptionDialog();
			} else if ("frame.menu.nextwindow".equals(actionCommand)){
				System.out.println("frame.menu.nextwindow");
			} else if ("frame.menu.help.openhelp".equals(actionCommand)){
				executeTheCommond("cmd /c start "+ System.getProperty("user.dir") +"\\doc\\navicat.chm");
			} else if ("frame.menu.help.onlinedoc".equals(actionCommand)){
				executeTheCommond("explorer http://dev.mysql.com/doc/");
			} else if ("frame.menu.help.register".equals(actionCommand)){
				new HelpRegisterDialog();
			} else if ("frame.menu.help.buynow".equals(actionCommand)){
				executeTheCommond("explorer http://www.navicat.com/store.html");
			} else if ("frame.menu.help.checkupdate".equals(actionCommand)){
				executeTheCommond("explorer http://www.navicat.com/checkupdate");
			} else if ("frame.menu.help.clientservice".equals(actionCommand)){
				executeTheCommond("explorer http://customer.navicat.com/");
			}
		}
	}

	/**
	 * 功能描述：打开文件或网站功能
	 * 
	 * @param command
	 */
	private void executeTheCommond(String command) {
		try {
			Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
