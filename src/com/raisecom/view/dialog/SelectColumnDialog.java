package com.raisecom.view.dialog;

import javax.swing.JDialog;

/**
 * 选择列对话框
 * @author Administrator
 *
 */
public class SelectColumnDialog extends JDialog{

	private static final long serialVersionUID = 3608156666513363227L;

	public SelectColumnDialog() {
		SelectColumnMainPanel runSqlMainPanel = new SelectColumnMainPanel(this);
		
		setContentPane(runSqlMainPanel);
		
		setModal(true);
		setResizable(false);
		
		setTitle("选择列");
		setSize(606, 472);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	
}
