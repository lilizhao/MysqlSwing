package com.raisecom.model.data;

import org.jdom.Element;

public class ServiceContext {
	
	private String hostOrIp; //主机或IP地址
	
	private String port; //端口
	
	private String userName; //用户名
	
	private String password; //密码
	
	private boolean isSavePassword; //保存密码
	

	public ServiceContext(Element serviceEle) {
		String host = serviceEle.getAttributeValue("Host");
		String port = serviceEle.getAttributeValue("Port");
		String userName = serviceEle.getAttributeValue("UserName");
		String password = serviceEle.getAttributeValue("Password");
		String savePassword = serviceEle.getAttributeValue("SavePassword");
		
		this.hostOrIp = host;
		this.port = port;
		this.userName = userName;
		this.password = password;
		this.isSavePassword = Boolean.valueOf(savePassword);
	}

	public String getHostOrIp() {
		return hostOrIp;
	}

	public void setHostOrIp(String hostOrIp) {
		this.hostOrIp = hostOrIp;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isSavePassword() {
		return isSavePassword;
	}

	public void setSavePassword(boolean isSavePassword) {
		this.isSavePassword = isSavePassword;
	}
	
	
	
}
