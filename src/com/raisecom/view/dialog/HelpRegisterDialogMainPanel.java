package com.raisecom.view.dialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

import com.raisecom.util.LimitedLengthDocument;
import com.raisecom.util.RegisterCodeCheck;

public class HelpRegisterDialogMainPanel extends JPanel{

	private static final long serialVersionUID = 3608156666513363227L;
	
	private HelpRegisterDialog helpRegisterDialog = null;
	
	private RegisterCodeCheck gegisterCodeCheck = null;
	
	private JPanel headPanel = null;
	private JPanel registerInfoPanel = null;;
	private JPanel btnPanel = null;
	
	private JLabel pleaseInputRegisterCodeLabel = null;
	private JLabel nameLabel = null;
	private JTextField nameTextField = null;
	private JLabel organizationLabel = null;
	private JTextField organizationTextField = null;
	private JLabel registerCodeLabel = null;
	
	private JTextField registerCodeTextField1 = null;
	private JLabel registerCodeLabel1 = null;
	private JTextField registerCodeTextField2 = null;
	private JLabel registerCodeLabel2 = null;
	private JTextField registerCodeTextField3 = null;
	private JLabel registerCodeLabel3 = null;
	private JTextField registerCodeTextField4 = null;
	private JLabel verificateLabel = null;
	
	private JButton makeSureBtn = null;
	private JButton cancelBtn = null;


	public HelpRegisterDialogMainPanel(HelpRegisterDialog helpRegisterDialog) {
		this.helpRegisterDialog = helpRegisterDialog;
		
		gegisterCodeCheck = new RegisterCodeCheck();
		
		initHeadPanel();
		initCenterPanel();
		initBtnPanel();
		
		addComponentPanel();
	}

	

	/**
	 * 功能描述：初始化表头面板
	 */
	private void initHeadPanel() {
		headPanel = new JPanel();
		headPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		pleaseInputRegisterCodeLabel = new JLabel("请在下面输入注册码。");
		
		headPanel.add(pleaseInputRegisterCodeLabel);
	}


	/**
	 * 功能描述：初始化注册信息对话框注册信息面板
	 */
	private void initCenterPanel() {
		HelpRegisterDocumentAdapter helpRegisterDocumentAdapter = new HelpRegisterDocumentAdapter();
		
		registerInfoPanel = new JPanel();
		registerInfoPanel.setLayout(new GridBagLayout());
		
		nameLabel = new JLabel("名:");
		nameTextField = new JTextField();
		organizationLabel = new JLabel("组织:");
		organizationTextField= new JTextField();
		registerCodeLabel = new JLabel("注册码:");
		
		JPanel registerCodePanel = new JPanel();
		
		registerCodeTextField1 = new JTextField(5);
		registerCodeLabel1 = new JLabel("-");
		registerCodeTextField2 = new JTextField(5);
		registerCodeLabel2 = new JLabel("-");
		registerCodeTextField3 = new JTextField(5);
		registerCodeLabel3 = new JLabel("-");
		registerCodeTextField4 = new JTextField(5);
		
		
		registerCodeTextField1.setDocument(new LimitedLengthDocument(registerCodeTextField1,4));
		registerCodeTextField2.setDocument(new LimitedLengthDocument(registerCodeTextField2,4));
		registerCodeTextField3.setDocument(new LimitedLengthDocument(registerCodeTextField3,4));
		registerCodeTextField4.setDocument(new LimitedLengthDocument(registerCodeTextField4,4));
		
		//初始化注册数据
		initCenterData();
		
		registerCodeTextField1.getDocument().addDocumentListener(helpRegisterDocumentAdapter);
		registerCodeTextField2.getDocument().addDocumentListener(helpRegisterDocumentAdapter);
		registerCodeTextField3.getDocument().addDocumentListener(helpRegisterDocumentAdapter);
		registerCodeTextField4.getDocument().addDocumentListener(helpRegisterDocumentAdapter);
		
		URL resource = null;
		if("".equals(registerCodeTextField1.getText())){
			resource = this.getClass().getResource("/image/dialog/wrong.png");
		}else{
			resource = this.getClass().getResource("/image/dialog/right.png");
		}
		
		verificateLabel = new JLabel(new ImageIcon(resource.getPath()));
		
		registerCodePanel.add(registerCodeTextField1);
		registerCodePanel.add(registerCodeLabel1);
		registerCodePanel.add(registerCodeTextField2);
		registerCodePanel.add(registerCodeLabel2);
		registerCodePanel.add(registerCodeTextField3);
		registerCodePanel.add(registerCodeLabel3);
		registerCodePanel.add(registerCodeTextField4);
		registerCodePanel.add(verificateLabel);
		
		
		registerInfoPanel.add(nameLabel,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 10, 0, 5), 0, 0));
		registerInfoPanel.add(nameTextField,new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 65, 0, 10), 0, 0));
		registerInfoPanel.add(organizationLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 10, 0, 5), 0, 0));
		registerInfoPanel.add(organizationTextField,new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 65, 0, 10), 0, 0));
		registerInfoPanel.add(registerCodeLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 10, 35, 5), 0, 0));
		registerInfoPanel.add(registerCodePanel,new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 60, 35, 5), 0, 0));
		
	}
	
	
	/**
	 * 功能描述：初始化中心注册信息
	 */
	private void initCenterData() {
		try {
			SAXBuilder saxBuilder = new SAXBuilder();
			Document document = saxBuilder.build(System.getProperty("user.dir")+"/config/gegister.xml");
			Element rootElement = document.getRootElement();
			Element registerEle = (Element) XPath.selectSingleNode(rootElement, "/gegister/information");
			String name = registerEle.getAttributeValue("name");
			String organization = registerEle.getAttributeValue("organization");
			String registrationcode = registerEle.getAttributeValue("registrationcode");
			
			nameTextField.setText(name);
			organizationTextField.setText(organization);
			
			if(registrationcode!=null && registrationcode.contains("-")){
				String[] registrationcodes = registrationcode.split("-");
				
				registerCodeTextField1.setText(registrationcodes[0]);
				registerCodeTextField2.setText(registrationcodes[1]);
				registerCodeTextField3.setText(registrationcodes[2]);
				registerCodeTextField4.setText(registrationcodes[3]);
			}
			
			
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * 功能描述：初始化底部按钮面板
	 */
	private void initBtnPanel() {
		HelpRegisterAdapter helpRegisterAdapter = new HelpRegisterAdapter();
		
		btnPanel = new JPanel();
		btnPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		makeSureBtn = new JButton("确定");
		
		if("".equals(registerCodeTextField1.getText())){
			makeSureBtn.setEnabled(false);
		}else{
			makeSureBtn.setEnabled(true);
		}
		
		cancelBtn = new JButton("取消");
		
		makeSureBtn.addActionListener(helpRegisterAdapter);
		cancelBtn.addActionListener(helpRegisterAdapter);
		
		btnPanel.add(makeSureBtn);
		btnPanel.add(cancelBtn);
		
	}
	
	/**
	 * 功能描述：添加注册信息面板和底部按钮面板到注册对话框主界面
	 */
	private void addComponentPanel() {
		setLayout(new BorderLayout());
		
		add(headPanel,BorderLayout.NORTH);
		add(registerInfoPanel,BorderLayout.CENTER);
		add(btnPanel,BorderLayout.SOUTH);
	}
	
	
	public void saveRegidterInfo() {
		
		try {
			SAXBuilder saxBuilder = new SAXBuilder();
			Document document = saxBuilder.build(System.getProperty("user.dir")+"/config/gegister.xml");
			Element rootElement = document.getRootElement();
			Element registerEle = (Element) XPath.selectSingleNode(rootElement, "/gegister/information");
			registerEle.setAttribute("name",nameTextField.getText());
			registerEle.setAttribute("organization",organizationTextField.getText());
			registerEle.setAttribute("registrationcode",
					registerCodeTextField1.getText()+"-"+registerCodeTextField2.getText()+"-"
				    +registerCodeTextField3.getText()+"-"+registerCodeTextField4.getText());
			
			XMLOutputter outp = new XMLOutputter();       
			FileOutputStream fileOutputStream = new FileOutputStream(System.getProperty("user.dir")+"/config/gegister.xml");
			outp.output(document, fileOutputStream);
			fileOutputStream.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			helpRegisterDialog.dispose();
		}

	}
	
	/**
	 * 功能描述：处理注册面板上按钮响应事件
	 * @author lilz-2686
	 *
	 */
	private final class HelpRegisterAdapter implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if(ae.getSource()==makeSureBtn){
				saveRegidterInfo();
			}else if(ae.getSource()==cancelBtn){
				helpRegisterDialog.dispose();
			}
		}
	}
	
	/**
	 * 功能描述：对注册码输入框进行监听
	 * @author lilz-2686
	 *
	 */
	private class HelpRegisterDocumentAdapter implements DocumentListener {
		public void removeUpdate(DocumentEvent documentevent) {
			verifyData();
		}

		public void insertUpdate(DocumentEvent documentevent) {
			verifyData();
		}

		public void changedUpdate(DocumentEvent documentevent) {
			verifyData();
		}

		private void verifyData() {
			String str1 = registerCodeTextField1.getText();
			String str2 = registerCodeTextField2.getText();
			String str3 = registerCodeTextField3.getText();
			String str4 = registerCodeTextField4.getText();
			
			StringBuilder stringBuilder = new StringBuilder();
			
			if(str1.length()==4 && str2.length()==4 && str3.length()==4 && str4.length()==4){
				stringBuilder = stringBuilder.append(str1).append("-").append(str2).append("-").append(str3).append("-").append(str4);
				boolean registerCodeverify = gegisterCodeCheck.registerCodeverify(stringBuilder.toString());
				
				if(registerCodeverify){
					makeSureBtn.setEnabled(true);
					URL resource = this.getClass().getResource("/image/dialog/right.png");
					verificateLabel.setIcon(new ImageIcon(resource.getPath()));
				}else{
					makeSureBtn.setEnabled(false);
					URL resource = this.getClass().getResource("/image/dialog/wrong.png");
					verificateLabel.setIcon(new ImageIcon(resource.getPath()));
				}
			}else{
				makeSureBtn.setEnabled(false);
				URL resource = this.getClass().getResource("/image/dialog/wrong.png");
				verificateLabel.setIcon(new ImageIcon(resource.getPath()));
			}
			
		}
	}

	
}
