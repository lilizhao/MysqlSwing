package com.raisecom.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import com.raisecom.model.data.JDBCConn;
import com.raisecom.util.SwingConstants;
import com.raisecom.view.custom.JAutoCompleteComboBox;
import com.raisecom.view.tree.ConnectionTree;
import com.raisecom.view.tree.DatabaseNode;

public class NewDatabaseMainPanel extends JPanel{

	private static final long serialVersionUID = 7736641963543831646L;
	
	private NewDatabaseDialog newDatabaseDialog;
	private ConnectionTree connTree;
	private String sourceTreeNode; //事件来源[是来自连接名节点的右键菜单，还是数据库名节点右键菜单[新建数据库,数据库属性]]
	
	private JTabbedPane tabbedPane;
	private JPanel commonPanel;
	private JPanel buttonPanel;
	
	private JLabel databaseNameLabel;
	private JTextField databaseNameTextField;
	private JLabel characterSetLabel;
	private JAutoCompleteComboBox characterSetComboBox;
	private JLabel arrangRuleLabel;
	private JAutoCompleteComboBox arrangRuleAutoCompleteComboBox;
	
	private JButton okButton;
	private JButton cancelButton;

	public NewDatabaseMainPanel(NewDatabaseDialog newDatabaseDialog, ConnectionTree connTree, String sourceTreeNode) {
		this.newDatabaseDialog = newDatabaseDialog;
		this.connTree = connTree;
		this.sourceTreeNode = sourceTreeNode;
		
		initJTabbedPane();
		initButtonPane();
		initListeners();
		
		addPaneComponent();
		setComponentEnable();
	}
	
	/**
	 * 设置组件使能与否
	 */
	private void setComponentEnable() {
		//如果是来自数据库节点右键数据库属性菜单
		if("databaseproperties".equals(sourceTreeNode)){
			tabbedPane.setEnabled(false);
			databaseNameTextField.setText(connTree.getSelectionPath().getLastPathComponent().toString());
			databaseNameLabel.setEnabled(false);
			databaseNameTextField.setEnabled(false);
			characterSetLabel.setEnabled(false);
			characterSetComboBox.setEnabled(false);
			arrangRuleLabel.setEnabled(false);
			arrangRuleAutoCompleteComboBox.setEnabled(false);
			
			okButton.setEnabled(false);
			cancelButton.setEnabled(false);
		}
	}

	/**
	 * 初始化界面组件监听
	 * @param connTree 
	 */
	private void initListeners() {
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				boolean flag = false;  //标记是否有相同数据库节点
				String newDatabaseName = databaseNameTextField.getText().trim();
				JDBCConn jdbcConn;
				int resultRows;
				
				int location = 0;
				
				if(!"".equals(newDatabaseName)){
					TreePath selectionConnectionTreePath = connTree.getSelectionPath();
					
					DefaultMutableTreeNode selectConnectionTreeNode = (DefaultMutableTreeNode) selectionConnectionTreePath.getLastPathComponent();
					
					//如果是来自数据库节点右键菜单新建数据库时添加兼容模式
					if("databasetreenode".equals(sourceTreeNode)){
						selectConnectionTreeNode = (DefaultMutableTreeNode) selectionConnectionTreePath.getParentPath().getLastPathComponent();
					}
					
					int childCount = selectConnectionTreeNode.getChildCount();
					
					//判断是否有相同节点
					for(int i=0;i<childCount;i++){
						DefaultMutableTreeNode nextChildTreeNode = (DefaultMutableTreeNode) selectConnectionTreeNode.getChildAt(i);
						if(newDatabaseName.equals(nextChildTreeNode.getUserObject().toString())){
							flag = true;
						}
						if(newDatabaseName.compareTo(nextChildTreeNode.getUserObject().toString()) > 0){
							location = i;
						}
					}
					
					if(flag == false){
						Connection connection = JDBCConn.allConnections.get(selectConnectionTreeNode.getUserObject().toString());
						
						jdbcConn = new JDBCConn();
						
						try {
							resultRows = jdbcConn.executeUpdate(connection, "CREATE DATABASE "+ newDatabaseName +"");
							if(resultRows > 0){
								DefaultMutableTreeNode newDatabaseTreeNode = new DefaultMutableTreeNode(new DatabaseNode(newDatabaseName));
								
								selectConnectionTreeNode.insert(newDatabaseTreeNode, location+1);
								connTree.setSelectionPath(new TreePath(newDatabaseTreeNode.getPath()));
								
								connTree.updateUI();
								newDatabaseDialog.dispose();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						
					}else{
						JOptionPane.showMessageDialog(null,"1007 - Can't create database '"+ newDatabaseName +"'; database exists","",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				newDatabaseDialog.dispose();
			}
		});
	}

	/**
	 * 添加界面组件到面板
	 */
	private void addPaneComponent() {
		this.setLayout(new BorderLayout());
		
		add(tabbedPane, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * 初始化底部按钮面板
	 */
	private void initButtonPane() {
		buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		okButton = new JButton("确认");
		okButton.setBorder(new EmptyBorder(new Insets(2, 30, 5, 30)));
		cancelButton = new JButton("取消");
		cancelButton.setBorder(new EmptyBorder(new Insets(2, 30, 5, 30)));
		
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
	}

	/**
	 * 初始化界面组件
	 */
	private void initJTabbedPane() {
		tabbedPane = new JTabbedPane();
		tabbedPane.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
		
		JPanel tabMainPanel = new JPanel();
		tabMainPanel.setLayout(new BorderLayout());
		tabMainPanel.setBackground(new Color(251, 251, 251));
		
		commonPanel = new JPanel();
		commonPanel.setOpaque(false);
		commonPanel.setLayout(new GridBagLayout());
		
		databaseNameLabel = new JLabel("数据库名:");
		databaseNameTextField = new JTextField();
		
		characterSetLabel = new JLabel("字符集:");
		characterSetComboBox = new JAutoCompleteComboBox(SwingConstants.getAllCharset());
		characterSetComboBox.setSelectedItem("UTF-8");
		arrangRuleLabel = new JLabel("排列规则:");
		arrangRuleAutoCompleteComboBox = new JAutoCompleteComboBox();
		
		commonPanel.add(databaseNameLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 10, 5, 10), 0, 0));
		commonPanel.add(databaseNameTextField, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 100, 5, 10), 0, 0));
		commonPanel.add(characterSetLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(15, 10, 5, 10), 0, 0));
		commonPanel.add(characterSetComboBox, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(15, 100, 5, 10), 0, 0));
		commonPanel.add(arrangRuleLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 10, 5, 10), 0, 0));
		commonPanel.add(arrangRuleAutoCompleteComboBox, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(2, 100, 5, 10), 0, 0));
		
		tabMainPanel.add(commonPanel,BorderLayout.NORTH);
		tabbedPane.add(" 常规  ", tabMainPanel);
	}
	
}













