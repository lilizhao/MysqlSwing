package com.raisecom.view.dialog;

import java.net.URL;

import javax.swing.JDialog;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import com.raisecom.util.SwingConstants;
import com.raisecom.view.tree.ConnectionTree;
import com.raisecom.view.tree.TableNode;

/**
 * 在数据库中查找对话框
 * @author Administrator
 *
 */
public class SearchInDatabaseDialog extends JDialog {

	private static final long serialVersionUID = -4622010253617457185L;

	public SearchInDatabaseDialog(ConnectionTree connTree) {
		URL resource = this.getClass().getResource("/image/dialog/datasynch.png");
		TreePath selectionDatabasePath;
		
		setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		
		SearchInDatabaseMainPanel databaseMainPanel = new SearchInDatabaseMainPanel();
		
		setContentPane(databaseMainPanel);
		
		setModal(false);
		setResizable(true);
		
		Object userObject = ((DefaultMutableTreeNode)connTree.getSelectionPath().getLastPathComponent()).getUserObject();
		
		if (userObject instanceof TableNode) {
			selectionDatabasePath = connTree.getSelectionPath().getParentPath();
		} else {
			selectionDatabasePath = connTree.getSelectionPath();
		}
		
		setTitle(selectionDatabasePath.getLastPathComponent() 
				+ " (" + selectionDatabasePath.getParentPath().getLastPathComponent() + ")"
				+ " - 在数据库中查找");
		setSize(768,594);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
