package com.raisecom.control.listaction;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.JList;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.raisecom.view.popup.ListNodePopupMenu;
import com.raisecom.view.tree.ConnectionNode;
import com.raisecom.view.tree.EventNode;
import com.raisecom.view.tree.FunctionNode;
import com.raisecom.view.tree.TableNode;
import com.raisecom.view.tree.ViewNode;

/**
 * 列表数据点击事件
 * @author lilz-2686
 *
 */
public class DataListMouseAdapter extends MouseAdapter{
	private JList dataList;
	private Element rootPopupElement;
	private ListNodePopupMenu listNodePopupMenu;

	public DataListMouseAdapter(JList dataList) {
		this.dataList = dataList;
		
		getPopupMenuRootElement();
	}

	/**
	 * 获取弹出菜单配置文件
	 */
	private void getPopupMenuRootElement() {
		try {
			SAXBuilder saxBuilder = new SAXBuilder();
			Document document = saxBuilder.build(this.getClass().getResource("/config/popup/popupmenu.xml"));
			
			rootPopupElement = document.getRootElement();
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void mouseReleased(MouseEvent me) {
		int index = dataList.locationToIndex(me.getPoint());
		dataList.setSelectedIndex(index);
		
		if (me.isPopupTrigger()) {
			ConnectionNode selectedValue = (ConnectionNode) dataList.getSelectedValue();
			
			if (selectedValue.getClass() == TableNode.class) {
				lidataMouseAdapter(me, selectedValue, "table");
			} else if (selectedValue.getClass() == ViewNode.class) {
				lidataMouseAdapter(me, selectedValue, "view");
			} else if (selectedValue.getClass() == FunctionNode.class) {
				lidataMouseAdapter(me, selectedValue, "function");
			} else if (selectedValue.getClass() == EventNode.class) {
				lidataMouseAdapter(me, selectedValue, "event");
			}
		}
	}

	private void lidataMouseAdapter(MouseEvent me, ConnectionNode selectedValue, String listType) {
		listNodePopupMenu = new ListNodePopupMenu(rootPopupElement, selectedValue, listType);
		listNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
	}

}
