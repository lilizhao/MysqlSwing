package com.raisecom.view.popup;

import java.net.URL;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

import com.raisecom.control.popupaction.PopupMenuActionListener;
import com.raisecom.view.tree.ConnectionNode;
import com.raisecom.view.tree.ConnectionTree;

public class CommonNodePopupMenu extends JPopupMenu{

	private static final long serialVersionUID = 432046809807338904L;
	
	private String menuName;
	private String menuItemName;
	
	private String nodeState;
	private String actionCommand;
	
	private JMenu menu;
	private JMenuItem menuItem;
	private String imagepath;
	
	private URL url;
	private ImageIcon imageIcon;

	/**
	 * 常规节点右键弹出菜单构造方法
	 * @param connTree 
	 * @param rootPopupElement 存在弹出菜单配置文件的根元素
	 * @param connectionNode 选中的节点
	 * @param connectNodeType 选中节点的类型
	 */
	public CommonNodePopupMenu(ConnectionTree connTree, Element rootPopupElement, ConnectionNode connectionNode, String connectNodeType) {
		try {
			Element conncectEle = (Element) XPath.selectSingleNode(rootPopupElement, "/menus/" + connectNodeType);
			
			@SuppressWarnings("unchecked")
			List<Element> childEles = conncectEle.getChildren();
			Iterator<Element> iterChildEles = childEles.iterator();
			while(iterChildEles.hasNext()){
				Element nextConnectMenuEle = iterChildEles.next();
				
				String menuOrMenuItem = nextConnectMenuEle.getName();
				
				if("menuitem".equals(menuOrMenuItem)){   //弹出菜单项
					addPopupMenuItem(connTree, nextConnectMenuEle, connectionNode);
				}else if("menu".equals(menuOrMenuItem)){  //弹出菜单项
					addPopupMenu(connTree, nextConnectMenuEle, connectionNode);
				}
			}
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 添加弹出菜单
	 * @param connTree
	 * @param nextConnectMenuEle
	 * @param connectionNode
	 */
	private void addPopupMenu(ConnectionTree connTree, Element nextConnectMenuEle, ConnectionNode connectionNode) {
		menuName = nextConnectMenuEle.getAttributeValue("name");
		imagepath = nextConnectMenuEle.getAttributeValue("imagepath");
		actionCommand = nextConnectMenuEle.getAttributeValue("actioncommand");
		
		menu = new JMenu(menuName);
		
		if(imagepath!=null){
			url = this.getClass().getResource(imagepath);
			imageIcon = new ImageIcon(url.getPath());
			menu.setIcon(imageIcon);
		}
		
		@SuppressWarnings("unchecked")
		List<Element> menuitemEles = nextConnectMenuEle.getChildren();
		Iterator<Element> iterMenuItemEles = menuitemEles.iterator();
		while(iterMenuItemEles.hasNext()){
			Element nextMenuItemEle = iterMenuItemEles.next();
			
			menuItemName = nextMenuItemEle.getAttributeValue("name");
			imagepath = nextMenuItemEle.getAttributeValue("imagepath");
			nodeState = nextMenuItemEle.getAttributeValue("state");
			
			if(!"|".equals(menuItemName)){
				
				menuItem = new JMenuItem(menuItemName);
				
				if(imagepath!=null){
					url = this.getClass().getResource(imagepath);
					imageIcon = new ImageIcon(url.getPath());
					menuItem.setIcon(imageIcon);
				}
				
				if(connectionNode.getState().equalsIgnoreCase(nodeState)){
					menuItem.setEnabled(false);
				}
				if(actionCommand!=null && !"".equals(actionCommand)){
					menuItem.setActionCommand(actionCommand);
				}
				
				menuItem.addActionListener(new PopupMenuActionListener(connTree, connectionNode));
				
				menu.add(menuItem);
			}else{
				menu.addSeparator();
			}
		}
		
		add(menu);
	}

	/**
	 * 添加弹出菜单项
	 * @param connTree 
	 * @param nextConnectMenuEle
	 */
	private void addPopupMenuItem(ConnectionTree connTree, Element nextConnectMenuEle, ConnectionNode connectionNode) {
		menuItemName = nextConnectMenuEle.getAttributeValue("name");
		imagepath = nextConnectMenuEle.getAttributeValue("imagepath");
		nodeState = nextConnectMenuEle.getAttributeValue("state");
		actionCommand = nextConnectMenuEle.getAttributeValue("actioncommand");
		
		if(!"|".equals(menuItemName)){
			
			menuItem = new JMenuItem(menuItemName);
			
			if(imagepath!=null){
				url = this.getClass().getResource(imagepath);
				
				if(url!=null){
					imageIcon = new ImageIcon(url.getPath());
					menuItem.setIcon(imageIcon);
				}
			}
			
			if(connectionNode.getState().equalsIgnoreCase(nodeState)){
				menuItem.setEnabled(false);
			}
			if(actionCommand!=null && !"".equals(actionCommand)){
				menuItem.setActionCommand(actionCommand);
			}
			
			menuItem.addActionListener(new PopupMenuActionListener(connTree, connectionNode));
			
			add(menuItem);
		}else{
			addSeparator();
		}
	}

}
