package com.raisecom.view.tree;

import javax.swing.ImageIcon;

import com.raisecom.util.ImageUtils;

public class SearchNode extends ConnectionNode{

	public SearchNode(String nodeName) {
		super(nodeName);
	}

	public ImageIcon getImageIcon() {
		return ImageUtils.SEARCH_NODE_IMAGE;
	}

}
