package com.raisecom.control.popupaction;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

import com.raisecom.model.data.JDBCConn;
import com.raisecom.model.data.ServiceContext;
import com.raisecom.util.OperationUtils;
import com.raisecom.util.SwingConstants;
import com.raisecom.view.dialog.ConnectionInfoDialog;
import com.raisecom.view.dialog.DataTransDialog;
import com.raisecom.view.dialog.NewConnectionDialog;
import com.raisecom.view.dialog.NewDatabaseDialog;
import com.raisecom.view.dialog.RunSqlDialog;
import com.raisecom.view.dialog.SearchInDatabaseDialog;
import com.raisecom.view.frame.ExtraFrame;
import com.raisecom.view.tree.ConnectionNameNode;
import com.raisecom.view.tree.ConnectionNode;
import com.raisecom.view.tree.ConnectionTree;
import com.raisecom.view.tree.DatabaseNode;

/**
 * 为所有的右键菜单项添加响应事件
 * @author lilz-2686
 *
 */
public class PopupMenuActionListener implements ActionListener{
	private ConnectionTree connTree;
	private ConnectionNode connectionNode;
	
	public PopupMenuActionListener(ConnectionTree connTree, ConnectionNode connectionNode) {
		this.connTree = connTree;
		this.connectionNode = connectionNode;
	}

	public void actionPerformed(ActionEvent ae) {
		Object source = ae.getSource(); 
		if(source instanceof JMenuItem){
			JMenuItem jMenuItem = (JMenuItem)source;
			String actionCommand = jMenuItem.getActionCommand();
			
			//处理连接节点右键弹出菜单的响应
			if ("menus.connection.openconnection".equals(actionCommand)) {
				openConnectionAction(connectionNode);
			} else if ("menus.connection.closeconnection".equals(actionCommand)) {
				closeConnectionAction(connectionNode);
			} else if ("menus.connection.newconnection".equals(actionCommand)) {
				new NewConnectionDialog();
			} else if ("menus.connection.copyconnection".equals(actionCommand)) {
				new NewConnectionDialog(connectionNode, OperationUtils.COPY);
			} else if ("menus.connection.deleteconnection".equals(actionCommand)) {
				deleteConnectionAction();
			} else if ("menus.connection.connectionproperties".equals(actionCommand)) {
				new NewConnectionDialog(connectionNode, OperationUtils.VIEW);
			} else if ("menus.connection.opendatabase".equals(actionCommand)) {
				openDatabaseBasedOnConnectionNameNode();
			} else if ("menus.connection.newdatabase".equals(actionCommand)) {
				new NewDatabaseDialog(connTree, "connectiontreenode");
			} else if ("menus.connection.runsql".equals(actionCommand)) {
				new RunSqlDialog(connTree);
			} else if ("menus.connection.databasetrans".equals(actionCommand)) {
				new DataTransDialog();
			} else if ("menus.connection.connectioninfo".equals(actionCommand)) {
				new ConnectionInfoDialog(connTree);
			}

			// 处理具体数据库节点右键弹出菜单的响应
			if ("menus.database.opendatabase".equals(actionCommand)) {
				openDatabseBasedOnDatabaseNameNode();
			} else if ("menus.database.closedatabase".equals(actionCommand)) {
				closeDatabseBasedOnDatabseNameNode();
			} else if ("menus.database.newdatabase".equals(actionCommand)) {
				new NewDatabaseDialog(connTree, "databasetreenode");
			} else if ("menus.database.deletedatabase".equals(actionCommand)) {
				deleteDatabseBaseOnDatabaseNameNode();
			} else if ("menus.database.databaseproperties".equals(actionCommand)) {
				new NewDatabaseDialog(connTree, "databaseproperties");
			} else if ("menus.database.runsql".equals(actionCommand)) {
				new RunSqlDialog(connTree);
			} else if ("menus.database.databasetrans".equals(actionCommand)) {
				new DataTransDialog();
			} else if ("menus.database.searchindatabase".equals(actionCommand)) {
				new SearchInDatabaseDialog(connTree);
			}

			// 处理表组节点右键弹出菜单的响应
			if ("menus.tablegroup.runsql".equals(actionCommand)) {
				new RunSqlDialog(connTree);
			} else if ("menus.tablegroup.searchindatabase".equals(actionCommand)) {
				new SearchInDatabaseDialog(connTree);
			}
			
			// 处理具体表节点右键弹出菜单的响应
			if ("menus.table.opentable".contains(actionCommand)) {
				new ExtraFrame(connTree);
			}
		}
		
	}

	/**
	 * 具体数据库节点上的删除数据库菜单响应事件
	 */
	private void deleteDatabseBaseOnDatabaseNameNode() {
		JDBCConn jdbcConn;
		int resultRows;
		
		TreePath selectionDatabaseTreePath = connTree.getSelectionPath();
		DefaultMutableTreeNode selectDatabseTreeNode = (DefaultMutableTreeNode) selectionDatabaseTreePath.getLastPathComponent();
		DefaultMutableTreeNode selectConnectionTreeNode = (DefaultMutableTreeNode) selectionDatabaseTreePath.getParentPath().getLastPathComponent();
		
		int n = JOptionPane.showConfirmDialog(
                null, "你确定吗 ? 这将会删除数据库 " + selectDatabseTreeNode.getUserObject() + "。",
                "确认删除",JOptionPane.YES_NO_OPTION);
        if (n == JOptionPane.YES_OPTION) {
        	Connection connection = JDBCConn.allConnections.get(selectConnectionTreeNode.getUserObject().toString());
			
			jdbcConn = new JDBCConn();
			
			try {
				resultRows = jdbcConn.executeUpdate(connection, "DROP DATABASE "+ selectDatabseTreeNode.getUserObject() +"");
				
				if(resultRows == 0){
					selectDatabseTreeNode.removeFromParent();
//					selectConnectionTreeNode.remove(selectDatabseTreeNode);
					connTree.setSelectionPath(selectionDatabaseTreePath.getParentPath());
					connTree.updateUI();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
	}

	/**
	 * 具体数据库节点上的关闭数据库菜单响应事件
	 */
	private void closeDatabseBasedOnDatabseNameNode() {
		TreePath selPath = connTree.getSelectionPath();
		
		DefaultMutableTreeNode selectConnectionNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
		selectConnectionNode.removeAllChildren();
		
		DatabaseNode databaseNode = (DatabaseNode)selectConnectionNode.getUserObject();
		databaseNode.setState(OperationUtils.CLOSESTATE);
		
		connTree.updateUI();
	}

	/**
	 * 基于右键打开数据库菜单打开具体数据库
	 */
	private void openDatabseBasedOnDatabaseNameNode() {
		TreePath selPath = connTree.getSelectionPath();
		
		DefaultMutableTreeNode selectConnectionNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
		DatabaseNode databaseNode = (DatabaseNode)selectConnectionNode.getUserObject();
		
		List<DefaultMutableTreeNode> generalCommonNodes = SwingConstants.generalCommonNode();
		ListIterator<DefaultMutableTreeNode> listCommonNodes = generalCommonNodes.listIterator();
		while(listCommonNodes.hasNext()){
			DefaultMutableTreeNode nextCommonNode = listCommonNodes.next();
			
			selectConnectionNode.add(nextCommonNode);
		}
		databaseNode.setState(OperationUtils.OPENSTATE);
					
		connTree.expandPath(new TreePath(selectConnectionNode.getPath()));
		connTree.updateUI();
	}

	/**
	 * 基于具体数据库名称节点打开数据库
	 */
	private void openDatabaseBasedOnConnectionNameNode() {
		boolean flag = false;  //标志是否存在指定的数据库名
		
		String databaseName = (String)JOptionPane.showInputDialog(
                null, "输入数据库名", "打开数据库",
                JOptionPane.PLAIN_MESSAGE, null, null,"");
		
		TreePath selPath = connTree.getSelectionPath();
		
		DefaultMutableTreeNode selectConnectionNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
		
		int childCount = selectConnectionNode.getChildCount();
		
		for(int i=0;i<childCount;i++){
			DefaultMutableTreeNode nextTreeNode = (DefaultMutableTreeNode) selectConnectionNode.getChildAt(i);
			String treeNodeName = nextTreeNode.getUserObject().toString();
			DatabaseNode databaseNode = (DatabaseNode)nextTreeNode.getUserObject();
			
			if(treeNodeName.equals(databaseName)){
				
				if(OperationUtils.CLOSESTATE.equals(databaseNode.getState())){   //如果节点已经是打开状态，就不需要再添加了
					List<DefaultMutableTreeNode> generalCommonNodes = SwingConstants.generalCommonNode();
					ListIterator<DefaultMutableTreeNode> listCommonNodes = generalCommonNodes.listIterator();
					while(listCommonNodes.hasNext()){
						DefaultMutableTreeNode nextCommonNode = listCommonNodes.next();
						
						nextTreeNode.add(nextCommonNode);
					}
					databaseNode.setState(OperationUtils.OPENSTATE);
					
				}
				flag = true;
				
				connTree.setSelectionPath(new TreePath(nextTreeNode.getPath()));
				connTree.expandPath(new TreePath(nextTreeNode.getPath()));
				
				connTree.updateUI();
			}
		}
		
		if(flag == false){
			JOptionPane.showMessageDialog(null, "1049 - Unknow database '"+ databaseName +"'",
                    "", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * 删除选定的连接
	 */
	private void deleteConnectionAction() {
		TreePath selPath = connTree.getSelectionPath();
		DefaultMutableTreeNode selectConnectionNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
		
		try {
			SAXBuilder builder = new SAXBuilder();
			
			String filePath = System.getProperty("user.dir")+"/config/connections.ncx";
			FileInputStream fis = new FileInputStream(filePath);
			Document document = builder.build(fis);
			Element rootElement = document.getRootElement();
			
			Element selectSingleElement = (Element) XPath.selectSingleNode(rootElement, "/Connections/Connection[@ConnectionName='"+selectConnectionNode.getUserObject().toString()+"']");
			
			boolean removeChild = rootElement.removeContent(selectSingleElement);
			
			FileOutputStream fos = new FileOutputStream(filePath);
			XMLOutputter output = new XMLOutputter();
			Format f = Format.getPrettyFormat();
			output.setFormat(f);
			output.output(document, fos);
			fos.close();
			
			//从配置文件中删除成功后才清除相应树节点
			if(removeChild == true){
				selectConnectionNode.removeFromParent();
				connTree.updateUI();
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * 打开连接的响应事件
	 * @param connectionNode
	 */
	private void openConnectionAction(ConnectionNode connectionNode) {
		JDBCConn jdbcConn;
		ResultSet rs = null;
		
		TreePath selPath = connTree.getSelectionPath();
		
		DefaultMutableTreeNode selectConnectionNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
		
		ConnectionNameNode connectionNameNode = (ConnectionNameNode) connectionNode;
		
		JDialog dialog = new JDialog();
		
		Thread waitThread = new Thread(new WaitRunnable(connectionNameNode, dialog));
		waitThread.start();
		
		Connection conn = JDBCConn.allConnections.get(connectionNameNode.toString());
		if(conn!=null){
			jdbcConn = new JDBCConn();
			rs = jdbcConn.query(conn, "SHOW DATABASES;");
		}else{
			ServiceContext serviceContext = connTree.getDataContext().getConnectionDataContext().get(connectionNameNode.toString());
			jdbcConn = new JDBCConn(connectionNameNode.toString(), serviceContext.getHostOrIp(), serviceContext.getUserName(),
					serviceContext.getPassword(), serviceContext.getPort());
			Connection newConn = jdbcConn.getConn();
			if(newConn!=null){
				rs = jdbcConn.query(newConn, "SHOW DATABASES");
			}
		}
		
		if(rs!=null){
			try {
				while (rs.next()) {
					String dataNode = rs.getString(1);
					
					DatabaseNode databaseNode = new DatabaseNode(dataNode);
					DefaultMutableTreeNode databaseTreeNode = new DefaultMutableTreeNode(databaseNode);
					
					selectConnectionNode.add(databaseTreeNode);
				}
				
				connTree.expandPath(new TreePath(selectConnectionNode.getPath()));
				connectionNameNode.setState(OperationUtils.OPENSTATE);
				
				dialog.dispose();
				
				connTree.updateUI();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}else{
			dialog.dispose();
			JOptionPane.showMessageDialog(null,"2003 - Can't connect to MySQL server on ' " + connectionNameNode.toString() + " ' (10061)","",
					JOptionPane.ERROR_MESSAGE);
			
		}
	}
	
	/**
	 * 关闭连接响应事件
	 * @param connectionNode
	 */
	private void closeConnectionAction(ConnectionNode connectionNode) {
		TreePath selPath = connTree.getSelectionPath();
		DefaultMutableTreeNode selectConnectionNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
		
		ConnectionNameNode connectionNameNode = (ConnectionNameNode) connectionNode;
		JDBCConn.allConnections.remove(connectionNameNode.toString());
		
		selectConnectionNode.removeAllChildren();
		connectionNameNode.setState(OperationUtils.CLOSESTATE);
		
		connTree.updateUI();
		
	}
	
	/**
	 * 构造连接等待对话框
	 * @param connectionNameNode
	 * @param dialog
	 */
	private void generalDialogContent(ConnectionNameNode connectionNameNode, JDialog dialog) {
		URL resource = this.getClass().getResource("/image/connection/connection.png");
		dialog.setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		dialog.setContentPane(new JLabel("     连接到  " + connectionNameNode.toString() + " ..."));
		dialog.setTitle("正在连接...");
		dialog.setModal(true);
		dialog.setSize(new Dimension(300, 120));
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);
	}
	
	private class WaitRunnable implements Runnable {
		private ConnectionNameNode connectionNameNode;
		private JDialog dialog;

		public WaitRunnable(ConnectionNameNode connectionNameNode,
				JDialog dialog) {
			this.connectionNameNode = connectionNameNode;
			this.dialog = dialog;
		}

		public void run() {
			generalDialogContent(connectionNameNode, dialog);
		}
	}

}
