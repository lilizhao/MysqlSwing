package com.raisecom.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class RegisterCodeCheck {
	
	public boolean registerCodeverify(String registerCode){
		boolean flag = false;
		try {
			Set<String> legalRegisterCodes = new HashSet<String>(); 
			
			FileReader reader = new FileReader(System.getProperty("user.dir")+
					"/config/register.code"); 
			BufferedReader br = new BufferedReader(reader);
			String str = null; 
			while((str = br.readLine())!= null) {
				legalRegisterCodes.add(str);
			} 
			br.close();
			reader.close();
			
			flag = legalRegisterCodes.contains(registerCode);
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return flag;
	}

}
