package com.raisecom.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import com.raisecom.util.BorderUtils;
import com.raisecom.view.custom.JAutoCompleteComboBox;

public class StructureSynchDialogMainPanel extends JPanel{
	
	private static final long serialVersionUID = -1261206938111837768L;
	
	private StructureSynchDialog structureSynchDialog;
	
	private JPanel northPanel;
	private JTabbedPane centerTabPanel;
	private JPanel southPanel;
	
	private JAutoCompleteComboBox dataSynchNameComboBox;
	private JLabel newDataSynchLabel;
	private JLabel saveDataSynchLabel;
	private JLabel saveToDataSynchLabel;
	private JLabel deleteDataSynchLabel;
	private JLabel separatorLabel;
	private JLabel structuresynchLabel;
	
	//常规面板组件
	private JLabel connectionLabel;
	private JAutoCompleteComboBox connectionComboBox;
	private JLabel databaseLabel;
	private JAutoCompleteComboBox datbaseComboBox;
	private JLabel targetConnectionLabel;
	private JAutoCompleteComboBox descModelComboBox;
	private JLabel descDatabaseLabel;
	private JAutoCompleteComboBox descDatbaseComboBox;
	
	private JCheckBox comparisonTableCheckBox;
	private JCheckBox contrastPrimaryKeyCheckBox;
	private JCheckBox comparativeForeignKeyCheckBox;
	private JCheckBox contrastIndexCheckBox;
	private JCheckBox contrastTriggerCheckBox;
	private JCheckBox comparingCharacterSetCheckBox;
	private JCheckBox contrastAutoIncrementValueCheckBox;
	private JCheckBox contrastPartitionCheckBox;
	private JCheckBox contrastViewCheckBox;
	private JCheckBox contrastFunctionCheckBox;
	private JCheckBox contrastEventCheckBox;
	
	private JCheckBox createObjectSQLCheckBox;
	private JCheckBox changeObjectSQLCheckBox;
	private JCheckBox deleteObjectSQLCheckBox;
	private JCheckBox afterRunCompareCheckBox;
	private JCheckBox anErrorEncounterContinueCheckBox;
	
	private JButton comparisonButton;
	
	//比对面板组件
	private JLabel sourceObjectLabel;
	private JLabel targetObjectLabel;
	private JTextArea souceObjectTextArea;
	private JTextArea targetObjectTextArea;
	private JLabel newProjectLabel;
	private JLabel modifyProjectLabel;
	private JProgressBar projectProgressBar;
	private JLabel serchAndModifyLabel;
	private JTextArea serchAndModifyTextArea;
	private JButton runInquireButton;
	
	
	//信息日志组件
	private JTextArea infoLogTextArea;
	private JProgressBar infoLogProgressBar;
	
	
	private JButton closeButton;

	public StructureSynchDialogMainPanel(StructureSynchDialog structureSynchDialog) {
		this.structureSynchDialog = structureSynchDialog;
		
		initNorthPanel();
		initCenterPanel();
		initSouth();
		addComponentPanel();
		initlisteners();
	}

	private void initNorthPanel() {
		northPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		dataSynchNameComboBox = new JAutoCompleteComboBox();
		dataSynchNameComboBox.setPrototypeDisplayValue("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		URL newResource = this.getClass().getResource("/image/dialog/newdatasynch.png");
		newDataSynchLabel = new JLabel("新建");
		newDataSynchLabel.setIcon(new ImageIcon(newResource));
		
		newDataSynchLabel.addMouseListener(new NewDataSynchAdapter());
		
		URL saveResource = this.getClass().getResource("/image/dialog/savedatatrans.png");
		saveDataSynchLabel = new JLabel("保存");
		saveDataSynchLabel.setIcon(new ImageIcon(saveResource));
		
		saveToDataSynchLabel = new JLabel("另存为");
		URL saveToResource = this.getClass().getResource("/image/dialog/savetodatatrans.png");
		saveToDataSynchLabel.setIcon(new ImageIcon(saveToResource));
		
		deleteDataSynchLabel = new JLabel("删除");
		URL deleteResource = this.getClass().getResource("/image/dialog/deletedatasynch.png");
		deleteDataSynchLabel.setIcon(new ImageIcon(deleteResource));
		
		separatorLabel = new JLabel("|");
		
		structuresynchLabel = new JLabel("结构同步");
		URL structuresynchResource = this.getClass().getResource("/image/dialog/structuresynch.png");
		structuresynchLabel.setIcon(new ImageIcon(structuresynchResource));
		
		northPanel.add(dataSynchNameComboBox);
		northPanel.add(newDataSynchLabel);
		northPanel.add(saveDataSynchLabel);
		northPanel.add(saveToDataSynchLabel);
		northPanel.add(deleteDataSynchLabel);
		northPanel.add(separatorLabel);
		northPanel.add(structuresynchLabel);
	}

	private void initCenterPanel() {
		centerTabPanel = new JTabbedPane();
		
		JPanel commonPanel = new JPanel();
		JPanel comparisonPanel = new JPanel();
		JPanel infoLogPanel = new JPanel();
		
		initCommonPanel(commonPanel);
		initComparisonPanel(comparisonPanel);
		initInfoLogPanel(infoLogPanel);
		
		centerTabPanel.add(" 常规  ", commonPanel);
		centerTabPanel.add(" 比对  ", comparisonPanel);
		centerTabPanel.add("信息日志", infoLogPanel);
	}

	/**
	 * 功能描述：初始化常规面板 
	 */
	private void initCommonPanel(JPanel commonPanel) {
		commonPanel.setLayout(new BorderLayout());
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new GridLayout(1, 2));
		
		JPanel sourcePanel = new JPanel();
		JPanel descPanel = new JPanel();
		
		sourcePanel.setBackground(new Color(252, 252, 252));
		sourcePanel.setBorder(BorderUtils.createEmsBorder("源"));
		descPanel.setBackground(new Color(252, 252, 252));
		descPanel.setBorder(BorderUtils.createEmsBorder("目标"));
		
		sourcePanel.setLayout(new GridBagLayout());
		
		connectionLabel = new JLabel("连接:");
		connectionComboBox = new JAutoCompleteComboBox();
		databaseLabel = new JLabel("数据库:");
		datbaseComboBox = new JAutoCompleteComboBox();
		
		sourcePanel.add(connectionLabel,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		sourcePanel.add(connectionComboBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		sourcePanel.add(databaseLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		sourcePanel.add(datbaseComboBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 10, 5), 0, 0));
		
		
		descPanel.setLayout(new BorderLayout());
		
		JPanel descNorthPanel = new JPanel(new GridBagLayout());
		descNorthPanel.setBackground(new Color(252,252,252));
		
		targetConnectionLabel = new JLabel("连接:");
		descModelComboBox = new JAutoCompleteComboBox();
		descDatabaseLabel = new JLabel("数据库:");
		descDatbaseComboBox = new JAutoCompleteComboBox();
		
		descNorthPanel.add(targetConnectionLabel,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		descNorthPanel.add(descModelComboBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST,GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		descNorthPanel.add(descDatabaseLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		descNorthPanel.add(descDatbaseComboBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 10, 5), 0, 0));
		
		descPanel.add(descNorthPanel,BorderLayout.NORTH);
		
		northPanel.add(sourcePanel);
		northPanel.add(descPanel);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(1, 2));
		
		JPanel contrastOptionPanel = new JPanel();
		contrastOptionPanel.setBorder(BorderUtils.createEmsBorder("比对选项"));
		contrastOptionPanel.setLayout(new BorderLayout());
		contrastOptionPanel.setBackground(new Color(252, 252, 252));
		
		JPanel southWestPanel = new JPanel(new GridBagLayout());
		contrastOptionPanel.add(southWestPanel,BorderLayout.NORTH);
		
		comparisonTableCheckBox = new JCheckBox("比对表",true);
		contrastPrimaryKeyCheckBox = new JCheckBox("比对主键",true);
		comparativeForeignKeyCheckBox = new JCheckBox("比对外键",true);
		contrastIndexCheckBox = new JCheckBox("比对索引",true);
		contrastTriggerCheckBox = new JCheckBox("比对触发器",true);
		comparingCharacterSetCheckBox = new JCheckBox("比对字符集",true);
		contrastAutoIncrementValueCheckBox = new JCheckBox("比对自动递增值",false);
		contrastPartitionCheckBox = new JCheckBox("比对分割区",true);
		contrastPartitionCheckBox.setEnabled(false);
		contrastViewCheckBox = new JCheckBox("比对视图",true);
		contrastFunctionCheckBox = new JCheckBox("比对函数",true);
		contrastEventCheckBox = new JCheckBox("比对事件",true);
		
		southWestPanel.add(comparisonTableCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		southWestPanel.add(contrastPrimaryKeyCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 25, 0, 5), 0, 0));
		southWestPanel.add(comparativeForeignKeyCheckBox,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 25, 0, 5), 0, 0));
		southWestPanel.add(contrastIndexCheckBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 25, 0, 5), 0, 0));
		southWestPanel.add(contrastTriggerCheckBox,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 25, 0, 5), 0, 0));
		southWestPanel.add(comparingCharacterSetCheckBox,new GridBagConstraints(0, 5, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 25, 0, 5), 0, 0));
		southWestPanel.add(contrastAutoIncrementValueCheckBox,new GridBagConstraints(0, 6, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 25, 0, 5), 0, 0));
		southWestPanel.add(contrastPartitionCheckBox,new GridBagConstraints(0, 7, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 25, 0, 5), 0, 0));
		southWestPanel.add(contrastViewCheckBox,new GridBagConstraints(0, 8, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		southWestPanel.add(contrastFunctionCheckBox,new GridBagConstraints(0, 9, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		southWestPanel.add(contrastEventCheckBox,new GridBagConstraints(0, 10, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		
		JPanel runOptionsPanel = new JPanel();
		runOptionsPanel.setBorder(BorderUtils.createEmsBorder("运行选项"));
		runOptionsPanel.setBackground(new Color(252,252,252));
		runOptionsPanel.setLayout(new BorderLayout());
		
		JPanel southEastPanel = new JPanel();
		southEastPanel.setBackground(new Color(252, 252, 252));
		southEastPanel.setLayout(new GridBagLayout());
		
		createObjectSQLCheckBox = new JCheckBox("创建对象的 SQL",true);
		changeObjectSQLCheckBox = new JCheckBox("改变对象的 SQL",true);
		deleteObjectSQLCheckBox = new JCheckBox("删除对象的 SQL",true);
		afterRunCompareCheckBox = new JCheckBox("运行后比对",false);
		anErrorEncounterContinueCheckBox = new JCheckBox("遇到错误继续",false);
		
		southEastPanel.add(createObjectSQLCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		southEastPanel.add(changeObjectSQLCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		southEastPanel.add(deleteObjectSQLCheckBox,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		southEastPanel.add(afterRunCompareCheckBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		southEastPanel.add(anErrorEncounterContinueCheckBox,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		
		runOptionsPanel.add(southEastPanel,BorderLayout.NORTH);
		
		centerPanel.add(contrastOptionPanel);
		centerPanel.add(runOptionsPanel);
		
		JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		southPanel.setBackground(new Color(252,252,252));
		
		comparisonButton =    new JButton("      比对      ");
		
		southPanel.add(comparisonButton);
		
		setComponentOpaque(false,southWestPanel,southEastPanel,comparisonTableCheckBox,contrastPrimaryKeyCheckBox,
				comparativeForeignKeyCheckBox,contrastIndexCheckBox,contrastTriggerCheckBox,
				comparingCharacterSetCheckBox,contrastAutoIncrementValueCheckBox,
				contrastPartitionCheckBox,contrastViewCheckBox,contrastFunctionCheckBox,
				contrastEventCheckBox,createObjectSQLCheckBox,changeObjectSQLCheckBox,
				deleteObjectSQLCheckBox,afterRunCompareCheckBox,anErrorEncounterContinueCheckBox);
		
		commonPanel.add(northPanel,BorderLayout.NORTH);
		commonPanel.add(centerPanel,BorderLayout.CENTER);
		commonPanel.add(southPanel,BorderLayout.SOUTH);
	}
	
	/**
	 * 功能描述：初始化高级面板
	 * @param advancePanel 
	 */
	private void initComparisonPanel(JPanel comparisonPanel) {
		comparisonPanel.setLayout(new BorderLayout());
		
		JPanel centerPanel = new JPanel();
		JPanel southPanel = new JPanel();
		
		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBackground(new Color(252, 252, 252));
		
		sourceObjectLabel = new JLabel("源对象:");
		targetObjectLabel = new JLabel("目标对象:");
		souceObjectTextArea = new JTextArea();
		JScrollPane sourceObjectPane = new JScrollPane(souceObjectTextArea,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		targetObjectTextArea = new JTextArea();
		JScrollPane targetObjectPane = new JScrollPane(targetObjectTextArea,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		
		
		centerPanel.add(sourceObjectLabel,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		centerPanel.add(targetObjectLabel,new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		centerPanel.add(sourceObjectPane,new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));
		centerPanel.add(targetObjectPane,new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));
		
		southPanel.setLayout(new GridBagLayout());
		southPanel.setBackground(new Color(252, 252, 252));
		
		JPanel projectPanel = new JPanel();
		
		newProjectLabel = new JLabel("新建项目");
		URL newProjectResource = this.getClass().getResource("/image/dialog/newproject.png");
		newProjectLabel.setIcon(new ImageIcon(newProjectResource));
		modifyProjectLabel = new JLabel("已修改项目");
		URL modifyProjectResource = this.getClass().getResource("/image/dialog/modifyproject.png");
		modifyProjectLabel.setIcon(new ImageIcon(modifyProjectResource));
		
		projectPanel.add(newProjectLabel);
		projectPanel.add(modifyProjectLabel);
		
		projectProgressBar = new JProgressBar();
		serchAndModifyLabel = new JLabel("查询已修改: (勾选以运行)");
		
		serchAndModifyTextArea = new JTextArea(5,1);
		JScrollPane serchAndModifyPane = new JScrollPane(serchAndModifyTextArea);
		
		runInquireButton = new JButton("运行查询");
		runInquireButton.setEnabled(false);
		
		southPanel.add(projectPanel,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		southPanel.add(projectProgressBar,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		southPanel.add(serchAndModifyLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		southPanel.add(serchAndModifyPane,new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));
		southPanel.add(runInquireButton,new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(5, 5, 10, 5), 0, 0));
		
		setComponentOpaque(false,projectPanel,newProjectLabel,modifyProjectLabel);
		
		comparisonPanel.add(centerPanel,BorderLayout.CENTER);
		comparisonPanel.add(southPanel,BorderLayout.SOUTH);
	}

	/**
	 * 功能描述：初始化信息日志面板
	 * @param infoLogPanel
	 */
	private void initInfoLogPanel(JPanel infoLogPanel) {
		infoLogPanel.setLayout(new GridBagLayout());
		infoLogPanel.setBackground(new Color(252,252,252));
		
		infoLogTextArea = new JTextArea();
		infoLogTextArea.setBackground(new Color(236, 233, 216));
		JScrollPane infoLogScrollPane = new JScrollPane(infoLogTextArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		infoLogProgressBar = new JProgressBar();
		
		infoLogPanel.add(infoLogScrollPane,new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogProgressBar,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 10, 5), 0, 0));
		
	}

	private void initSouth() {
		southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		closeButton = new JButton("关闭");
		
		southPanel.add(closeButton);
	}

	private void addComponentPanel() {
		setLayout(new GridBagLayout());
		
		add(northPanel,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		add(centerTabPanel,new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));
		add(southPanel,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
	}
	
	/**
	 * 功能描述：设置组件为透明
	 * @param components
	 */
	private void setComponentOpaque(boolean flag,Component...components) {
		for(Component component:components){
			if(component instanceof JPanel){
				JPanel panelComponent = (JPanel) component;
				panelComponent.setOpaque(flag);
			}else if(component instanceof JCheckBox){
				JCheckBox checkBoxComponent = (JCheckBox) component;
				checkBoxComponent.setOpaque(flag);
			}
		}
	}
	
	/**
	 * 功能描述：为标签添加鼠标响应事件
	 * @author Administrator
	 *
	 */
	private class NewDataSynchAdapter extends MouseAdapter {
		public void mouseEntered(MouseEvent e) {
			if(e.getSource()==newDataSynchLabel){
				newDataSynchLabel.setBackground(Color.WHITE);
			}
		}

		public void mouseClicked(MouseEvent e) {
			if(e.getSource()==newDataSynchLabel){
				newDataSynchLabel.setBackground(Color.GRAY);
			}
			
		}
		
	}
	
	/**
	 * 功能描述：初始化组件监听
	 */
	private void initlisteners() {
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				structureSynchDialog.dispose();
			}
		});
	}
	
}




















