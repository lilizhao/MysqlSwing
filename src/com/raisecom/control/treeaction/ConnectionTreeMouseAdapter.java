package com.raisecom.control.treeaction;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.raisecom.model.data.JDBCConn;
import com.raisecom.model.data.ServiceContext;
import com.raisecom.model.data.ToolBarRadioBtnBean;
import com.raisecom.util.OperationUtils;
import com.raisecom.util.SwingConstants;
import com.raisecom.view.frame.MainFrame;
import com.raisecom.view.panel.MainPanel;
import com.raisecom.view.popup.CommonNodePopupMenu;
import com.raisecom.view.toolbar.MainToolBar;
import com.raisecom.view.tree.BackupNode;
import com.raisecom.view.tree.ConnectionNameNode;
import com.raisecom.view.tree.ConnectionNode;
import com.raisecom.view.tree.ConnectionTree;
import com.raisecom.view.tree.DatabaseNode;
import com.raisecom.view.tree.EventNode;
import com.raisecom.view.tree.FunctionNode;
import com.raisecom.view.tree.ReportNode;
import com.raisecom.view.tree.SearchNode;
import com.raisecom.view.tree.TableNode;
import com.raisecom.view.tree.ViewNode;

/**
 * 树点击适配器
 * @author lilz-2686
 *
 */
public class ConnectionTreeMouseAdapter extends MouseAdapter {
	private ConnectionTree connTree;
	private JList dataList;
	
	private Element rootPopupElement;

	private CommonNodePopupMenu commonNodePopupMenu;

	
	public ConnectionTreeMouseAdapter(ConnectionTree connTree, JList dataList) {
		this.connTree = connTree;
		this.dataList = dataList;
		
		getPopupMenuRootElement();
	}

	
	/**
	 * 获取弹出菜单配置文件
	 */
	private void getPopupMenuRootElement() {
		try {
			SAXBuilder saxBuilder = new SAXBuilder();
			Document document = saxBuilder.build(MainPanel.class.getResource("/config/popup/popupmenu.xml"));
			
			rootPopupElement = document.getRootElement();
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void mouseReleased(MouseEvent me) {
		resetSlectedTreePath(me);
		
		TreePath selPath = connTree.getSelectionPath();
		
		if(selPath!=null){ 
			DefaultMutableTreeNode selectConnectionNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
			Object userObject = selectConnectionNode.getUserObject();
			Object[] paths = selPath.getPath();
			
			if (me.isPopupTrigger()) {
				if(userObject instanceof ConnectionNameNode){
					ConnectionNameNode connectionNameNode = (ConnectionNameNode) userObject;
					
					commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, connectionNameNode, "connection");
					commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
				}else if(userObject instanceof DatabaseNode){
					DatabaseNode databaseNode = (DatabaseNode) userObject;
					
					commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, databaseNode, "database");
					commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
				}else if(userObject instanceof TableNode){
					TableNode tableNode = (TableNode) userObject;
					
					if(paths.length == 4){  //选中的不是具体表，具体表的父节点
						commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, tableNode, "tablegroup");
						commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
					}else if(paths.length == 5){  //选中的是具体表
						commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, tableNode, "table");
						commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
					}
				}else if(userObject instanceof ViewNode){
					ViewNode viewNode = (ViewNode) userObject;
					
					if(paths.length == 4){  //选中的不是具体视图，具体视图的父节点
						commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, viewNode, "viewgroup");
						commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
					}else if(paths.length == 5){  //选中是具体视图
						commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, viewNode, "view");
						commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
					}
				}else if(userObject instanceof FunctionNode){
					FunctionNode functionNode = (FunctionNode) userObject;
					
					if(paths.length == 4){  //选中的不是具体函数，具体函数的父节点
						commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, functionNode, "functiongroup");
						commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
					}else if(paths.length == 5){  //选中的是具体函数
						commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, functionNode, "function");
						commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
					}
				}else if(userObject instanceof EventNode){
					EventNode eventNode = (EventNode) userObject;
					
					if(paths.length == 4){  //选中的不是具体事件，具体事件的父节点
						commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, eventNode, "eventgroup");
						commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
					}else if(paths.length == 5){  //选中的是具体事件
						commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, eventNode, "event");
						commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
					}
				}else if(userObject instanceof SearchNode){
					SearchNode searchNode = (SearchNode) userObject;
					
					if(paths.length == 4){  //选中的不是具体查询，是具体查询的父节点
						commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, searchNode, "querygroup");
						commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
					}
				}else if(userObject instanceof ReportNode){
					ReportNode reportNode = (ReportNode) userObject;
					
					if(paths.length == 4){  //选中的不是具体报表，是具体报表的父节点
						commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, reportNode, "reportgroup");
						commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
					}
				}else if(userObject instanceof BackupNode){
					BackupNode backupNode = (BackupNode) userObject;
					
					if(paths.length == 4){  //选中的不是具体备份，是具体备份的父节点
						commonNodePopupMenu = new CommonNodePopupMenu(connTree, rootPopupElement, backupNode, "backupgroup");
						commonNodePopupMenu.show(me.getComponent(), me.getX(), me.getY());
					}
				}
			}
		}
	}

	private void resetSlectedTreePath(MouseEvent me) {
		TreePath path = connTree.getPathForLocation(me.getX(),me.getY()); 
		if(path!=null){
			connTree.setSelectionPath(path); 
		}
	}

	public void mouseClicked(MouseEvent me) {
		TreePath selPath = connTree.getSelectionPath();
		
		if(selPath!=null){  //选中的部分必须是有效部分才进行处理，选中空白处不作逻辑处理
			DefaultMutableTreeNode selectConnectionNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
			Object userObject = selectConnectionNode.getUserObject();
			
			if(me.getClickCount()==2){   //处理鼠标双击事件，实质是进行实时查询，并将查询的结果作为子节点挂载到该叶子上
				JDBCConn jdbcConn;
				ResultSet rs = null;;
				
				if(userObject instanceof ConnectionNameNode){    //选中的连接名称节点
					ConnectionNameNode connectionNameNode = (ConnectionNameNode) userObject;
					
					//连接节点为关闭状态时才转化为打开状态
					if(OperationUtils.CLOSESTATE.equalsIgnoreCase(connectionNameNode.getState())){
						JDialog dialog = new JDialog();
						
						Thread waitThread = new Thread(new WaitRunnable(connectionNameNode, dialog));
						waitThread.start();
						
						Connection conn = JDBCConn.allConnections.get(connectionNameNode.toString());
						if(conn!=null){
							jdbcConn = new JDBCConn();
							rs = jdbcConn.query(conn, "SHOW DATABASES;");
						}else{
							ServiceContext serviceContext = connTree.getDataContext().getConnectionDataContext().get(connectionNameNode.toString());
							jdbcConn = new JDBCConn(connectionNameNode.toString(), serviceContext.getHostOrIp(), serviceContext.getUserName(),
									serviceContext.getPassword(), serviceContext.getPort());
							Connection newConn = jdbcConn.getConn();
							if(newConn!=null){
								rs = jdbcConn.query(newConn, "SHOW DATABASES");
							}
						}
						
						if(rs!=null){
							try {
								while (rs.next()) {
									String dataNode = rs.getString(1);
									
									DatabaseNode databaseNode = new DatabaseNode(dataNode);
									DefaultMutableTreeNode databaseTreeNode = new DefaultMutableTreeNode(databaseNode);
									
									selectConnectionNode.add(databaseTreeNode);
								}
								
								connTree.expandPath(new TreePath(selectConnectionNode.getPath()));
								connectionNameNode.setState(OperationUtils.OPENSTATE);
								
								dialog.dispose();
								
								connTree.updateUI();
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}else{
							dialog.dispose();
							JOptionPane.showMessageDialog(MainFrame.getMainFrame(),"2003 - Can't connect to MySQL server on ' " + connectionNameNode.toString() + " ' (10061)","",
									JOptionPane.ERROR_MESSAGE);
							
						}
					}
					
				}else if(userObject instanceof DatabaseNode){  //选中的是数据库名称节点
					DatabaseNode databaseNode = (DatabaseNode) userObject;
					
					//数据库节点为关闭状态时才转化为打开状态
					if(OperationUtils.CLOSESTATE.equalsIgnoreCase(databaseNode.getState())){
						List<DefaultMutableTreeNode> generalCommonNodes = SwingConstants.generalCommonNode();
						ListIterator<DefaultMutableTreeNode> listCommonNodes = generalCommonNodes.listIterator();
						while(listCommonNodes.hasNext()){
							DefaultMutableTreeNode nextCommonNode = listCommonNodes.next();
							
							selectConnectionNode.add(nextCommonNode);
						}
						
						connTree.expandPath(new TreePath(selectConnectionNode.getPath()));
						databaseNode.setState(OperationUtils.OPENSTATE);
						
						connTree.updateUI();
					}
				}
				
			} else if (me.getClickCount()==1) {
				if (userObject instanceof TableNode && selPath.getPathCount()==4){  //选中的是第三层的表节点(第四层表节点表示具体的表)
					TableNode tableNode = (TableNode) userObject;
					
					Object[] selectPath = selPath.getPath();
					String connectionName = selectPath[1].toString();
					String databaseName = selectPath[2].toString();
					String executeSql = "SELECT TABLE_NAME from information_schema.`TABLES` WHERE TABLE_SCHEMA = '" + databaseName + "';";
					
					expansionTreeNode(connectionName, selPath, selectConnectionNode, tableNode, executeSql);
					setDataList(selectConnectionNode, tableNode);
					setToolBarSelectBtn("buttons.tool.table");
				} else if (userObject instanceof ViewNode && selPath.getPathCount()==4){    //选中的是第三层的视图节点(第四层表节点表示具体的视图)
					ViewNode viewNode = (ViewNode) userObject;
					
					Object[] selectPath = selPath.getPath();
					String connectionName = selectPath[1].toString();
					String databaseName = selectPath[2].toString();
					String executeSql = "select TABLE_NAME from information_schema.tables where table_schema='" + databaseName + "' and table_type='view'";
					
					expansionTreeNode(connectionName, selPath, selectConnectionNode, viewNode, executeSql);
					setDataList(selectConnectionNode, viewNode);
					setToolBarSelectBtn("buttons.tool.view");
				} else if (userObject instanceof FunctionNode && selPath.getPathCount()==4){  //选中的是第三层的函数节点(第四层表节点表示具体的函数)
					FunctionNode functionNode = (FunctionNode) userObject;
					
					Object[] selectPath = selPath.getPath();
					String connectionName = selectPath[1].toString();
					String databaseName = selectPath[2].toString();
					String executeSql = "SELECT `name` FROM mysql.proc WHERE db = '" + databaseName + "' AND type = 'function'";
					
					expansionTreeNode(connectionName, selPath, selectConnectionNode, functionNode, executeSql);
					setDataList(selectConnectionNode, functionNode);
					setToolBarSelectBtn("buttons.tool.function");
				} else if (userObject instanceof EventNode && selPath.getPathCount()==4){   //选中的是第三层的事件节点(第四层表节点表示具体的事件)
					EventNode eventNode = (EventNode) userObject;
					
					Object[] selectPath = selPath.getPath();
					String connectionName = selectPath[1].toString();
					String databaseName = selectPath[2].toString();
					String executeSql = "SELECT EVENT_NAME FROM INFORMATION_SCHEMA.EVENTS WHERE EVENT_SCHEMA = '" + databaseName + "'";
					
					expansionTreeNode(connectionName, selPath, selectConnectionNode, eventNode, executeSql);
					setDataList(selectConnectionNode, eventNode);
					setToolBarSelectBtn("buttons.tool.event");
				} else if (userObject instanceof SearchNode && selPath.getPathCount()==4) {  //选中的是第三层的查询节点(第四层表节点表示具体的查询)
					
				} else if (userObject instanceof ReportNode && selPath.getPathCount()==4) {  //选中的是第三层的报表节点(第四层表节点表示具体的报表)
					
				} else if (userObject instanceof BackupNode && selPath.getPathCount()==4) {  //选中的是第三层的备份节点(第四层表节点表示具体的备份)
					
				}
			}
		}
	}

	/**
	 * 根据选中的第三层节点(表组、视图组、函数组、事件组等等)对ToolBar上的单选按钮进行联动控制
	 * @param actioncommand
	 */
	private void setToolBarSelectBtn(String actioncommand) {
		Map<String, ToolBarRadioBtnBean> mapToolBarRadioBtnBean = MainToolBar.getMapToolBarRadioBtnBean();
		mapToolBarRadioBtnBean.get(actioncommand).getjRadioButton().setSelected(true);
		
		Set<Entry<String, ToolBarRadioBtnBean>> entrySet = mapToolBarRadioBtnBean.entrySet();
		for (Entry<String, ToolBarRadioBtnBean> entry : entrySet) {
			ToolBarRadioBtnBean barRadioBtnBean = entry.getValue();
			JRadioButton getjRadioButton = barRadioBtnBean.getjRadioButton();
			boolean selected = getjRadioButton.isSelected();
			
			if (selected) {
				getjRadioButton.setIcon(new ImageIcon(this.getClass().getResource(barRadioBtnBean.getIsSelectedImage())));
			} else {
				getjRadioButton.setIcon(new ImageIcon(this.getClass().getResource(barRadioBtnBean.getIsNotSelectedImage())));
			}
		}
	}

	/**
	 * 为List添加数据
	 * @param selectConnectionNode
	 * @param commonNode
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setDataList(DefaultMutableTreeNode selectConnectionNode, ConnectionNode commonNode) {
		List<ConnectionNode> connectionNodes = new LinkedList<ConnectionNode>();
		
		int childCount = selectConnectionNode.getChildCount();
		
		for (int i = 0; i<childCount; i++) {
			DefaultMutableTreeNode childAt = (DefaultMutableTreeNode) selectConnectionNode.getChildAt(i);
			
			Iterator<String> iterCommonNodeClass = SwingConstants.getCommonNodeClass().iterator();
			while (iterCommonNodeClass.hasNext()) {
				try {
					Class clazz = Class.forName(iterCommonNodeClass.next());
					
					if (commonNode.getClass() == clazz) {
						Constructor<ConnectionNode> constructor = clazz.getConstructor(String.class);
						ConnectionNode newNodeInstance = constructor.newInstance(childAt.toString());
						connectionNodes.add(newNodeInstance);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		dataList.setListData(connectionNodes.toArray());
	}

	/**
	 * 展开选中的常规节点
	 * @param connectionName 选中的连接名称
	 * @param selPath 选中的路径
	 * @param selectConnectionNode 选中的节点
	 * @param commonNode 常规节点
	 * @param executeSql 执行查询的SQL语句
	 */
	private void expansionTreeNode(String connectionName, TreePath selPath, DefaultMutableTreeNode selectConnectionNode, ConnectionNode commonNode , String executeSql) {
		JDBCConn jdbcConn;
		ResultSet rs;
		
		//常规节点为关闭状态时才转化为打开状态
		if(OperationUtils.CLOSESTATE.equalsIgnoreCase(commonNode.getState())){
			Connection conn = JDBCConn.allConnections.get(connectionName);
			
			if(conn!=null){
				jdbcConn = new JDBCConn();
				rs = jdbcConn.query(conn,executeSql);
				
				if(rs!=null){
					try {
						while (rs.next()) {
							String subNode = rs.getString(1);
							
							ConnectionNode subTableNode;
							
							if(commonNode instanceof TableNode){
								subTableNode = new TableNode(subNode);
								addTreeNodeToCommonNode(selectConnectionNode, subTableNode);
							}else if(commonNode instanceof ViewNode){
								subTableNode = new ViewNode(subNode);
								addTreeNodeToCommonNode(selectConnectionNode, subTableNode);
							}else if(commonNode instanceof FunctionNode){
								subTableNode = new FunctionNode(subNode);
								addTreeNodeToCommonNode(selectConnectionNode, subTableNode);
							}else if(commonNode instanceof EventNode){
								subTableNode = new EventNode(subNode);
								addTreeNodeToCommonNode(selectConnectionNode, subTableNode);
							}
							
						}
						
//						connTree.expandPath(new TreePath(selectConnectionNode.getPath())); //第三层节点单击后不需展开
						commonNode.setState(OperationUtils.OPENSTATE);
						
						connTree.updateUI();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void addTreeNodeToCommonNode(
			DefaultMutableTreeNode selectConnectionNode,
			ConnectionNode subTableNode) {
		DefaultMutableTreeNode subTableTreeNode = new DefaultMutableTreeNode(subTableNode);
		
		selectConnectionNode.add(subTableTreeNode);
	}

	/**
	 * 构造连接等待对话框
	 * @param connectionNameNode
	 * @param dialog
	 */
	private void generalDialogContent(ConnectionNameNode connectionNameNode, JDialog dialog) {
		URL resource = this.getClass().getResource("/image/connection/connection.png");
		dialog.setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		dialog.setContentPane(new JLabel("     连接到  " + connectionNameNode.toString() + " ..."));
		dialog.setTitle("正在连接...");
		dialog.setModal(true);
		dialog.setSize(new Dimension(300, 120));
		dialog.setLocationRelativeTo(MainFrame.getMainFrame());
		dialog.setVisible(true);
	}
	
	private class WaitRunnable implements Runnable {
		private ConnectionNameNode connectionNameNode;
		private JDialog dialog;

		public WaitRunnable(ConnectionNameNode connectionNameNode,
				JDialog dialog) {
			this.connectionNameNode = connectionNameNode;
			this.dialog = dialog;
		}

		public void run() {
			generalDialogContent(connectionNameNode, dialog);
		}
	}
}

