package com.raisecom.util;

import javax.swing.BorderFactory;
import javax.swing.border.Border;

public class BorderUtils {
	
	public static Border createEmsBorder(String borderLabel){
		Border etched = BorderFactory.createEtchedBorder();
		return BorderFactory.createTitledBorder(etched,borderLabel);
	}
}
