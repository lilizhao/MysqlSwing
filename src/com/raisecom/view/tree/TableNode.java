package com.raisecom.view.tree;

import javax.swing.ImageIcon;

import com.raisecom.util.ImageUtils;

public class TableNode extends ConnectionNode{

	public TableNode(String nodeName) {
		super(nodeName);
	}

	public ImageIcon getImageIcon() {
		return ImageUtils.TABLE_NODE_IMAGE;
	}

}
