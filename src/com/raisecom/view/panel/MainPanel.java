package com.raisecom.view.panel;

import javax.swing.JSplitPane;

import com.raisecom.util.BorderUtils;
import com.raisecom.view.tree.ConnectionTree;

/**
 * 主要工作区界面
 * @author lilz-2686
 *
 */
public class MainPanel extends JSplitPane{
	private static final long serialVersionUID = -6522845419266377573L;
	
	private ConnectionTree connTree;
	
	private ConnPanel connPanel;
	private ProPanel proPanel;
	
	public MainPanel(ConnectionTree connTree) {
		this.connTree = connTree;
		
		initViewComponent();
		
		setDividerSize(10);
		add(connPanel, JSplitPane.LEFT);
		add(proPanel, JSplitPane.RIGHT);
		setDividerLocation((int)(970*0.3));
		
		setBorder(BorderUtils.createEmsBorder(""));
		setDividerSize(6);
		
//		setUI(new JSplitPaneUI());
//		setOneTouchExpandable(true);
	}



	/**
	 * 初始化界面组件
	 */
	private void initViewComponent() {
		connPanel = new ConnPanel(connTree);
		proPanel = new ProPanel(connTree);
	}
	
	
}
