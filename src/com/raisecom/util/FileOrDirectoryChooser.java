package com.raisecom.util;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JTextField;

public class FileOrDirectoryChooser extends JFileChooser{
	
	private static final long serialVersionUID = -4383818906878192417L;
	
	private JTextField textField;

	public FileOrDirectoryChooser(String filePath, int fileSelectionMode, JTextField textField) {
		super(new File(filePath).isDirectory()?new File(filePath):new File(filePath).getParentFile());
		setFileSelectionMode(fileSelectionMode);
		if(!new File(filePath).isDirectory()){
			setSelectedFile(new File(filePath));
		}
		this.textField = textField;
	}
	
	public void approveSelection() {
		super.approveSelection();
		//设置当前打开的目录为默认目录
		this.setCurrentDirectory(this.getSelectedFile());
		selectFolder(this.getSelectedFile().getAbsolutePath());
	}

	private void selectFolder(String absolutePath) {
		textField.setText(absolutePath);
	}

	
}
