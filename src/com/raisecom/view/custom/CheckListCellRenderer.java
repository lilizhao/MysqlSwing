package com.raisecom.view.custom;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class CheckListCellRenderer extends JCheckBox implements ListCellRenderer {

	private static final long serialVersionUID = -2575287177726702542L;

	public CheckListCellRenderer() {
		super();
		setOpaque(true);
	}

	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
		this.setText(value.toString());
		setBackground(isSelected ? list.getSelectionBackground() : list
				.getBackground());
		setForeground(isSelected ? list.getSelectionForeground() : list
				.getForeground());
		this.setSelected(isSelected);
		return this;
	}

}