package com.raisecom.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.raisecom.util.BorderUtils;
import com.raisecom.view.custom.JAutoCompleteComboBox;

public class DataSynchDialogMainPanel extends JPanel{
	
	private static final long serialVersionUID = -1261206938111837768L;
	
	private DataSynchDialog dataSynchDialog;
	
	private JPanel northPanel;
	private JTabbedPane centerTabPanel;
	private JPanel southPanel;
	
	private JAutoCompleteComboBox dataSynchNameComboBox;
	private JLabel newDataSynchLabel;
	private JLabel saveDataSynchLabel;
	private JLabel saveToDataSynchLabel;
	private JLabel deleteDataSynchLabel;
	private JLabel separatorLabel;
	private JLabel structuresynchLabel;
	
	//常规面板组件
	private JLabel connectionLabel;
	private JAutoCompleteComboBox connectionComboBox;
	private JLabel databaseLabel;
	private JAutoCompleteComboBox datbaseComboBox;
	private JLabel souceConnectionLabel;
	private JAutoCompleteComboBox descModelComboBox;
	private JLabel descDatabaseLabel;
	private JAutoCompleteComboBox descDatbaseComboBox;
	private JButton allSelectButton;
	private JButton disAllSelectButton;
	
	//高级面板组件
	private JCheckBox useTransactionCheckBox;
	private JCheckBox displayDynchInformationCheckBox;
	private JCheckBox insertRecordsCheckBox;
	private JCheckBox deleteRecordsCheckBox;
	private JCheckBox updateRecordsCheckBox;
	
	//信息日志组件
	private JLabel infoLogTableLabel;
	private JLabel infoLogHaveProcessedLabel;
	private JLabel infoLogUpdateLabel;
	private JLabel infoLogInsertLabel;
	private JLabel infoLogDeleteLabel;
	private JLabel infoLogTimeLabel;
	private JTextArea infoLogTextArea;
	
	private JButton previewButton;
	private JButton startButton;
	private JButton closeButton;

	public DataSynchDialogMainPanel(DataSynchDialog dataSynchDialog) {
		this.dataSynchDialog = dataSynchDialog;
		
		initNorthPanel();
		initCenterPanel();
		initSouth();
		addComponentPanel();
		initlisteners();
	}

	private void initNorthPanel() {
		northPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		dataSynchNameComboBox = new JAutoCompleteComboBox();
		dataSynchNameComboBox.setPrototypeDisplayValue("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		URL newResource = this.getClass().getResource("/image/dialog/newdatasynch.png");
		newDataSynchLabel = new JLabel("新建");
		newDataSynchLabel.setIcon(new ImageIcon(newResource));
		
		newDataSynchLabel.addMouseListener(new NewDataSynchAdapter());
		
		URL saveResource = this.getClass().getResource("/image/dialog/savedatatrans.png");
		saveDataSynchLabel = new JLabel("保存");
		saveDataSynchLabel.setIcon(new ImageIcon(saveResource));
		
		saveToDataSynchLabel = new JLabel("另存为");
		URL saveToResource = this.getClass().getResource("/image/dialog/savetodatatrans.png");
		saveToDataSynchLabel.setIcon(new ImageIcon(saveToResource));
		
		deleteDataSynchLabel = new JLabel("删除");
		URL deleteResource = this.getClass().getResource("/image/dialog/deletedatasynch.png");
		deleteDataSynchLabel.setIcon(new ImageIcon(deleteResource));
		
		separatorLabel = new JLabel("|");
		
		structuresynchLabel = new JLabel("结构同步");
		URL structuresynchResource = this.getClass().getResource("/image/dialog/structuresynch.png");
		structuresynchLabel.setIcon(new ImageIcon(structuresynchResource));
		
		northPanel.add(dataSynchNameComboBox);
		northPanel.add(newDataSynchLabel);
		northPanel.add(saveDataSynchLabel);
		northPanel.add(saveToDataSynchLabel);
		northPanel.add(deleteDataSynchLabel);
		northPanel.add(separatorLabel);
		northPanel.add(structuresynchLabel);
	}

	private void initCenterPanel() {
		centerTabPanel = new JTabbedPane();
		
		JPanel commonPanel = new JPanel();
		JPanel advancePanel = new JPanel();
		JPanel infoLogPanel = new JPanel();
		
		initCommonPanel(commonPanel);
		initAdvancePanel(advancePanel);
		initInfoLogPanel(infoLogPanel);
		
		centerTabPanel.add(" 常规  ", commonPanel);
		centerTabPanel.add(" 高级  ", advancePanel);
		centerTabPanel.add("信息日志", infoLogPanel);
	}

	/**
	 * 功能描述：初始化常规面板 
	 */
	private void initCommonPanel(JPanel commonPanel) {
		commonPanel.setLayout(new BorderLayout());
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new GridLayout(1, 2));
		
		JPanel sourcePanel = new JPanel();
		JPanel descPanel = new JPanel();
		
		sourcePanel.setBackground(new Color(252, 252, 252));
		sourcePanel.setBorder(BorderUtils.createEmsBorder("源"));
		descPanel.setBackground(new Color(252, 252, 252));
		descPanel.setBorder(BorderUtils.createEmsBorder("目标"));
		
		sourcePanel.setLayout(new GridBagLayout());
		
		connectionLabel = new JLabel("连接:");
		connectionComboBox = new JAutoCompleteComboBox();
		databaseLabel = new JLabel("数据库:");
		datbaseComboBox = new JAutoCompleteComboBox();
		
		
		sourcePanel.add(connectionLabel,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(7, 5, 7, 5), 0, 0));
		sourcePanel.add(connectionComboBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		sourcePanel.add(databaseLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		sourcePanel.add(datbaseComboBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 10, 5), 0, 0));
		
		
		
		descPanel.setLayout(new BorderLayout());
		
		JPanel descNorthPanel = new JPanel(new GridBagLayout());
		descNorthPanel.setBackground(new Color(252,252,252));
		
		souceConnectionLabel = new JLabel("连接:");
		descModelComboBox = new JAutoCompleteComboBox();
		descDatabaseLabel = new JLabel("数据库:");
		descDatbaseComboBox = new JAutoCompleteComboBox();
		
		descNorthPanel.add(souceConnectionLabel,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		descNorthPanel.add(descModelComboBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST,GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		descNorthPanel.add(descDatabaseLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		descNorthPanel.add(descDatbaseComboBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 10, 5), 0, 0));
		
		descPanel.add(descNorthPanel,BorderLayout.NORTH);
		
		northPanel.add(sourcePanel);
		northPanel.add(descPanel);
		
		Object rows[][] = {{"", ""}};
		String columns[] = {"源表","目标表"};
		TableModel model = new DefaultTableModel(rows, columns);
		JTable table = new JTable(model);
		JScrollPane centerScrollPane = new JScrollPane(table);
		
		JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		allSelectButton =    new JButton("      全选      ");
		disAllSelectButton = new JButton("全部取消选择");
		
		southPanel.add(allSelectButton);
		southPanel.add(disAllSelectButton);
		
		commonPanel.add(northPanel,BorderLayout.NORTH);
		commonPanel.add(centerScrollPane,BorderLayout.CENTER);
		commonPanel.add(southPanel,BorderLayout.SOUTH);
	}
	
	/**
	 * 功能描述：初始化高级面板
	 * @param advancePanel 
	 */
	private void initAdvancePanel(JPanel advancePanel) {
		advancePanel.setLayout(new GridLayout(1, 2));
		advancePanel.setBackground(new Color(252, 252, 252));
		
		JPanel tablePanel = new JPanel();
		JPanel recordPanel = new JPanel();
		
		tablePanel.setBorder(BorderUtils.createEmsBorder(""));
		tablePanel.setLayout(new BorderLayout());
		
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new GridBagLayout());
		
		useTransactionCheckBox = new JCheckBox("使用事务",true);
		displayDynchInformationCheckBox = new JCheckBox("显示详细同步信息",true);
		
		leftPanel.add(useTransactionCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(15, 5, 0, 5), 0, 0));
		leftPanel.add(displayDynchInformationCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		
		tablePanel.add(leftPanel,BorderLayout.NORTH);
		
		recordPanel.setBorder(BorderUtils.createEmsBorder(""));
		recordPanel.setLayout(new BorderLayout());
		
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new GridBagLayout());
		
		insertRecordsCheckBox = new JCheckBox("插入记录",true);
		deleteRecordsCheckBox = new JCheckBox("删除记录",true);
		updateRecordsCheckBox = new JCheckBox("更新记录",true);
		
		rightPanel.add(insertRecordsCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(15, 5, 0, 5), 0, 0));
		rightPanel.add(deleteRecordsCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		rightPanel.add(updateRecordsCheckBox,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		
		recordPanel.add(rightPanel,BorderLayout.NORTH);
		
		tablePanel.setBackground(new Color(252,252,252));
		recordPanel.setBackground(new Color(252,252,252));
		
		setComponentOpaque(false,leftPanel,rightPanel,useTransactionCheckBox,displayDynchInformationCheckBox,
				insertRecordsCheckBox,deleteRecordsCheckBox,updateRecordsCheckBox);
		
		advancePanel.add(tablePanel);
		advancePanel.add(recordPanel);
	}

	/**
	 * 功能描述：初始化信息日志面板
	 * @param infoLogPanel
	 */
	private void initInfoLogPanel(JPanel infoLogPanel) {
		infoLogPanel.setLayout(new GridBagLayout());
		infoLogPanel.setBackground(new Color(252, 252, 252));
		
		infoLogTableLabel = new JLabel("表:");
		infoLogHaveProcessedLabel = new JLabel("已处理:");
		infoLogUpdateLabel = new JLabel("更新:");
		infoLogInsertLabel = new JLabel("插入:");
		infoLogDeleteLabel = new JLabel("删除:");
		infoLogTimeLabel = new JLabel("时间:");
		infoLogTextArea = new JTextArea();
		infoLogTextArea.setBackground(new Color(236,233,216));
		infoLogTextArea.setEditable(false);
		JScrollPane infoLogTextAreaPane = new JScrollPane(infoLogTextArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
//		infoLogProgressBar = new JProgressBar();
		
		infoLogPanel.add(infoLogTableLabel,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(11, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogHaveProcessedLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogUpdateLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogInsertLabel,new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,GridBagConstraints.NONE,
				new Insets(2, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogDeleteLabel,new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogTimeLabel,new GridBagConstraints(0, 5, 1, 1, 1.0, 0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(2, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogTextAreaPane,new GridBagConstraints(0, 6, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(13, 5, 5, 5), 0, 0));
	}

	private void initSouth() {
		southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		previewButton = new JButton("预览");
		startButton = new JButton("开始");
		closeButton = new JButton("关闭");
		
		southPanel.add(previewButton);
		southPanel.add(startButton);
		southPanel.add(closeButton);
	}

	private void addComponentPanel() {
		setLayout(new GridBagLayout());
		
		add(northPanel,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		add(centerTabPanel,new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));
		add(southPanel,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
	}
	
	/**
	 * 功能描述：设置组件为透明
	 * @param components
	 */
	private void setComponentOpaque(boolean flag,Component...components) {
		for(Component component:components){
			if(component instanceof JPanel){
				JPanel panelComponent = (JPanel) component;
				panelComponent.setOpaque(flag);
			}else if(component instanceof JCheckBox){
				JCheckBox checkBoxComponent = (JCheckBox) component;
				checkBoxComponent.setOpaque(flag);
			}
		}
	}
	
	/**
	 * 功能描述：为标签添加鼠标响应事件
	 * @author Administrator
	 *
	 */
	private class NewDataSynchAdapter extends MouseAdapter {
		public void mouseEntered(MouseEvent e) {
			if(e.getSource()==newDataSynchLabel){
				newDataSynchLabel.setBackground(Color.WHITE);
			}
		}

		public void mouseClicked(MouseEvent e) {
			if(e.getSource()==newDataSynchLabel){
				newDataSynchLabel.setBackground(Color.GRAY);
			}
			
		}
		
	}
	
	/**
	 * 功能描述：初始化组件监听
	 */
	private void initlisteners() {
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dataSynchDialog.dispose();
			}
		});
	}

	
}




















