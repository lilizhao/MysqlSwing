package com.raisecom.view.tree;

import javax.swing.ImageIcon;

import com.raisecom.util.ImageUtils;

public class ConnectionNameNode extends ConnectionNode{

	public ConnectionNameNode(String nodeName) {
		super(nodeName);
	}

	public ImageIcon getImageIcon() {
		if("closed".equalsIgnoreCase(getState())){
			return ImageUtils.CONNECTION_NAME_NODE_IMAGE;
		}else{
			return ImageUtils.CONNECTION_NAME_NODE_IMAGE2;
		}
	}

}
