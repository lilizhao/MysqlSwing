package com.raisecom.view.custom;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class MyFileFilter extends FileFilter{
	protected String ext;
	protected String description;

	public MyFileFilter(String ext, String description) {
		this.ext = ext.toLowerCase();
		this.description = description;
	}

	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}

		if (f.getName().toLowerCase().endsWith(ext)) {
			return true;
		}

		return false;
	}

	public String getDescription() {
		return description;
	}

}
