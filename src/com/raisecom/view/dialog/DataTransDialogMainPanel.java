package com.raisecom.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import com.raisecom.util.BorderUtils;
import com.raisecom.view.custom.CheckListCellRenderer;
import com.raisecom.view.custom.JAutoCompleteComboBox;

public class DataTransDialogMainPanel extends JPanel{
	
	private static final long serialVersionUID = -1261206938111837768L;
	
	private DataTransDialog dataTransDialog;
	
	private JPanel northPanel;
	private JTabbedPane centerTabPanel;
	private JPanel southPanel;
	
	private JAutoCompleteComboBox dataTransNameComboBox;
	private JLabel newDataTransLabel;
	private JLabel saveDataTransLabel;
	private JLabel saveToDataTransLabel;
	private JLabel deleteDataTransLabel;
	
	//常规面板组件
	private JLabel connectionLabel;
	private JAutoCompleteComboBox connectionComboBox;
	private JLabel databaseLabel;
	private JAutoCompleteComboBox datbaseComboBox;
	private JLabel databaseObjectLabel;
	private JList databaseObjectList;
	private JButton allSelectbButton;
	private JButton allDisselectButton;
	private JRadioButton connectionRadioButton;
	private JRadioButton fileRadioButton;
	private JAutoCompleteComboBox descModelComboBox;
	private JLabel descDatabaseLabel;
	private JAutoCompleteComboBox descDatbaseComboBox;
	
	//高级面板组件
	private JCheckBox advanceCreateTableCheckBox;
	private JCheckBox advanceContainsIndexCheckBox;
	private JCheckBox advanceContainsForeignKeyConstraintCheckBox;
	private JCheckBox advanceContainsEngineOrTableTypeCheckBox;
	private JCheckBox advanceContainsCharacterSetCheckBox;
	private JCheckBox advanceContainsAutoincrementCheckBox;
	private JCheckBox advanceContainsOtherSheetOptionCheckBox;
	private JCheckBox advanceContainsTriggerCheckBox;
	
	private JCheckBox advanceInsertRecordCheckBox;
	private JCheckBox advanceLockTableCheckBox;
	private JCheckBox advanceUsingTransactionCheckBox;
	private JCheckBox advanceCompleteInsertStatementsCheckBox;
	private JCheckBox advanceExpandableInsertStatementsCheckBox;
	private JCheckBox advanceDelayedInsertStatementsCheckBox;
	private JCheckBox advanceLOBHexFormatCheckBox;
	
	private JCheckBox advanceEncounteredErrorContinueCheckBox;
	private JCheckBox advanceLockSourceTableCheckBox;
	private JCheckBox advanceCreateDeletedBeforeTargetCheckBox;
	private JCheckBox advanceCreateTargetDatabaseCheckBox;
	private JCheckBox advanceUseShowCreateCheckBox;
	
	
	//信息日志组件
	private JLabel infoLogObjLabel;
	private JLabel infoLogProcessedRecordLabel;
	private JLabel infoLogTransmissionRecordLabel;
	private JLabel infoLogErrorLabel;
	private JLabel infoLogTimeLabel;
	private JTextArea infoLogTextArea;
	private JProgressBar infoLogProgressBar;
	
	private JButton startButton;
	private JButton closeButton;

	public DataTransDialogMainPanel(DataTransDialog dataTransDialog) {
		this.dataTransDialog = dataTransDialog;
		
		initNorthPanel();
		initCenterPanel();
		initSouth();
		addComponentPanel();
		initlisteners();
	}

	private void initNorthPanel() {
		northPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		dataTransNameComboBox = new JAutoCompleteComboBox();
		dataTransNameComboBox.setPrototypeDisplayValue("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		URL newResource = this.getClass().getResource("/image/dialog/newdatatrans.png");
		newDataTransLabel = new JLabel("新建");
		newDataTransLabel.setIcon(new ImageIcon(newResource));
		
		newDataTransLabel.addMouseListener(new NewDataTransAdapter());
		
		URL saveResource = this.getClass().getResource("/image/dialog/savedatatrans.png");
		saveDataTransLabel = new JLabel("保存");
		saveDataTransLabel.setIcon(new ImageIcon(saveResource));
		
		saveToDataTransLabel = new JLabel("另存为");
		URL saveToResource = this.getClass().getResource("/image/dialog/savetodatatrans.png");
		saveToDataTransLabel.setIcon(new ImageIcon(saveToResource));
		
		deleteDataTransLabel = new JLabel("删除");
		URL deleteResource = this.getClass().getResource("/image/dialog/deletedatatrans.png");
		deleteDataTransLabel.setIcon(new ImageIcon(deleteResource));
		
		northPanel.add(dataTransNameComboBox);
		northPanel.add(newDataTransLabel);
		northPanel.add(saveDataTransLabel);
		northPanel.add(saveToDataTransLabel);
		northPanel.add(deleteDataTransLabel);
	}

	private void initCenterPanel() {
		centerTabPanel = new JTabbedPane();
		
		JPanel commonPanel = new JPanel();
		JPanel advancePanel = new JPanel();
		JPanel infoLogPanel = new JPanel();
		
		initCommonPanel(commonPanel);
		initAdvancePanel(advancePanel);
		initInfoLogPanel(infoLogPanel);
		
		centerTabPanel.add(" 常规  ", commonPanel);
		centerTabPanel.add(" 高级  ", advancePanel);
		centerTabPanel.add("信息日志", infoLogPanel);
	}

	/**
	 * 功能描述：初始化常规面板 
	 */
	private void initCommonPanel(JPanel commonPanel) {
		commonPanel.setLayout(new GridLayout(1, 2));
		
		JPanel sourcePanel = new JPanel();
		JPanel descPanel = new JPanel();
		
		sourcePanel.setBackground(new Color(252, 252, 252));
		sourcePanel.setBorder(BorderUtils.createEmsBorder("源"));
		descPanel.setBackground(new Color(252, 252, 252));
		descPanel.setBorder(BorderUtils.createEmsBorder("目标"));
		
		sourcePanel.setLayout(new GridBagLayout());
		
		connectionLabel = new JLabel("连接:");
		connectionComboBox = new JAutoCompleteComboBox();
		databaseLabel = new JLabel("数据库:");
		datbaseComboBox = new JAutoCompleteComboBox();
		databaseObjectLabel = new JLabel("数据库对象:");
		
		databaseObjectList = new JList();
		databaseObjectList.setCellRenderer(new CheckListCellRenderer());
		JScrollPane databaseObjectScrollPane = new JScrollPane(databaseObjectList);
		
		JPanel btnPanel = new JPanel(new GridLayout(1, 3));
		
		allSelectbButton = new JButton("全选");
		allDisselectButton = new JButton("全部取消选择");
		JPanel nullPanel = new JPanel();
		nullPanel.setBackground(new Color(252, 252, 252));
		
		btnPanel.add(allSelectbButton);
		btnPanel.add(allDisselectButton);
		btnPanel.add(nullPanel);
		
		
		sourcePanel.add(connectionLabel,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(7, 5, 7, 5), 0, 0));
		sourcePanel.add(connectionComboBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		sourcePanel.add(databaseLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		sourcePanel.add(datbaseComboBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		sourcePanel.add(databaseObjectLabel,new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		sourcePanel.add(databaseObjectScrollPane,new GridBagConstraints(0, 5, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));
		sourcePanel.add(btnPanel,new GridBagConstraints(0, 6, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		
		
		
		descPanel.setLayout(new BorderLayout());
		
		JPanel descNorthPanel = new JPanel(new GridBagLayout());
		descNorthPanel.setBackground(new Color(252,252,252));
		
		ButtonGroup buttonGroup = new ButtonGroup();
		
		connectionRadioButton = new JRadioButton("连接");
		connectionRadioButton.setBackground(new Color(252,252,252));
		fileRadioButton = new JRadioButton("文件");
		fileRadioButton.setBackground(new Color(252,252,252));
		descModelComboBox = new JAutoCompleteComboBox();
		descDatabaseLabel = new JLabel("数据库:");
		descDatbaseComboBox = new JAutoCompleteComboBox();
		
		JPanel descModelPanel = new JPanel(new GridLayout(1, 2));
		
		buttonGroup.add(connectionRadioButton);
		buttonGroup.add(fileRadioButton);
		
		descModelPanel.add(connectionRadioButton);
		descModelPanel.add(fileRadioButton);
		
		
		descNorthPanel.add(descModelPanel,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		descNorthPanel.add(descModelComboBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST,GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		descNorthPanel.add(descDatabaseLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		descNorthPanel.add(descDatbaseComboBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		
		descPanel.add(descNorthPanel,BorderLayout.NORTH);
		
		commonPanel.add(sourcePanel);
		commonPanel.add(descPanel);
	}
	
	/**
	 * 功能描述：初始化高级面板
	 * @param advancePanel 
	 */
	private void initAdvancePanel(JPanel advancePanel) {
		advancePanel.setLayout(new GridLayout(1, 2));
		advancePanel.setBackground(new Color(252, 252, 252));
		
		JPanel tableOptionPanel = new JPanel();
		JPanel recordAndOtherOptionPanel = new JPanel();
		
		tableOptionPanel.setLayout(new BorderLayout());
		tableOptionPanel.setBorder(BorderUtils.createEmsBorder("表选项"));
		
		JPanel leftPanel =new JPanel();
		tableOptionPanel.add(leftPanel,BorderLayout.NORTH);
		
		leftPanel.setLayout(new GridBagLayout());
		
		advanceCreateTableCheckBox = new JCheckBox("创建表",true);
		advanceContainsIndexCheckBox = new JCheckBox("包含索引",true);
		advanceContainsForeignKeyConstraintCheckBox = new JCheckBox("包含外键限制",true);
		advanceContainsEngineOrTableTypeCheckBox = new JCheckBox("包含引擎或表类型",true);
		advanceContainsCharacterSetCheckBox = new JCheckBox("包含字符集",true);
		advanceContainsAutoincrementCheckBox = new JCheckBox("包含自动递增",true);
		advanceContainsOtherSheetOptionCheckBox = new JCheckBox("包含其他表选项",false);
		advanceContainsTriggerCheckBox = new JCheckBox("包含触发器",true);
		
		leftPanel.add(advanceCreateTableCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		leftPanel.add(advanceContainsIndexCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, 
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		leftPanel.add(advanceContainsForeignKeyConstraintCheckBox,new GridBagConstraints(0, 2, 1, 1,
				1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		leftPanel.add(advanceContainsEngineOrTableTypeCheckBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		leftPanel.add(advanceContainsCharacterSetCheckBox,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		leftPanel.add(advanceContainsAutoincrementCheckBox,new GridBagConstraints(0, 5, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		leftPanel.add(advanceContainsOtherSheetOptionCheckBox,new GridBagConstraints(0, 6, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		leftPanel.add(advanceContainsTriggerCheckBox,new GridBagConstraints(0, 7, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		
		setComponentOpaque(false,tableOptionPanel,leftPanel,
				advanceCreateTableCheckBox,advanceContainsIndexCheckBox,
				advanceContainsForeignKeyConstraintCheckBox,advanceContainsEngineOrTableTypeCheckBox,
				advanceContainsCharacterSetCheckBox,advanceContainsAutoincrementCheckBox,
				advanceContainsOtherSheetOptionCheckBox,advanceContainsTriggerCheckBox);
		
		recordAndOtherOptionPanel.setLayout(new GridBagLayout());
		recordAndOtherOptionPanel.setBackground(new Color(252,252,252));
		
		JPanel recordPanel = new JPanel(new BorderLayout());
		JPanel otherPanel = new JPanel(new GridBagLayout());
		
		recordPanel.setBorder(BorderUtils.createEmsBorder("记录选项"));
		
		JPanel recordContentPanel = new JPanel();
		recordContentPanel.setLayout(new GridBagLayout());
		
		recordPanel.add(recordContentPanel,BorderLayout.NORTH);
		
		
		advanceInsertRecordCheckBox = new JCheckBox("插入记录",true);
		advanceLockTableCheckBox = new JCheckBox("锁定目标表",false);
		advanceUsingTransactionCheckBox = new JCheckBox("使用事务",true);
		advanceCompleteInsertStatementsCheckBox = new JCheckBox("使用完整插入语句",false);
		advanceExpandableInsertStatementsCheckBox = new JCheckBox("使用扩展插入语句",true);
		advanceDelayedInsertStatementsCheckBox = new JCheckBox("使用延迟插入语句",false);
		advanceLOBHexFormatCheckBox = new JCheckBox("为   BLOB 使用十六进制格式",true);
		
		recordContentPanel.add(advanceInsertRecordCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		recordContentPanel.add(advanceLockTableCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		recordContentPanel.add(advanceUsingTransactionCheckBox,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		recordContentPanel.add(advanceCompleteInsertStatementsCheckBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		recordContentPanel.add(advanceExpandableInsertStatementsCheckBox,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		recordContentPanel.add(advanceDelayedInsertStatementsCheckBox,new GridBagConstraints(0, 5, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		recordContentPanel.add(advanceLOBHexFormatCheckBox,new GridBagConstraints(0, 6, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 30, 0, 5), 0, 0));
		
		
		otherPanel.setBorder(BorderUtils.createEmsBorder("其他选项"));
		
		
		advanceEncounteredErrorContinueCheckBox = new JCheckBox("遇到错误继续",false);
		advanceEncounteredErrorContinueCheckBox.setEnabled(false);
		advanceLockSourceTableCheckBox = new JCheckBox("锁定源表",false);
		advanceCreateDeletedBeforeTargetCheckBox = new JCheckBox("创建前删除目标对象",true);
		advanceCreateTargetDatabaseCheckBox = new JCheckBox("创建目标数据库或模式  (如果不存在)",true);
		advanceUseShowCreateCheckBox = new JCheckBox("使用 SHOW CREATE TABLE 中的 DDL",false);
		
		otherPanel.add(advanceEncounteredErrorContinueCheckBox,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		otherPanel.add(advanceLockSourceTableCheckBox,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		otherPanel.add(advanceCreateDeletedBeforeTargetCheckBox,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		otherPanel.add(advanceCreateTargetDatabaseCheckBox,new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 0, 5), 0, 0));
		otherPanel.add(advanceUseShowCreateCheckBox,new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(4, 5, 25, 5), 0, 0));
		
		
		setComponentOpaque(false,recordPanel,otherPanel,recordContentPanel,
				advanceLockTableCheckBox,advanceInsertRecordCheckBox,advanceUsingTransactionCheckBox,
				advanceCompleteInsertStatementsCheckBox,advanceExpandableInsertStatementsCheckBox,
				advanceDelayedInsertStatementsCheckBox,advanceLOBHexFormatCheckBox,
				advanceEncounteredErrorContinueCheckBox,advanceLockSourceTableCheckBox,
				advanceCreateDeletedBeforeTargetCheckBox,advanceCreateTargetDatabaseCheckBox,
				advanceUseShowCreateCheckBox);
		
		recordAndOtherOptionPanel.add(recordPanel,new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, 
				new Insets(5, 5, 0, 5), 0, 0));
		recordAndOtherOptionPanel.add(otherPanel,new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		
		advancePanel.add(tableOptionPanel);
		advancePanel.add(recordAndOtherOptionPanel);
	}

	/**
	 * 功能描述：初始化信息日志面板
	 * @param infoLogPanel
	 */
	private void initInfoLogPanel(JPanel infoLogPanel) {
		infoLogPanel.setLayout(new GridBagLayout());
		infoLogPanel.setBackground(new Color(252, 252, 252));
		
		infoLogObjLabel = new JLabel("对象:");
		infoLogProcessedRecordLabel = new JLabel("已处理记录:");
		infoLogTransmissionRecordLabel = new JLabel("已传输记录:");
		infoLogErrorLabel = new JLabel("错误:");
		infoLogTimeLabel = new JLabel("时间:");
		infoLogTextArea = new JTextArea();
		infoLogTextArea.setBackground(new Color(236,233,216));
		infoLogTextArea.setEditable(false);
		JScrollPane infoLogTextAreaPane = new JScrollPane(infoLogTextArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		infoLogProgressBar = new JProgressBar();
		
		infoLogPanel.add(infoLogObjLabel,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogProcessedRecordLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogTransmissionRecordLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogErrorLabel,new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,GridBagConstraints.NONE,
				new Insets(2, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogTimeLabel,new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 5, 0, 5), 0, 0));
		infoLogPanel.add(infoLogTextAreaPane,new GridBagConstraints(0, 5, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(13, 5, 5, 5), 0, 0));
		infoLogPanel.add(infoLogProgressBar,new GridBagConstraints(0, 6, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 
				new Insets(0, 5, 3, 5), 0, 0));
	}

	private void initSouth() {
		southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		startButton = new JButton("开始");
		closeButton = new JButton("关闭");
		
		southPanel.add(startButton);
		southPanel.add(closeButton);
	}

	private void addComponentPanel() {
		setLayout(new GridBagLayout());
		
		add(northPanel,new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0));
		add(centerTabPanel,new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));
		add(southPanel,new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
	}
	
	/**
	 * 功能描述：设置组件为透明
	 * @param components
	 */
	private void setComponentOpaque(boolean flag,Component...components) {
		for(Component component:components){
			if(component instanceof JPanel){
				JPanel panelComponent = (JPanel) component;
				panelComponent.setOpaque(flag);
			}else if(component instanceof JCheckBox){
				JCheckBox checkBoxComponent = (JCheckBox) component;
				checkBoxComponent.setOpaque(flag);
			}
		}
	}
	
	/**
	 * 功能描述：为标签添加鼠标响应事件
	 * @author Administrator
	 *
	 */
	private class NewDataTransAdapter extends MouseAdapter {
		public void mouseEntered(MouseEvent e) {
			if(e.getSource()==newDataTransLabel){
				newDataTransLabel.setBackground(Color.WHITE);
			}
		}

		public void mouseClicked(MouseEvent e) {
			if(e.getSource()==newDataTransLabel){
				newDataTransLabel.setBackground(Color.GRAY);
			}
		}
	}
	
	/**
	 * 功能描述：初始化组件监听
	 */
	private void initlisteners() {
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dataTransDialog.dispose();
			}
		});
	}

	
}




















