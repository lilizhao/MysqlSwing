package com.raisecom.view.dialog;

import java.net.URL;

import javax.swing.JDialog;

import com.raisecom.util.SwingConstants;

public class DataTransDialog extends JDialog{

	private static final long serialVersionUID = 3608156666513363227L;

	public DataTransDialog() {
		URL resource = this.getClass().getResource("/image/dialog/datatrans.png");
		setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		
		DataTransDialogMainPanel dataTransDialogMainPanel = new DataTransDialogMainPanel(this);
		
		setContentPane(dataTransDialogMainPanel);
		
		setModal(false);
		
		setTitle("���ݴ���");
		setSize(769,594);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	
}
