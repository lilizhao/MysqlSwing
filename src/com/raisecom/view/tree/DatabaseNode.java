package com.raisecom.view.tree;

import javax.swing.ImageIcon;

import com.raisecom.util.ImageUtils;

public class DatabaseNode extends ConnectionNode{

	public DatabaseNode(String nodeName) {
		super(nodeName);
	}

	public ImageIcon getImageIcon() {
		if("closed".equalsIgnoreCase(getState())){
			return ImageUtils.DATABASE_NODE_IMAGE;
		}else{
			return ImageUtils.DATABASE_NODE_IMAGE2;
		}
	}

}
