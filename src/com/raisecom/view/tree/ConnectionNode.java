package com.raisecom.view.tree;

import javax.swing.ImageIcon;

import com.raisecom.util.OperationUtils;

public abstract class ConnectionNode {
	
	private String nodeName;   //节点名称
	private String state = OperationUtils.CLOSESTATE; 	   //节点状态
	
	public ConnectionNode(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getNodeName() {
		return nodeName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 返回对应图标
	 * @return
	 */
	public abstract ImageIcon getImageIcon();

	public String toString() {
		return nodeName;
	}
	
	

}
