package com.raisecom.model.data;

import javax.swing.JRadioButton;

public class ToolBarRadioBtnBean implements Cloneable {

	private String actionCommand;
	private JRadioButton jRadioButton;
	private String isSelectedImage;
	private String isNotSelectedImage;

	public String getActionCommand() {
		return actionCommand;
	}

	public void setActionCommand(String actionCommand) {
		this.actionCommand = actionCommand;
	}

	public JRadioButton getjRadioButton() {
		return jRadioButton;
	}

	public void setjRadioButton(JRadioButton jRadioButton) {
		this.jRadioButton = jRadioButton;
	}

	public String getIsSelectedImage() {
		return isSelectedImage;
	}

	public void setIsSelectedImage(String isSelectedImage) {
		this.isSelectedImage = isSelectedImage;
	}

	public String getIsNotSelectedImage() {
		return isNotSelectedImage;
	}

	public void setIsNotSelectedImage(String isNotSelectedImage) {
		this.isNotSelectedImage = isNotSelectedImage;
	}

}
