package com.raisecom.model.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class JDBCConn {
	private String connectionName;
	
	private String host;
	private String username;
	private String password;
	private String port;

	
	public static Map<String, Connection> allConnections = new HashMap<String, Connection>();

	public JDBCConn() {
	}

	public JDBCConn(String connectionName, String host, String username,
			String password, String port) {
		this.connectionName = connectionName;
		this.host = host;
		this.username = username;
		this.password = password;
		this.port = port;
	}



	public Connection getConn() {
		try {
			Class.forName("org.gjt.mm.mysql.Driver");
			Connection conn = DriverManager.getConnection(
							"jdbc:mysql://" + host + ":" + port + "/information_schema?useUnicode=true&amp;characterEncoding=UTF-8",
							username, password);
			if(conn!=null){
				allConnections.put(connectionName, conn);
			}
			return conn;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (SQLException e) {
//			e.printStackTrace();
			return null;
		}
	}

	public ResultSet query(Connection conn, String sql) {
		try {
			if (conn == null) {
				throw new Exception("database connection can't use!");
			}
			if (sql == null)
				throw new Exception(
						"check your parameter: 'sql'! don't input null!");
			// 声明语句
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			return rs;
			
//			ResultSetMetaData rmeta = rs.getMetaData();
//			// 获得数据字段个数
//			int numColumns = rmeta.getColumnCount();
//			while (rs.next()) {
//				for (int i = 0; i < numColumns; i++) {
//					String sTemp = rs.getString(i + 1);
//					System.out.print(sTemp + "  ");
//				}
//				System.out.println("");
//			}
		} catch (Exception e) {
			e.printStackTrace();
//			System.out.println("query error:" + e);
		}
		return null;
	}
	
	/**
	 * 执行结果返回的行数
	 * @param conn
	 * @param sql
	 * @return
	 * @throws Exception 
	 */
	public int executeUpdate(Connection conn, String sql) throws Exception {
		int resultRows = 0;
		
		if (conn == null) {
			throw new Exception("database connection can't use!");
		}
		if (sql == null){
			throw new Exception("check your parameter: 'sql'! don't input null!");
		}
		
		Statement stmt = conn.createStatement();
		
		resultRows = stmt.executeUpdate(sql);
		
		return resultRows;
			
	}

	/**
	 * <br>
	 * 方法说明：执行插入、更新、删除等没有返回结果集的SQL语句 <br>
	 * 输入参数：Connection con 数据库连接 <br>
	 * 输入参数：String sql 要执行的SQL语句 <br>
	 * 返回类型：
	 */
	public void execute(Connection con, String sql) {
		try {
			if (con == null)
				return;
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			System.out.println("execute error: sql = " + sql);
			System.out.println(e);
		}
	}
	
	/**
	 * <br>
	 * 方法说明：主方法 <br>
	 * 输入参数： <br>
	 * 返回类型：
	 */
	public static void main(String[] arg) {
		JDBCConn jdbcConn = new JDBCConn();
		Connection conn = jdbcConn.getConn();
		jdbcConn.query(conn, "SHOW DATABASES;");
	}
}
