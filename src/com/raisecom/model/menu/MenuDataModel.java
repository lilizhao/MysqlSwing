package com.raisecom.model.menu;

import java.io.IOException;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

public class MenuDataModel {
	
	/**
	 * 获取所有连接的名称配置
	 * @return
	 */
	public Vector<String> getConecntionNameVectorData() {
		Vector<String> listData = new  Vector<String>();
		
		try {
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(System.getProperty("user.dir")+"\\config\\connections.ncx");
			
			Element rootElement = document.getRootElement();
			@SuppressWarnings("unchecked")
			List<Element> connectionNodes = XPath.selectNodes(rootElement, "/Connections/Connection");
			ListIterator<Element> listIterEles = connectionNodes.listIterator();
			while(listIterEles.hasNext()){
				Element nextEle = listIterEles.next();
				String connectionName = nextEle.getAttributeValue("ConnectionName");
				listData.add(connectionName);
			}
			
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listData;
	}
	
	/**
	 * 获取文件关联项
	 * @return
	 */
	public Vector<String> getOptionFileAssactionnVectorData() {
		Vector<String> listData = new  Vector<String>();
		listData.add(".npi - Import Profile");
		listData.add(".npe - Export Table Profile");
		listData.add(".npev - Export View Profile");
		listData.add(".npeq - Export Query Profile");
		listData.add(".npb - Backup Profile");
		listData.add(".npj - Batch Job Profile");
		listData.add(".npd - Data Synchronize Profile");
		listData.add(".nps - Structure Synchronize Profile");
		listData.add(".npt - Data Transfer Profile");
		listData.add(".ndm - Model Profile");
		
		return listData;
	}

}
