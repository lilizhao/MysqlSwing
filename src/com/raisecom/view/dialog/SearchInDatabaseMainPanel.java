package com.raisecom.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class SearchInDatabaseMainPanel extends JPanel {

	private static final long serialVersionUID = -6473484600218270544L;
	
	//初始化面板组件
	private JPanel centerPanel;
	private JPanel southPanel;
	
	private JLabel searchLabel;
	private JTextField searchTextField;
	private JLabel searchEscapeChar;
	private JComboBox containComboBox;
	private JButton searchButton;
	private JLabel searchResultLabel;
	private JTable searchTable;
	private JLabel readyLabel;

	public SearchInDatabaseMainPanel() {
		initCenterPanel();
		initSourthPanel();
		initComponent();
	}

	private void initCenterPanel() {
		centerPanel = new JPanel();
		centerPanel.setLayout(new GridBagLayout());
		
		searchLabel = new JLabel("查找:");
		searchTextField = new JTextField();
		searchEscapeChar = new JLabel("[\\]作为转义字符");
		containComboBox = initContainComboBox();
		containComboBox.setSelectedItem("包含");
		searchButton = new JButton("查找");
		searchButton.setBorder(new EmptyBorder(5, 25, 5, 25));
		searchButton.setEnabled(false);
		searchResultLabel = new JLabel("查找结果:");
		searchTable = initSearchTable();
		JScrollPane searchScrollPane = new JScrollPane(searchTable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		searchScrollPane.getViewport().setBackground(new Color(220, 220, 220));
		
		centerPanel.add(searchLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 8, 5, 5), 0, 0));
		centerPanel.add(searchTextField, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 55, 5, 5), 0, 0));
		centerPanel.add(searchEscapeChar, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 10, 5, 145), 0, 0));
		centerPanel.add(containComboBox, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 55, 5, 5), 0, 0));
		centerPanel.add(searchButton, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 15), 0, 0));
		centerPanel.add(searchResultLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 8, 5, 5), 0, 0));
		centerPanel.add(searchScrollPane, new GridBagConstraints(0, 3, 3, 1, 1.0, 1.0,
				GridBagConstraints.EAST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));

	}
	
	private JTable initSearchTable() {
		String[] columnNames = {"名","符合记录的数目",""};
		 Object[][] data = {
		            {"阿呆", "Taipei",new Integer(66), 
		              new Integer(32), new Integer(98), new Boolean(false),new Boolean(false)},
		            {"阿瓜", "ChiaYi",new Integer(85), 
		              new Integer(69), new Integer(154), new Boolean(true),new Boolean(false)},          
		        };
		JTable jTable = new JTable(data, columnNames);
		return jTable;
	}

	private JComboBox initContainComboBox() {
		Vector<String> containVector = new Vector<String>();
		containVector.add("包含");
		containVector.add("确切");
		containVector.add("前缀");
		containVector.add("正则表达式");
		return new JComboBox(containVector);
	}

	private void initSourthPanel() {
		southPanel = new JPanel();
		southPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		readyLabel = new JLabel("就绪。");
		southPanel.add(readyLabel);
	}
	
	private void initComponent() {
		setLayout(new BorderLayout());
		
		add(centerPanel, BorderLayout.CENTER);
		add(southPanel, BorderLayout.SOUTH);
	}
	
}
