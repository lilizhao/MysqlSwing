package com.raisecom.model.velocity;

import java.io.StringWriter;
import java.io.Writer;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;

public class VelocityGenerator<T> {

	private String vmPath; // 模板路径
	private String vmName; // 模板名称
	private String contextStr; //上下文标签
	private T t;  //存储上下文Bean
	
	public VelocityGenerator(String vmPath, String vmName,String contextStr) {
		this.vmPath = vmPath;
		this.vmName = vmName;
		this.contextStr = contextStr;
	}

	public String generateStringByVM() {
		// 获取模板引擎
		VelocityEngine ve = new VelocityEngine();
		// 模板文件所在的路径
		String path = vmPath;
		// 设置参数
		ve.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, path);
		// 处理中文问题
		ve.setProperty(Velocity.INPUT_ENCODING, "GBK");
		ve.setProperty(Velocity.OUTPUT_ENCODING, "GBK");
		try {
			// 初始化模板
			ve.init();
			// 获取模板Velocity模板的名称
			Template template = ve.getTemplate(vmName);
			// 获取上下文
			VelocityContext context = new VelocityContext();
			// 把数据填入上下文
			setVMParms(context);

			Writer stringWriter = new StringWriter();
			template.merge(context, stringWriter);
			
			return stringWriter.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void setT(T t) {
		this.t = t;
	}

	private void setVMParms(VelocityContext context) {
		context.put(contextStr, t);	
	}

}
