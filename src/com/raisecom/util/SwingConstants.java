package com.raisecom.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.tree.DefaultMutableTreeNode;

import com.raisecom.view.tree.BackupNode;
import com.raisecom.view.tree.EventNode;
import com.raisecom.view.tree.FunctionNode;
import com.raisecom.view.tree.ReportNode;
import com.raisecom.view.tree.SearchNode;
import com.raisecom.view.tree.TableNode;
import com.raisecom.view.tree.ViewNode;

public class SwingConstants {
	/**
	 * 功能描述：获取image对象
	 * @param imagePath
	 * @return
	 */
	public static Image getImageIcon(String imagePath){
		Toolkit tk=Toolkit.getDefaultToolkit();
		
        Image image=tk.createImage(imagePath);
        
        return image;
	}
	
	/**
	 * 为面板上的所有组件统一着色
	 * @param userPanel
	 */
	public static void setColor(Color color,JPanel...userPanels) {
		for(JPanel userPanel:userPanels){
			userPanel.setBackground(color);
			Component[] components = userPanel.getComponents();
			setColorForSubComponent(color, components);
		}
	}

	private static void setColorForSubComponent(Color color, Component[] components) {
		for(Component component:components){
			if(component instanceof JPanel){
				component.setBackground(color);
				
				Component[] subComponents = ((JPanel)component).getComponents();
				setColorForSubComponent(color,subComponents);
			}else if(component instanceof JTabbedPane){
				component.setBackground(color);
				
				Component[] subComponents = ((JTabbedPane)component).getComponents();
				setColorForSubComponent(color,subComponents);
				
			}else{
				component.setBackground(color);
			}
		}
	}
	
	/**
	 * 功能描述： 获取全部可用的字符集
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Vector getAllCharset(){
		Vector allCharsetVector = null;
		
        SortedMap<String, Charset> availableCharsets = Charset.availableCharsets();
        Set<String> keySet = availableCharsets.keySet();
        allCharsetVector = new Vector(keySet);
        
        return allCharsetVector; 
         
	}
	
	/**
	 * 获取系统支持的字体
	 * @return
	 */
	public static Vector<String> getAllSystemFont(){
		Vector<String> allFontNames = new Vector<String>();
		Font[] allFonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
		for(Font font:allFonts){
			allFontNames.add(font.getName());
		}
		
		return allFontNames;
	}
	
	public static Vector<String> getAllSystemFontSize(){
		Vector<String> fontSizes = new Vector<String>();
		fontSizes.add("9");
		fontSizes.add("10");
		fontSizes.add("12");
		fontSizes.add("14");
		fontSizes.add("16");
		fontSizes.add("18");
		fontSizes.add("20");
		fontSizes.add("22");
		fontSizes.add("24");
		fontSizes.add("26");
		fontSizes.add("28");
		fontSizes.add("36");
		fontSizes.add("48");
		fontSizes.add("72");
		
		return fontSizes;
	}
	
	/**
	 * 生成所有通用节点
	 * @return
	 */
	public static List<DefaultMutableTreeNode> generalCommonNode(){
		List<DefaultMutableTreeNode> connectionNodes = new ArrayList<DefaultMutableTreeNode>();
		
		TableNode tableNode = new TableNode("表");
		DefaultMutableTreeNode tableTreeNode = new DefaultMutableTreeNode(tableNode);
		ViewNode viewNode = new ViewNode("视图");
		DefaultMutableTreeNode viewTreeNode = new DefaultMutableTreeNode(viewNode);
		FunctionNode functionNode = new FunctionNode("函数");
		DefaultMutableTreeNode functionTreeNode = new DefaultMutableTreeNode(functionNode);
		EventNode eventNode = new EventNode("事件");
		DefaultMutableTreeNode eventTreeNode = new DefaultMutableTreeNode(eventNode);
		SearchNode searchNode = new SearchNode("查询");
		DefaultMutableTreeNode searchTreeNode = new DefaultMutableTreeNode(searchNode);
		ReportNode reportNode = new ReportNode("报表");
		DefaultMutableTreeNode reportTreeNode = new DefaultMutableTreeNode(reportNode);
		BackupNode backupNode = new BackupNode("备份");
		DefaultMutableTreeNode backupTreeNode = new DefaultMutableTreeNode(backupNode);
		
		connectionNodes.add(tableTreeNode);
		connectionNodes.add(viewTreeNode);
		connectionNodes.add(functionTreeNode);
		connectionNodes.add(eventTreeNode);
		connectionNodes.add(searchTreeNode);
		connectionNodes.add(reportTreeNode);
		connectionNodes.add(backupTreeNode);
		
		return connectionNodes;
	}

	/**
	 * 获取通用节点类名
	 * @return
	 */
	public static List<String> getCommonNodeClass() {
		List<String> comonNodeClassList = new LinkedList<String>();
		
		comonNodeClassList.add("com.raisecom.view.tree.TableNode");
		comonNodeClassList.add("com.raisecom.view.tree.ViewNode");
		comonNodeClassList.add("com.raisecom.view.tree.FunctionNode");
		comonNodeClassList.add("com.raisecom.view.tree.EventNode");
		comonNodeClassList.add("com.raisecom.view.tree.SearchNode");
		comonNodeClassList.add("com.raisecom.view.tree.ReportNode");
		comonNodeClassList.add("com.raisecom.view.tree.BackupNode");
		
		return comonNodeClassList;
	}
	
	/**
	 * 获取选择列记录
	 * @return
	 */
	public static Map<String,List<String>> getSelectColumns() {
		Map<String,List<String>> selectColumnTreeNodes = new LinkedHashMap<String, List<String>>();
		
		selectColumnTreeNodes.put("    用户", Arrays.asList("用户;每小时最多连接数;每小时最多更新数;最大连接数;每小时最多查询数;SSL 类型".split(";")));
		selectColumnTreeNodes.put("    表", Arrays.asList("表;表类型;创建日期;修改日期;数据长度;索引长度;行;行格式;自动递增数值;最大数据长度;检查时间;数据空闲;创建选项;排序规则;注释;组".split(";")));
		selectColumnTreeNodes.put("    视图", Arrays.asList("视图;组;检查选项;安全性类型;定义者;可以更新".split(";")));
		selectColumnTreeNodes.put("    函数", Arrays.asList("函数;组;安全性类型;定义者;修改日期;存储类型;注释;数据访问;决定性;创建日期".split(";")));
		selectColumnTreeNodes.put("    事件", Arrays.asList("事件;运行时间;状态;区间字段;区间值;事件重复类型;定义者;创建日期;修改日期;STARTS;ON COMPLETION;ENDS;注释;组".split(";")));
		selectColumnTreeNodes.put("    查询", Arrays.asList("查询;文件大小;创建日期;修改日期;组".split(";")));
		selectColumnTreeNodes.put("    报表", Arrays.asList("报表;文件大小;创建日期;修改日期;组".split(";")));
		selectColumnTreeNodes.put("    备份", Arrays.asList("备份;文件大小;创建日期;修改日期;组".split(";")));
		selectColumnTreeNodes.put("    计划", Arrays.asList("计划;文件大小;创建日期;修改日期;组".split(";")));
		
		return selectColumnTreeNodes;
	}
	
}
















