package com.raisecom.view.custom;

import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.raisecom.view.tree.ConnectionNode;

public class ConnectTreeCellRender extends DefaultTreeCellRenderer {

	private static final long serialVersionUID = -8435334257687790749L;

	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean sel, boolean expanded, boolean leaf, int row,
			boolean hasFocus){
		super.getTreeCellRendererComponent(tree, value, sel, 
				expanded, leaf, row, hasFocus);
		
//		setClosedIcon(null);
		
		//得到树节点
		DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode)value;
		//得到节点对象
		Object userObject = treeNode.getUserObject();
		
		if(userObject instanceof ConnectionNode){
			ConnectionNode treeNodeInfo = (ConnectionNode)treeNode.getUserObject();
			//设置图片
			if (treeNodeInfo != null) {
				this.setText(treeNodeInfo.getNodeName());
				this.setIcon(treeNodeInfo.getImageIcon());
			}
		}
		
		return this;
		
	}
	
	

}
