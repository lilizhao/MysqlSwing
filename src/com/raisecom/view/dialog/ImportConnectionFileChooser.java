package com.raisecom.view.dialog;

import javax.swing.JFileChooser;

public class ImportConnectionFileChooser extends JFileChooser{
	private static final long serialVersionUID = -2785459776828975693L;

	public ImportConnectionFileChooser() {
		setFileSelectionMode(JFileChooser.FILES_ONLY);
	}
	
}
