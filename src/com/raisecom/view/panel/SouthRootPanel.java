package com.raisecom.view.panel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import com.raisecom.model.data.ServiceContext;
import com.raisecom.view.tree.ConnectionNameNode;
import com.raisecom.view.tree.ConnectionTree;
import com.raisecom.view.tree.DatabaseNode;
import com.raisecom.view.tree.EventNode;
import com.raisecom.view.tree.FunctionNode;
import com.raisecom.view.tree.TableNode;
import com.raisecom.view.tree.ViewNode;

public class SouthRootPanel extends JPanel{

	private static final long serialVersionUID = 5680453199222153342L;

	private ConnectionTree connTree;
	private JLabel connectionNameLabel;
	private JLabel serverNameLabel;
	private JLabel userNameLabel;
	private JLabel databseNameLabel;

	public SouthRootPanel(ConnectionTree connTree) {
		this.connTree = connTree;
		
		initViewComponent(connTree);
		initViewPanel();
	}

	/**
	 * 初始化界面组件
	 * @param connTree 
	 */
	private void initViewComponent(ConnectionTree connTree) {
		DefaultMutableTreeNode connRoot = (DefaultMutableTreeNode) connTree.getModel().getRoot();
		int connectionCount = connRoot.getChildCount();
		
		connectionNameLabel = new JLabel(connectionCount + " 服务器");
		
		serverNameLabel = new JLabel();
		
		userNameLabel = new JLabel();
		
		databseNameLabel = new JLabel();
		
		connTree.addTreeSelectionListener(new ConnTreeSelectionListener(this));
		
	}

	/**
	 * 绘制界面
	 * @param southRootPanel 
	 */
	private void initViewPanel() {
		setLayout(new FlowLayout(FlowLayout.LEFT));
		
		Box southBox = Box.createHorizontalBox();
		southBox.add(connectionNameLabel);
		southBox.add(Box.createHorizontalStrut(200));
		southBox.add(serverNameLabel);
		southBox.add(Box.createHorizontalStrut(20));
		southBox.add(userNameLabel);
		southBox.add(Box.createHorizontalStrut(20));
		southBox.add(databseNameLabel);
		
		add(southBox,BorderLayout.CENTER);
	}
	
	/**
	 * 树节点选择事件
	 * @author Administrator
	 *
	 */
	private class ConnTreeSelectionListener implements TreeSelectionListener {
		private SouthRootPanel southRootPanel;
		
		public ConnTreeSelectionListener(SouthRootPanel southRootPanel) {
			this.southRootPanel = southRootPanel;
		}

		public void valueChanged(TreeSelectionEvent e) {
			TreePath selPath = e.getPath();
			
			setFirstLabel(selPath);
			
			DefaultMutableTreeNode lastSelectTreeNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
			
			ConnectionNameNode connectionNameNodeObj = getConnectionNameNode(lastSelectTreeNode);
			DatabaseNode databaseNameNodeObj = getDatabaseNameNode(lastSelectTreeNode);
			
			serverNameLabel.setText(" "+connectionNameNodeObj.toString());
			serverNameLabel.setIcon(new ImageIcon(SouthRootPanel.class.getResource("/image/connection/connection.png")));

			ServiceContext serviceContext = connTree.getDataContext().getConnectionDataContext().get(serverNameLabel.getText().trim());
			String userName = serviceContext.getUserName();
			
			userNameLabel.setText("用户: " + userName);
			
			if(databaseNameNodeObj == null){
				databseNameLabel.setText("");
			}else{
				databseNameLabel.setText("数据库: "+databaseNameNodeObj.toString());
			}
			
			
			SwingUtilities.updateComponentTreeUI(southRootPanel);
		}

		/**
		 * 设置第一层标示
		 * @param selPath
		 */
		private void setFirstLabel(TreePath selPath) {
			Object[] selectPaths = selPath.getPath();
			if(selectPaths.length == 2){
				DefaultMutableTreeNode selectConncetionTreeNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
				int childCount = selectConncetionTreeNode.getChildCount();
				
				if(childCount!=0){
					connectionNameLabel.setText(childCount + " 数据库");
				}
			}else if(selectPaths.length == 3){
				DefaultMutableTreeNode selectDatabaseTreeNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
				int siblingCount = selectDatabaseTreeNode.getSiblingCount();
				
				connectionNameLabel.setText(siblingCount + " 数据库");
			}else if(selectPaths.length == 4){
				DefaultMutableTreeNode selectConncetionTreeNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
				Object userObject = selectConncetionTreeNode.getUserObject();
				if(userObject instanceof TableNode){
					setFirLableForFourthCommonNode(selectConncetionTreeNode,"表");
				}else if(userObject instanceof ViewNode){
					setFirLableForFourthCommonNode(selectConncetionTreeNode,"视图");
				}else if(userObject instanceof FunctionNode){
					setFirLableForFourthCommonNode(selectConncetionTreeNode,"函数");
				}else if(userObject instanceof EventNode){
					setFirLableForFourthCommonNode(selectConncetionTreeNode,"事件");
				}
			}else if(selectPaths.length == 5){
				DefaultMutableTreeNode selectConncetionTreeNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
				Object userObject = selectConncetionTreeNode.getUserObject();
				if(userObject instanceof TableNode){
					setFirLableForFifthCommonNode(selectConncetionTreeNode,"表");
				}else if(userObject instanceof ViewNode){
					setFirLableForFifthCommonNode(selectConncetionTreeNode,"视图");
				}else if(userObject instanceof FunctionNode){
					setFirLableForFifthCommonNode(selectConncetionTreeNode,"函数");
				}else if(userObject instanceof EventNode){
					setFirLableForFifthCommonNode(selectConncetionTreeNode,"事件");
				}
			}
		}

		/**
		 * 选中第四层时的响应事件
		 * 选中常规节点时设置底部面板第一个标识
		 * @param selectConncetionTreeNode
		 */
		private void setFirLableForFourthCommonNode(DefaultMutableTreeNode selectConncetionTreeNode,String nodeText) {
			int childCount = selectConncetionTreeNode.getChildCount();
			
			if(childCount!=0){
				connectionNameLabel.setText(childCount + " " + nodeText);
			}
		}
		
		/**
		 * 选中第五层时的响应事件
		 * 选中常规节点时设置底部面板第一个标识
		 * @param selectConncetionTreeNode
		 * @param string
		 */
		private void setFirLableForFifthCommonNode(DefaultMutableTreeNode selectConncetionTreeNode, String nodeText) {
			int siblingCount = selectConncetionTreeNode.getSiblingCount();
			
			if(siblingCount!=0){
				connectionNameLabel.setText(siblingCount + " " + nodeText);
			}
		}

		/**
		 * 获取选叶子最上层数据库信息,可能为空
		 * @param lastSelectTreeNode
		 */
		private DatabaseNode getDatabaseNameNode(DefaultMutableTreeNode lastSelectTreeNode) {
			Object userObject = lastSelectTreeNode.getUserObject();
			if(userObject instanceof ConnectionNameNode){
				return null;
			}else if(userObject instanceof DatabaseNode){
				return (DatabaseNode) userObject;
			}else{
				return getDatabaseNameNode((DefaultMutableTreeNode) lastSelectTreeNode.getParent());
			}
		}

		/**
		 * 获取选中叶子最上层连接名信息
		 * @param lastSelectTreeNode
		 */
		private ConnectionNameNode getConnectionNameNode(DefaultMutableTreeNode lastSelectTreeNode) {
			Object userObject = lastSelectTreeNode.getUserObject();
			
			if(userObject instanceof ConnectionNameNode){
				return (ConnectionNameNode) userObject;
			}else{
				return getConnectionNameNode((DefaultMutableTreeNode) lastSelectTreeNode.getParent());
			}
		}
	}
	

}
