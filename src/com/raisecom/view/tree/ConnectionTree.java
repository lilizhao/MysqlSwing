package com.raisecom.view.tree;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import com.raisecom.model.data.ConnectionDataContext;
import com.raisecom.view.custom.ConnectTreeCellRender;

public class ConnectionTree extends JTree{

	private static final long serialVersionUID = 351169654573134200L;
	
	private ConnectionDataContext dataContext;

	public ConnectionTree(DefaultMutableTreeNode root) {
		super(root);
		this.setRootVisible(false);
		this.setShowsRootHandles(true);
		this.setCellRenderer(new ConnectTreeCellRender());
		
		//展开第一层节点
//		for (int i = 0; i < root.getChildCount(); i++) {
//			DefaultMutableTreeNode node = (DefaultMutableTreeNode)root.getChildAt(i);
//			this.expandPath(new TreePath(node.getPath()));
//		}
	}

	public ConnectionTree(ConnectionDataContext dataContext) {
		this.dataContext = dataContext;
		this.setRootVisible(false);
		this.setShowsRootHandles(true);
		this.setCellRenderer(new ConnectTreeCellRender());
		
	}

	public ConnectionDataContext getDataContext() {
		return dataContext;
	}
	
	
	
}
	
