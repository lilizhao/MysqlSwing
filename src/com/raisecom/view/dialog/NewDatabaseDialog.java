package com.raisecom.view.dialog;

import java.net.URL;

import javax.swing.JDialog;

import com.raisecom.util.SwingConstants;
import com.raisecom.view.tree.ConnectionTree;

public class NewDatabaseDialog extends JDialog{

	private static final long serialVersionUID = 3608156666513363227L;

	public NewDatabaseDialog(ConnectionTree connTree, String sourceTreeNode) {
		URL resource = this.getClass().getResource("/image/dialog/newdatabase.png");
		setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		
		NewDatabaseMainPanel databaseMainPanel = new NewDatabaseMainPanel(this, connTree, sourceTreeNode);
		
		setContentPane(databaseMainPanel);
		
		setModal(true);
		
		setTitle("�½����ݿ�");
		setSize(448,394);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	
}
