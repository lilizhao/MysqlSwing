package com.raisecom.view.dialog;

import java.net.URL;

import javax.swing.JDialog;

import com.raisecom.util.SwingConstants;
import com.raisecom.view.tree.ConnectionTree;

public class RunSqlDialog extends JDialog{

	private static final long serialVersionUID = 3608156666513363227L;

	public RunSqlDialog(ConnectionTree connTree) {
		URL resource = this.getClass().getResource("/image/dialog/datasynch.png");
		setIconImage(SwingConstants.getImageIcon(resource.getPath()));
		
		RunSqlMainPanel runSqlMainPanel = new RunSqlMainPanel(connTree, this);
		
		setContentPane(runSqlMainPanel);
		
		setModal(false);
		setResizable(false);
		
		setTitle("���� SQL �ļ�");
		setSize(408,354);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	
}
