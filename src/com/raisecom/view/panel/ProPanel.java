package com.raisecom.view.panel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.raisecom.control.listaction.DataListMouseAdapter;
import com.raisecom.control.treeaction.ConnectionTreeMouseAdapter;
import com.raisecom.view.custom.MyListCellRenderer;
import com.raisecom.view.tree.ConnectionTree;

/**
 * 属性面板
 * @author lilz-2686
 */
public class ProPanel extends JPanel{

	private static final long serialVersionUID = 43945479998802678L;

	private ConnectionTree connTree;
	
	//定义界面组件
	private JPanel toolPanel;
	private JList dataList;
	private JScrollPane dataScrollPane;
	
	public ProPanel(ConnectionTree connTree) {
		this.connTree = connTree;
		
		initViewComponent();
		initListener();
	}
	
	/**
	 * 初始化界面组件
	 */
	private void initViewComponent() {
		setLayout(new BorderLayout());
		
		initNorthPanel();
		initCenterPanel();
		
		add(toolPanel, BorderLayout.NORTH);
		add(dataScrollPane, BorderLayout.CENTER);
	}

	/**
	 * 初始化North界面
	 */
	private void initNorthPanel() {
		toolPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		toolPanel.add(new JLabel("打开表"));
	}

	/**
	 * 初始化Center界面
	 */
	private void initCenterPanel() {
		dataList = new JList();
		//设置先纵向后横向滚动 
		dataList.setLayoutOrientation(JList.VERTICAL_WRAP);
		dataList.setFixedCellHeight(16);
		dataList.setFixedCellWidth(250);
		
		//VERTICAL  在单个列中垂直布置单元。  
		//HORIZONTAL_WRAP  水平布置单元，根据需要将单元包装到新行中。如果 visibleRowCount 属性小于等于 0，则包装由该列表的宽度确定；否则，以确保列表中 visibleRowCount 行的方式进行包装。  
		//VERTICAL_WRAP  垂直布置单元，根据需要将单元包装到新列中。如果 visibleRowCount 属性小于等于 0，则包装由该列表的宽度确定；否则，在 visibleRowCount 行进行包装。 
		dataList.setVisibleRowCount(0);   
		dataList.setCellRenderer(new MyListCellRenderer());
		dataList.addMouseListener(new DataListMouseAdapter(dataList));
		
		dataScrollPane = new JScrollPane(dataList, JScrollPane.VERTICAL_SCROLLBAR_NEVER,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}

	/**
	 * 初始化组件监听
	 */
	private void initListener() {
		connTree.addMouseListener(new ConnectionTreeMouseAdapter(connTree, dataList));
	}

}
