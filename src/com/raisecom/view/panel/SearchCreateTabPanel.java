package com.raisecom.view.panel;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

import com.raisecom.view.tree.ConnectionTree;

public class SearchCreateTabPanel extends JTabbedPane {

	private static final long serialVersionUID = 164340930804224344L;
	
	private ConnectionTree connTree;

	public SearchCreateTabPanel(ConnectionTree connTree) {
		this.connTree = connTree;
		
		initViewComponent();
	}

	private void initViewComponent() {
		JPanel leftComponent = initLeftComponent();
//		initRightComponent();
		
		addTab("查询创建工具", leftComponent);
		addTab("查询编辑器", new JPanel());
	}

	private JPanel initLeftComponent() {
		JPanel leftPanel = new JPanel(); 
		
		leftPanel.setLayout(new BorderLayout());
		
		JSplitPane leftSplitPane = new JSplitPane();
		
		return leftPanel;
	}
	
}
















