package com.raisecom.model.velocity;

public class ConnectionInfoBean {
	
	//General Information
	private String connectionName;
	private String hostName;
	private String port;
	private String userName;
	private String password;
	private boolean savePassword;
	
	//Advanced Information
	private String savePath;
	private String code;
	private String keepConnectionInterval;
	private boolean useMysqlCharSet;
	private boolean useAutoCompression;
	private boolean useAutoConnection;
	private boolean useAdvanceConnection;
	
	//SSL Information
	private boolean useSSL;
	private boolean useAuthen;
	private String clientKey;
	private String clientCert;
	private String caCert;
	
	//SSH Information
	private boolean useSSH;
	private String sshHost;
	private String sshPort;
	private String sshUserName;
	private String authenMethod;
	private boolean sshSavePassword;
	
	//HTTP Information
	private boolean useHttpChannel;
	private String httpChannelAddress;
	private boolean useHttpBase64Code;
	private boolean useHttpPasswordAuthen;
	private String httpUserName;
	private boolean httpSavePassword;
	private boolean httpUseCertAuthen;
	private String httpClientKey;
	private String httpClientCert;
	private String httpCaCert;
	private boolean useHttpProxy;
	private String httpProxyServerHost;
	private String httpProxyServerPort;
	private String httpProxyServerUserName;
	private boolean httpProxyServerSavePassword;
	
	//Other Information
	private String serverVersion;
	private String communicationProtocol;
	private String serverInformation;
	
	public boolean isUseHttpChannel() {
		return useHttpChannel;
	}
	public void setUseHttpChannel(boolean useHttpChannel) {
		this.useHttpChannel = useHttpChannel;
	}
	public String getHttpChannelAddress() {
		return httpChannelAddress;
	}
	public void setHttpChannelAddress(String httpChannelAddress) {
		this.httpChannelAddress = httpChannelAddress;
	}
	public boolean isUseHttpBase64Code() {
		return useHttpBase64Code;
	}
	public void setUseHttpBase64Code(boolean useHttpBase64Code) {
		this.useHttpBase64Code = useHttpBase64Code;
	}
	public boolean isUseHttpPasswordAuthen() {
		return useHttpPasswordAuthen;
	}
	public void setUseHttpPasswordAuthen(boolean useHttpPasswordAuthen) {
		this.useHttpPasswordAuthen = useHttpPasswordAuthen;
	}
	public String getHttpUserName() {
		return httpUserName;
	}
	public void setHttpUserName(String httpUserName) {
		this.httpUserName = httpUserName;
	}
	public boolean isHttpSavePassword() {
		return httpSavePassword;
	}
	public void setHttpSavePassword(boolean httpSavePassword) {
		this.httpSavePassword = httpSavePassword;
	}
	public boolean isHttpUseCertAuthen() {
		return httpUseCertAuthen;
	}
	public void setHttpUseCertAuthen(boolean httpUseCertAuthen) {
		this.httpUseCertAuthen = httpUseCertAuthen;
	}
	public String getHttpClientKey() {
		return httpClientKey;
	}
	public void setHttpClientKey(String httpClientKey) {
		this.httpClientKey = httpClientKey;
	}
	public String getHttpClientCert() {
		return httpClientCert;
	}
	public void setHttpClientCert(String httpClientCert) {
		this.httpClientCert = httpClientCert;
	}
	public String getHttpCaCert() {
		return httpCaCert;
	}
	public void setHttpCaCert(String httpCaCert) {
		this.httpCaCert = httpCaCert;
	}
	public boolean isUseHttpProxy() {
		return useHttpProxy;
	}
	public void setUseHttpProxy(boolean useHttpProxy) {
		this.useHttpProxy = useHttpProxy;
	}
	public String getHttpProxyServerHost() {
		return httpProxyServerHost;
	}
	public void setHttpProxyServerHost(String httpProxyServerHost) {
		this.httpProxyServerHost = httpProxyServerHost;
	}
	public String getHttpProxyServerPort() {
		return httpProxyServerPort;
	}
	public void setHttpProxyServerPort(String httpProxyServerPort) {
		this.httpProxyServerPort = httpProxyServerPort;
	}
	public String getHttpProxyServerUserName() {
		return httpProxyServerUserName;
	}
	public void setHttpProxyServerUserName(String httpProxyServerUserName) {
		this.httpProxyServerUserName = httpProxyServerUserName;
	}
	public boolean isHttpProxyServerSavePassword() {
		return httpProxyServerSavePassword;
	}
	public void setHttpProxyServerSavePassword(boolean httpProxyServerSavePassword) {
		this.httpProxyServerSavePassword = httpProxyServerSavePassword;
	}
	public String getServerVersion() {
		return serverVersion;
	}
	public void setServerVersion(String serverVersion) {
		this.serverVersion = serverVersion;
	}
	public String getCommunicationProtocol() {
		return communicationProtocol;
	}
	public void setCommunicationProtocol(String communicationProtocol) {
		this.communicationProtocol = communicationProtocol;
	}
	public String getServerInformation() {
		return serverInformation;
	}
	public void setServerInformation(String serverInformation) {
		this.serverInformation = serverInformation;
	}
	public boolean isUseSSH() {
		return useSSH;
	}
	public void setUseSSH(boolean useSSH) {
		this.useSSH = useSSH;
	}
	public String getSshHost() {
		return sshHost;
	}
	public void setSshHost(String sshHost) {
		this.sshHost = sshHost;
	}
	public String getSshPort() {
		return sshPort;
	}
	public void setSshPort(String sshPort) {
		this.sshPort = sshPort;
	}
	public String getSshUserName() {
		return sshUserName;
	}
	public void setSshUserName(String sshUserName) {
		this.sshUserName = sshUserName;
	}
	public String getAuthenMethod() {
		return authenMethod;
	}
	public void setAuthenMethod(String authenMethod) {
		this.authenMethod = authenMethod;
	}
	public boolean isSshSavePassword() {
		return sshSavePassword;
	}
	public void setSshSavePassword(boolean sshSavePassword) {
		this.sshSavePassword = sshSavePassword;
	}
	public String getConnectionName() {
		return connectionName;
	}
	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isSavePassword() {
		return savePassword;
	}
	public void setSavePassword(boolean savePassword) {
		this.savePassword = savePassword;
	}
	public String getSavePath() {
		return savePath;
	}
	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getKeepConnectionInterval() {
		return keepConnectionInterval;
	}
	public void setKeepConnectionInterval(String keepConnectionInterval) {
		this.keepConnectionInterval = keepConnectionInterval;
	}
	public boolean isUseMysqlCharSet() {
		return useMysqlCharSet;
	}
	public void setUseMysqlCharSet(boolean useMysqlCharSet) {
		this.useMysqlCharSet = useMysqlCharSet;
	}
	public boolean isUseAutoCompression() {
		return useAutoCompression;
	}
	public void setUseAutoCompression(boolean useAutoCompression) {
		this.useAutoCompression = useAutoCompression;
	}
	public boolean isUseAutoConnection() {
		return useAutoConnection;
	}
	public void setUseAutoConnection(boolean useAutoConnection) {
		this.useAutoConnection = useAutoConnection;
	}
	public boolean isUseAdvanceConnection() {
		return useAdvanceConnection;
	}
	public void setUseAdvanceConnection(boolean useAdvanceConnection) {
		this.useAdvanceConnection = useAdvanceConnection;
	}
	public boolean isUseSSL() {
		return useSSL;
	}
	public void setUseSSL(boolean useSSL) {
		this.useSSL = useSSL;
	}
	public boolean isUseAuthen() {
		return useAuthen;
	}
	public void setUseAuthen(boolean useAuthen) {
		this.useAuthen = useAuthen;
	}
	public String getClientKey() {
		return clientKey;
	}
	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}
	public String getClientCert() {
		return clientCert;
	}
	public void setClientCert(String clientCert) {
		this.clientCert = clientCert;
	}
	public String getCaCert() {
		return caCert;
	}
	public void setCaCert(String caCert) {
		this.caCert = caCert;
	}
	
	
	
	
}











