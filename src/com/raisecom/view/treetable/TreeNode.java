package com.raisecom.view.treetable;
import java.util.ArrayList;
import java.util.List;

public class TreeNode {
	private String name;
	private List<TreeNode> children = new ArrayList<TreeNode>();

	public TreeNode() {
	}

	public TreeNode(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public List<TreeNode> getChildren() {
		return children;
	}

	public String toString() {
		return "MyTreeNode: " + name;
	}
}
