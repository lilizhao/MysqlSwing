package com.raisecom.view.tree;

import javax.swing.ImageIcon;

import com.raisecom.util.ImageUtils;

public class BackupNode extends ConnectionNode{

	public BackupNode(String nodeName) {
		super(nodeName);
	}

	public ImageIcon getImageIcon() {
		return ImageUtils.BACKUP_NODE_IMAGE;
	}

}
