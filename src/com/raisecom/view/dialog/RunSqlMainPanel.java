package com.raisecom.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.raisecom.util.FileOrDirectoryChooser;
import com.raisecom.util.SwingConstants;
import com.raisecom.view.custom.JAutoCompleteComboBox;
import com.raisecom.view.custom.MyFileFilter;
import com.raisecom.view.tree.ConnectionTree;

public class RunSqlMainPanel extends JPanel{

	private static final long serialVersionUID = -4208137703316093695L;
	
	private ConnectionTree connTree;
	private RunSqlDialog runSqlDialog;
	
	private JTabbedPane tabbedPane;
	private JPanel buttonPanel;
	
	//常规界面组件
	private JLabel serverLabel;
	private JLabel serverIPLabel;
	private JLabel databaseLabel;
	private JLabel databaseNameLabel;
	private JLabel fileLabel;
	private JTextField fileTextField;
	private JButton fileBroswerButton;
	private JLabel codeLabel;
	private JAutoCompleteComboBox codeAutoCompleteComboBox;
	private JCheckBox continueWhenErrorCheckBox;
	private JCheckBox runMultiTaskCheckBox;
	private JCheckBox setAutoCommitCheckBox;
	
	//信息日志界面组件
	private JLabel searchLabel;
	private JLabel searchValueLabel;
	private JLabel processedLabel;
	private JLabel processedValueLabel;
	private JLabel errorLabel;
	private JLabel errorValueLabel;
	private JLabel timeLabel;
	private JLabel timeValueLabel;
	private JTextArea infoLogTextArea;
	private JProgressBar infoLogProgressBar;
	
	//底部面板组件
	private JButton beginButton;
	private JButton closeButton;
	
	public RunSqlMainPanel(ConnectionTree connTree, RunSqlDialog runSqlDialog) {
		this.connTree = connTree;
		this.runSqlDialog = runSqlDialog;
		
		initTabPane();
		initBtnPane();
		addViewComponent();
		initListener();
	}

	/**
	 * 初始化界面组件监听
	 */
	private void initListener() {
		fileBroswerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FileOrDirectoryChooser fileChooser = new FileOrDirectoryChooser(fileTextField.getText(),
						JFileChooser.FILES_ONLY,fileTextField);
				fileChooser.setAcceptAllFileFilterUsed(false);
				fileChooser.setFileFilter(new MyFileFilter(".sql", "*.sql"));
				fileChooser.showOpenDialog(runSqlDialog);
			}
		});
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runSqlDialog.dispose();
			}
		});
	}

	/**
	 * 添加面板组件到界面
	 */
	private void addViewComponent() {
		this.setLayout(new BorderLayout());
		
		add(tabbedPane, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * 初始化底部按钮面板
	 */
	private void initBtnPane() {
		buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		beginButton = new JButton("开始");
		beginButton.setBorder(new EmptyBorder(new Insets(5, 25, 5, 25)));
		closeButton = new JButton("关闭");
		closeButton.setBorder(new EmptyBorder(new Insets(5, 25, 5, 25)));
		
		buttonPanel.add(beginButton);
		buttonPanel.add(closeButton);
	}

	/**
	 * 初始化tab面板
	 */
	private void initTabPane() {
		tabbedPane = new JTabbedPane();
		tabbedPane.setBorder(new EmptyBorder(new Insets(5, 10, 2, 10)));
		
		JPanel commonPanel = new JPanel();
		JPanel inforLogPanel = new JPanel();
		
		initCommonPanel(commonPanel);
		initInfoLogPanel(inforLogPanel);
		
		tabbedPane.add(" 常规  ", commonPanel);
		tabbedPane.add("信息日志", inforLogPanel);
	}

	/**
	 * 初始化信息
	 * @param inforLogPanel
	 */
	private void initInfoLogPanel(JPanel inforLogPanel) {
		inforLogPanel.setLayout(new GridBagLayout());
		inforLogPanel.setBackground(new Color(251, 251, 251));
		
		searchLabel = new JLabel("查询:");
		searchValueLabel = new JLabel("0");
		processedLabel = new JLabel("已处理:");
		processedValueLabel = new JLabel("0");
		errorLabel = new JLabel("错误:");
		errorValueLabel = new JLabel("0");
		timeLabel = new JLabel("时间:");
		timeValueLabel = new JLabel("N/A");
		infoLogTextArea = new JTextArea();
		infoLogTextArea.setBackground(new Color(236, 233, 216));
		JScrollPane infoLogScrollPane = new JScrollPane(infoLogTextArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		infoLogProgressBar = new JProgressBar();
		
		inforLogPanel.add(searchLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(10, 10, 1, 5), 0, 0));
		inforLogPanel.add(searchValueLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(10, 50, 1, 5), 0, 0));
		inforLogPanel.add(processedLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(1, 10, 1, 5), 0, 0));
		inforLogPanel.add(processedValueLabel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(1, 50, 1, 5), 0, 0));
		inforLogPanel.add(errorLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(1, 10, 1, 5), 0, 0));
		inforLogPanel.add(errorValueLabel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(1, 50, 1, 5), 0, 0));
		inforLogPanel.add(timeLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(1, 10, 1, 5), 0, 0));
		inforLogPanel.add(timeValueLabel, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(1, 50, 1, 5), 0, 0));
		inforLogPanel.add(infoLogScrollPane, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 10, 5, 10), 0, 0));
		inforLogPanel.add(infoLogProgressBar, new GridBagConstraints(0, 6, 2, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(3, 10, 3, 10), 0, 0));
	}

	/**
	 * 初始化常规面板
	 * @param commonPanel 
	 */
	private void initCommonPanel(JPanel commonPanel) {
		commonPanel.setLayout(new BorderLayout());
		commonPanel.setBackground(new Color(251, 251, 251));
		
		JPanel northCommonPanel = new JPanel();
		northCommonPanel.setOpaque(false);
		northCommonPanel.setLayout(new GridBagLayout());
		
		serverLabel = new JLabel("服务器:");
		String serverIpAddress = connTree.getSelectionPath().getLastPathComponent().toString();
		serverIPLabel = new JLabel(serverIpAddress);
		databaseLabel = new JLabel("数据库:");
		databaseNameLabel = new JLabel("N/A");
		fileLabel = new JLabel("文件:");
		fileTextField = new JTextField();
		fileBroswerButton = new JButton("..");
		fileBroswerButton.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
		codeLabel = new JLabel("编码:");
		codeAutoCompleteComboBox = new JAutoCompleteComboBox(SwingConstants.getAllCharset());
		codeAutoCompleteComboBox.setSelectedItem("UTF-8");
		continueWhenErrorCheckBox = new JCheckBox("遇到错误继续", true);
		continueWhenErrorCheckBox.setOpaque(false);
		runMultiTaskCheckBox = new JCheckBox("每个运行中运行多重查询", true);
		runMultiTaskCheckBox.setOpaque(false);
		setAutoCommitCheckBox = new JCheckBox("SET AUTOCOMMIT=0", true);
		setAutoCommitCheckBox.setOpaque(false);
		
		northCommonPanel.add(serverLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 10, 5, 5), 0, 0));
		northCommonPanel.add(serverIPLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 50, 5, 5), 0, 0));
		northCommonPanel.add(databaseLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 10, 5, 5), 0, 0));
		northCommonPanel.add(databaseNameLabel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 50, 5, 5), 0, 0));
		northCommonPanel.add(fileLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 10, 5, 5), 0, 0));
		northCommonPanel.add(fileTextField, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 50, 5, 2), 0, 0));
		northCommonPanel.add(fileBroswerButton, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 2, 5, 10), 0, 0));
		northCommonPanel.add(codeLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 10, 5, 5), 0, 0));
		northCommonPanel.add(codeAutoCompleteComboBox, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 50, 5, 5), 0, 0));
		northCommonPanel.add(continueWhenErrorCheckBox, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 50, 3, 5), 0, 0));
		northCommonPanel.add(runMultiTaskCheckBox, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 50, 3, 5), 0, 0));
		northCommonPanel.add(setAutoCommitCheckBox, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 50, 3, 5), 0, 0));
		
		commonPanel.add(northCommonPanel, BorderLayout.NORTH);
	}

}















