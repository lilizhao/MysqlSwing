package com.raisecom.view.custom;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

import com.raisecom.view.tree.ConnectionNode;

/**
 * �б���Ⱦ��
 * @author lilz-2686
 *
 */
public class MyListCellRenderer extends DefaultListCellRenderer {
	
	private static final long serialVersionUID = -3249940236543127169L;

	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
		JLabel label = (JLabel)super.getListCellRendererComponent(list, 
				value, index, isSelected, cellHasFocus);
		ConnectionNode connectionNode = (ConnectionNode)value;
		label.setIcon(connectionNode.getImageIcon());
		if (isSelected) {
			setBackground(new Color(47, 106, 197));
			setForeground(new Color(255, 255, 255));
		}
		return this;
	}
}
