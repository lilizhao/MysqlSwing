package com.raisecom.util;

import javax.swing.ImageIcon;

public class ImageUtils {
	
	//连接界面连接名(关闭)图标
	public static final ImageIcon CONNECTION_NAME_NODE_IMAGE = new ImageIcon(ImageUtils.class.getResource("/image/connection/connection.png"));
	//连接界面连接名(打开)图标
	public static final ImageIcon CONNECTION_NAME_NODE_IMAGE2 = new ImageIcon(ImageUtils.class.getResource("/image/connection/connection2.png"));
	
	//数据库(关闭)图标
	public static final ImageIcon DATABASE_NODE_IMAGE = new ImageIcon(ImageUtils.class.getResource("/image/connection/database.png"));
	//数据库(打开)图标
	public static final ImageIcon DATABASE_NODE_IMAGE2 = new ImageIcon(ImageUtils.class.getResource("/image/connection/database2.png"));
	
	//表图标
	public static final ImageIcon TABLE_NODE_IMAGE = new ImageIcon(ImageUtils.class.getResource("/image/connection/table.png"));
	
	//视图图标
	public static final ImageIcon VIEW_NODE_IMAGE = new ImageIcon(ImageUtils.class.getResource("/image/connection/view.png"));
	
	//函数图标
	public static final ImageIcon FUNCTION_NODE_IMAGE = new ImageIcon(ImageUtils.class.getResource("/image/connection/function.png"));
	
	//事件图标
	public static final ImageIcon EVENT_NODE_IMAGE = new ImageIcon(ImageUtils.class.getResource("/image/connection/event.png"));
	
	//查询图标
	public static final ImageIcon SEARCH_NODE_IMAGE = new ImageIcon(ImageUtils.class.getResource("/image/connection/search.png"));
	
	//报表图标
	public static final ImageIcon REPORT_NODE_IMAGE = new ImageIcon(ImageUtils.class.getResource("/image/connection/report.png"));
	
	//备份图标
	public static final ImageIcon BACKUP_NODE_IMAGE = new ImageIcon(ImageUtils.class.getResource("/image/connection/backup.png"));
	

}












