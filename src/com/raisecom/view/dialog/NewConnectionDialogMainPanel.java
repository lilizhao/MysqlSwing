package com.raisecom.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

import com.raisecom.model.data.ConnectionDataContext;
import com.raisecom.model.data.ServiceContext;
import com.raisecom.util.BorderUtils;
import com.raisecom.util.EncryptUtils;
import com.raisecom.util.FileOrDirectoryChooser;
import com.raisecom.util.OperationUtils;
import com.raisecom.util.SwingConstants;
import com.raisecom.view.tree.ConnectionNode;

public class NewConnectionDialogMainPanel extends JPanel{
	
	private static final long serialVersionUID = -607451765977294650L;

	private NewConnectionDialog newConnectionDialog;
	private String operateType;
	
	//定义所有面板
	private JTabbedPane jTabbedPane = new JTabbedPane();
	
	private JPanel commonPanel = new JPanel();
	private JPanel advancePanel = new JPanel();
	private JPanel sslPanel = new JPanel();
	private JPanel sshPanel = new JPanel();
	private JPanel httpPanel = new JPanel();
	
	private JPanel btnPanel = new JPanel();
	
	//定义常规面板所有组件 
	private JLabel connectionNameLabel = new JLabel("  连接名:");
	private JTextField connectionNameField = new JTextField(32);
	private JLabel hostNameOrIPAddressLabel = new JLabel("  主机名或IP地址:");
	private JTextField hostNameOrIPAddressField = new JTextField("localhost",32);
	private JLabel portLabel = new JLabel("  端口:");
	private JTextField portTextField = new JTextField("3306",7);
	private JLabel userNameLabel = new JLabel("  用户名:");
	private JTextField userNameTextField = new JTextField("root",20);
	private JLabel passwordLabel = new JLabel("  密码:");
	private JTextField passwordTextField = new JTextField(20);
	private JCheckBox savePasswordCheckBox = new JCheckBox("保存密码");
	
	//定义高级面板所有组件
	private JLabel setSavePathLabel = null;
	private JTextField setSavePathField = null;
	private JButton setSavePathBroswerBtn = null; 
	
	private JLabel codeLabel = null;
	private JComboBox codeComboBox = null;
	
	private JCheckBox keepConnectionTimeCheckBox = null;
	private JTextField keepConnectionTimeTextField = null;
	
	private JCheckBox useMySQLCharSet = null;
	
	private JCheckBox useCompreCheckBox = null;
	
	private JCheckBox autoConnectCheckBox = null;
	
	private JCheckBox usePipeSocketCheckBox = null;
	private JTextField usePipeSocketTextField = null;
	

	private JCheckBox useAdvanConnCheckBox = null; 
	private JButton selectAllButton = null; 
	
	private JLabel databaseLabel = null;
	private JButton deselectAllButton = null;
	private JList allDatabaseList = null;
	
	private JButton addDatabaseToListButton = null;
	private JButton removeDatabaseToListButton = null;
	private JLabel advanceUserNameLabel = null;
	private JTextField advanceUserNameTextField = null;
	private JLabel advancePasswordLabel = null;
	private JTextField advancePasswordTextField = null;
	
	
	//定义SSL面板所有组件
	private JCheckBox useSSLCheckBox = null;
	private JCheckBox useAuthenCheckBox = null;
	private JLabel clientSideKeyLabel = null;
	private JTextField clientSideKeyTextField = null;
	private JButton clientSideKeyButton = null;
	
	private JLabel clientCertificateLabel = null;
	private JTextField clientCertificateTextField = null;
	private JButton clientCertificateButton = null;
	
	private JLabel caCertificateLabel = null;
	private JTextField caCertificateTextField = null;
	private JButton caCertificateButton = null;
	
	private JLabel assignPasswordLabel = null; 
	private JTextField assignPasswordTextField = null;
	
	
	//定义SSH面板所有组件
	private JCheckBox useSSHChannelCheckBox = null;
	private JLabel sshHostOrIPLabel = null;
	private JTextField sshHostOrIPTextField = null;
	private JLabel sshPortLabel = null;
	private JTextField sshPortTextField = null;
	private JLabel sshUseNameLabel = null;
	private JTextField sshUseNameTextField = null;
	private JLabel authenMethodLabel = null;
	private JComboBox authenMethodComboBox = null;
	private JLabel sshPasswordLabel = null;
	private JTextField sshPasswordTextField = null;
	private JCheckBox sshSaveCheckBox = null;
	
	
	//定义HTTP面板所有组件
	private JCheckBox useHttpChannelCheckBox = null; 
	private JLabel channelAddressLabel = null;
	private JTextField channelAddressTextField = null;
	private JCheckBox useBase64CodeCheckBox = null;
	
	private JCheckBox useHttpPasswordCheckBox = null;
	private JLabel httpUserNameLabel = null;
	private JTextField httpUserNameTextField = null;
	private JLabel httpPasswordLabel = null;
	private JTextField httpPasswordTextField = null;
	private JCheckBox httpSavePasswordCheckBox = null;
	
	private JCheckBox useVerificateCheckBox = null;
	private JLabel httpClientKeyLabel = null;
	private JTextField httpClientKeyTextField = null;
	private JButton httpClientKeyButton = null;
	private JLabel httpClientCertificateLabel = null;
	private JTextField httpClientCertificateTextField = null;
	private JButton httpClientCertificateButton = null;
	private JLabel httpCaCertificateLabel = null;
	private JTextField httpCaCertificateTextField = null;
	private JButton httpCaCertificateButton = null;
	private JLabel httpPasswordShortWordLabel = null;
	private JTextField httpPasswordShortWordTextField = null;
	
	private JCheckBox httpUseProxyServerCheckBox = null;
	private JLabel httpHostLabel = null;
	private JTextField httpHostTextField = null;
	private JLabel httpPortLabel = null;
	private JTextField httpPortTextField = null;
	private JLabel httpProxyUserNameLabel = null;
	private JTextField httpProxyUserNameTextField = null;
	private JLabel httpProxyPasswordLabel = null;
	private JTextField httpProxyPasswordTextField = null;
	private JCheckBox httpProxySavePasswordCheckBox = null;
	
	
	//定义连接测试面板所有组件
	private JButton connectionTestBtn = new JButton("      连接测试      ");
	private JButton saveBtn = new JButton("  确定  ");
	private JButton cancelBtn = new JButton("  取消  ");
	

	/**
	 * 构造方法 1
	 * @param newConnectionDialog
	 */
	public NewConnectionDialogMainPanel(NewConnectionDialog newConnectionDialog) {
		this.newConnectionDialog = newConnectionDialog;
		
		setLayout(new BorderLayout());
		
		initJTabbedPane();
		initBtnPanel();
		initListeners();
		
		add(jTabbedPane, BorderLayout.CENTER);
		add(btnPanel, BorderLayout.SOUTH);
	}

	/**
	 * 构造方法 2
	 * @param newConnectionDialog
	 * @param connectionNode
	 */
	public NewConnectionDialogMainPanel(NewConnectionDialog newConnectionDialog, ConnectionNode connectionNode, String operateType) {
		this.newConnectionDialog = newConnectionDialog;
		this.operateType = operateType;
		
		setLayout(new BorderLayout());
		
		initJTabbedPane();
		initBtnPanel();
		initListeners();
		
		add(jTabbedPane, BorderLayout.CENTER);
		add(btnPanel, BorderLayout.SOUTH);
		
		initComponentValue(connectionNode);
	}

	/**
	 * 初始化组件的具体数值,紧紧在复制连接和连接属性的时候使用
	 * @param connectionNode 
	 */
	private void initComponentValue(ConnectionNode connectionNode) {
		ConnectionDataContext connectionDataContext = new ConnectionDataContext();
		ServiceContext serviceContextByConnectionName = connectionDataContext.getServiceContextByConnectionName(connectionNode.toString());
		
		if(OperationUtils.COPY.equals(operateType)){
			connectionNameField.setText(connectionNode.toString() + "_copy");
		}else if(OperationUtils.VIEW.equals(operateType)){
			connectionNameField.setText(connectionNode.toString());
		}
		hostNameOrIPAddressField.setText(serviceContextByConnectionName.getHostOrIp());
		portTextField.setText(serviceContextByConnectionName.getPort());
		userNameTextField.setText(serviceContextByConnectionName.getUserName());
		passwordTextField.setText(serviceContextByConnectionName.getPassword());
	}

	/**
	 * 功能描述：初始化JTabbedPane面板
	 */
	private void initJTabbedPane() {
		initCommonPanel();
		initAdvancePanel();
		initSSLPanel();
		initSSHPanel();
		initHttpPanel();
		
		jTabbedPane.add("常规   ", commonPanel);
		jTabbedPane.add("高级   ", advancePanel);
		jTabbedPane.add("SSL   ", sslPanel);
		jTabbedPane.add("SSH   ", sshPanel);
		jTabbedPane.add("HTTP  ", httpPanel);
		
		jTabbedPane.setBorder(new EmptyBorder(5, 5, 0, 5));
	}
	

	/**
	 * 功能描述：初始化常规面板
	 */
	private void initCommonPanel() {
		Color color = new Color(248,248,248);
		EmptyBorder emptyBorder = new EmptyBorder(1, 1, 1, 1);
		
		commonPanel.setLayout(new GridLayout(2, 1));
		
		
		JPanel firstHalfPanel = new JPanel();
		JPanel secondHalfPanel = new JPanel();
		
		firstHalfPanel.setLayout(new GridLayout(7,1));
		
		JPanel connectionNamePanel = new JPanel();
		connectionNamePanel.setBorder(emptyBorder);
		connectionNamePanel.setLayout(new GridLayout(1, 2));
		connectionNamePanel.add(connectionNameLabel);
		
		JPanel connectionNameFieldPanel = new JPanel();
		connectionNameFieldPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		connectionNameFieldPanel.add(connectionNameField);
		
		connectionNamePanel.add(connectionNameFieldPanel);
		firstHalfPanel.add(connectionNamePanel);
		
		JPanel nullPanel = new JPanel();
		nullPanel.setBorder(emptyBorder);
		nullPanel.setLayout(new GridLayout(1, 2));
		nullPanel.add(new JLabel(""));
		nullPanel.add(new JLabel());
		firstHalfPanel.add(nullPanel);
		
		JPanel hostNameOrIPAddressPanel = new JPanel();
		hostNameOrIPAddressPanel.setBorder(emptyBorder);
		hostNameOrIPAddressPanel.setLayout(new GridLayout(1, 2));
		hostNameOrIPAddressPanel.add(hostNameOrIPAddressLabel);
		
		JPanel hostNameOrIPAddressFieldPanel = new JPanel();
		hostNameOrIPAddressFieldPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		hostNameOrIPAddressFieldPanel.add(hostNameOrIPAddressField);
		
		hostNameOrIPAddressPanel.add(hostNameOrIPAddressFieldPanel);
		firstHalfPanel.add(hostNameOrIPAddressPanel);
		
		JPanel portPanel = new JPanel();
		portPanel.setBorder(emptyBorder);
		portPanel.setLayout(new GridLayout(1, 2));
		portPanel.add(portLabel);
		
		JPanel portTextFieldPanel = new JPanel();
		portTextFieldPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		portTextFieldPanel.add(portTextField);
		
		portPanel.add(portTextFieldPanel);
		firstHalfPanel.add(portPanel);
		
		JPanel userNamePanel = new JPanel();
		userNamePanel.setBorder(emptyBorder);
		userNamePanel.setLayout(new GridLayout(1, 2));
		userNamePanel.add(userNameLabel);
		
		JPanel userNameTextFieldPanel = new JPanel();
		userNameTextFieldPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		userNameTextFieldPanel.add(userNameTextField);
		
		userNamePanel.add(userNameTextFieldPanel);
		firstHalfPanel.add(userNamePanel);
		
		JPanel passwordPanel = new JPanel();
		passwordPanel.setBorder(emptyBorder);
		passwordPanel.setLayout(new GridLayout(1, 2));
		passwordPanel.add(passwordLabel);
		
		JPanel passwordTextFieldPanel = new JPanel();
		passwordTextFieldPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		passwordTextFieldPanel.add(passwordTextField);
		
		passwordPanel.add(passwordTextFieldPanel);
		firstHalfPanel.add(passwordPanel);
		
		JPanel savePasswordPanel = new JPanel();
		savePasswordPanel.setBorder(emptyBorder);
		savePasswordPanel.setLayout(new GridLayout(1, 2));
		savePasswordPanel.add(new JLabel());
		savePasswordCheckBox.setSelected(true);
		savePasswordPanel.add(savePasswordCheckBox);
		firstHalfPanel.add(savePasswordPanel);
		
		commonPanel.add(firstHalfPanel);
		
		commonPanel.add(secondHalfPanel);
		
		SwingConstants.setColor(color,commonPanel);
	}

	
	/**
	 * 功能描述：初始化高级面板
	 */
	private void initAdvancePanel() {
		advancePanel.setLayout(new BorderLayout());
		
		
		JPanel primaryConnectionPanel = new JPanel();
		JPanel advanceConnectionPanel = new JPanel();
		
		//添加初级连接属性
		primaryConnectionPanel.setLayout(new GridBagLayout());
		
		setSavePathLabel = new JLabel("设置保存路径:");
		setSavePathField = new JTextField(System.getProperty("user.dir")+"\\config");
		setSavePathBroswerBtn = new JButton("..");
		
		codeLabel = new JLabel("编码:");
		codeComboBox = new JComboBox(SwingConstants.getAllCharset());
		codeComboBox.setSelectedItem("UTF-8");
		
		keepConnectionTimeCheckBox = new JCheckBox("保持连接间隔(秒):");
		keepConnectionTimeTextField = new JTextField("240",10);
		
		useMySQLCharSet = new JCheckBox("使用 MySQL 字符集");
		useMySQLCharSet.setSelected(true);
		useMySQLCharSet.addActionListener(new NewConnectionAction());
		
		useCompreCheckBox = new JCheckBox("使用压缩");
		
		autoConnectCheckBox = new JCheckBox("自动连接");
		
		usePipeSocketCheckBox = new JCheckBox("使用名称管道、套接字:");
		usePipeSocketTextField = new JTextField("/tmp/mysql.sock",20);
		
		setComponentEnable(false,codeLabel,codeComboBox);
		
		primaryConnectionPanel.add(setSavePathLabel,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0));
		primaryConnectionPanel.add(setSavePathField,new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		primaryConnectionPanel.add(setSavePathBroswerBtn,new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0));
		
		primaryConnectionPanel.add(codeLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0));
		primaryConnectionPanel.add(codeComboBox,new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		primaryConnectionPanel.add(keepConnectionTimeCheckBox,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 5, 5), 0, 0));
		primaryConnectionPanel.add(keepConnectionTimeTextField,new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0));
		
		primaryConnectionPanel.add(useMySQLCharSet,new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 5, 5), 0, 0));
		
		primaryConnectionPanel.add(useCompreCheckBox,new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 5, 5), 0, 0));
		
		primaryConnectionPanel.add(autoConnectCheckBox,new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 5, 5), 0, 0));
		
		primaryConnectionPanel.add(usePipeSocketCheckBox,new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 5, 5), 0, 0));
		primaryConnectionPanel.add(usePipeSocketTextField,new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0));
		
		
		//添加高级连接属性
		advanceConnectionPanel.setBorder(BorderUtils.createEmsBorder(""));
		advanceConnectionPanel.setLayout(new GridBagLayout());
		
		
		useAdvanConnCheckBox = new JCheckBox("使用高级连接");
		
		useAdvanConnCheckBox.addActionListener(new NewConnectionAction());
		
		selectAllButton = new JButton("全选");
		
		databaseLabel = new JLabel("数据库:");
		deselectAllButton = new JButton("全部取消选择");
		
		allDatabaseList = new JList();
		allDatabaseList.setBorder(BorderFactory.createEtchedBorder());
		
		addDatabaseToListButton = new JButton("添加数据库到列表");
		removeDatabaseToListButton = new JButton("移除数据库到列表");
		advanceUserNameLabel = new JLabel("用户名:");
		advanceUserNameTextField = new JTextField();
		advancePasswordLabel = new JLabel("密码:");
		advancePasswordTextField = new JTextField();
		
		setComponentEnable(false,selectAllButton,databaseLabel,deselectAllButton,allDatabaseList,addDatabaseToListButton,
				removeDatabaseToListButton,advanceUserNameLabel,advanceUserNameTextField,advancePasswordLabel,advancePasswordTextField);
		
		
		advanceConnectionPanel.add(useAdvanConnCheckBox,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(5, 0, 0, 5), 0, 0));
		advanceConnectionPanel.add(selectAllButton,new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, 
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, 
				new Insets(5, 5, 0, 5), 0, 0));
		
		advanceConnectionPanel.add(databaseLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(5, 5, 0, 5), 0, 0));
		advanceConnectionPanel.add(deselectAllButton,new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, 
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, 
				new Insets(5, 5, 0, 5), 0, 0));
		
		advanceConnectionPanel.add(allDatabaseList,new GridBagConstraints(0, 2, 1, 6, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, 
				new Insets(5, 5, 5, 5), 0, 0));
		
		advanceConnectionPanel.add(addDatabaseToListButton,new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		advanceConnectionPanel.add(removeDatabaseToListButton,new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, 
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, 
				new Insets(5, 5, 0, 5), 0, 0));
		advanceConnectionPanel.add(advanceUserNameLabel,new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, 
				new Insets(5, 5, 0, 5), 0, 0));
		advanceConnectionPanel.add(advanceUserNameTextField,new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, 
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		advanceConnectionPanel.add(advancePasswordLabel,new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		advanceConnectionPanel.add(advancePasswordTextField,new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		
		
		advancePanel.add(primaryConnectionPanel,BorderLayout.NORTH);
		advancePanel.add(advanceConnectionPanel,BorderLayout.CENTER);
		
		SwingConstants.setColor(new Color(248,248,248),advancePanel);
		
		
	}
	
	/**
	 * 功能描述：初始化SSL面板
	 */
	private void initSSLPanel() {
		sslPanel.setLayout(new GridBagLayout());
		
		useSSLCheckBox = new JCheckBox("使用SSL");
		useSSLCheckBox.addActionListener(new NewConnectionAction());
		
		JPanel verificationPanel = new JPanel();
		verificationPanel.setBorder(BorderUtils.createEmsBorder("验证"));
		verificationPanel.setLayout(new GridBagLayout());
		
		JPanel nullPanel = new JPanel(); 
		
		
		useAuthenCheckBox = new JCheckBox("使用验证");
		useAuthenCheckBox.addActionListener(new NewConnectionAction());
		clientSideKeyLabel = new JLabel("客户端密匙:");
		clientSideKeyTextField = new JTextField();
		clientSideKeyButton = new JButton("..");
		
		clientCertificateLabel = new JLabel("客户端证书:");
		clientCertificateTextField = new JTextField();
		clientCertificateButton = new JButton("..");
		
		caCertificateLabel = new JLabel("CA 证书:");
		caCertificateTextField = new JTextField();
		caCertificateButton = new JButton("..");
		
		assignPasswordLabel = new JLabel("指定密码检索表:");
		assignPasswordTextField = new JTextField();
		
//		setComponentEnable(false,useAuthenCheckBox,clientSideKeyLabel,clientSideKeyTextField,
//				clientSideKeyButton,clientCertificateLabel,clientCertificateTextField,clientCertificateButton,
//				caCertificateLabel,caCertificateTextField,caCertificateButton,
//				assignPasswordLabel,assignPasswordTextField);

		
		verificationPanel.add(useAuthenCheckBox,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		verificationPanel.add(clientSideKeyLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(5, 5, 0, 5), 0, 0));
		verificationPanel.add(clientSideKeyTextField,new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 
				new Insets(5, 5, 0, 5), 0, 0));
		verificationPanel.add(clientSideKeyButton,new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		
		verificationPanel.add(clientCertificateLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		verificationPanel.add(clientCertificateTextField,new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		verificationPanel.add(clientCertificateButton,new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		
		
		verificationPanel.add(caCertificateLabel,new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		verificationPanel.add(caCertificateTextField,new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		verificationPanel.add(caCertificateButton,new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, 
				new Insets(5, 5, 0, 5), 0, 0));
		
		verificationPanel.add(assignPasswordLabel,new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5), 0, 0));
		verificationPanel.add(assignPasswordTextField,new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 
				new Insets(5, 5, 5, 5), 0, 0));
		
		setComponentEnable(false, verificationPanel);
		
		sslPanel.add(useSSLCheckBox,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(5, 5, 0, 5), 0, 0));
		sslPanel.add(verificationPanel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		sslPanel.add(nullPanel,new GridBagConstraints(0, 2, 1, 1, 1.0,1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(5, 5, 0, 5), 0, 0));
		
		
		SwingConstants.setColor(new Color(248,248,248),sslPanel);
		
	}
	
	/**
	 * 功能描述：初始化SSHPanel面板
	 */
	private void initSSHPanel() {
		sshPanel.setLayout(new FlowLayout());
		
		JPanel useSSHPanel = new JPanel();
		
		
		useSSHPanel.setLayout(new GridBagLayout());
		
		useSSHChannelCheckBox = new JCheckBox("使用 SSH 通道");
		useSSHChannelCheckBox.addActionListener(new NewConnectionAction());
		sshHostOrIPLabel = new JLabel("主机名或 IP 地址:");
		sshHostOrIPTextField = new JTextField(50);
		sshPortLabel = new JLabel("端口:");
		sshPortTextField = new JTextField("22",7);
		sshUseNameLabel = new JLabel("用户名:");
		sshUseNameTextField = new JTextField(25);
		
		authenMethodLabel = new JLabel("验证方法");
		
		Vector<String> authenMethodVector = new Vector<String>();
		authenMethodVector.add("密码");
		authenMethodVector.add("公钥");
		authenMethodComboBox = new JComboBox(authenMethodVector);
		authenMethodComboBox.setPrototypeDisplayValue("xxxxxxxxxxxxxxxxxxxxxx");
		
		sshPasswordLabel = new JLabel("密码:");
		sshPasswordTextField = new JTextField(25);
		sshSaveCheckBox = new JCheckBox("保存密码"); 
		
		setComponentEnable(false,sshHostOrIPLabel,sshHostOrIPTextField,sshPortLabel,sshPortTextField,
				sshUseNameLabel,sshUseNameTextField,authenMethodLabel,authenMethodComboBox,
				sshPasswordLabel,sshPasswordTextField,sshSaveCheckBox);
		
		useSSHPanel.add(useSSHChannelCheckBox,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		useSSHPanel.add(sshHostOrIPLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(5, 5, 0, 5), 0, 0));
		useSSHPanel.add(sshHostOrIPTextField,new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		useSSHPanel.add(sshPortLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		useSSHPanel.add(sshPortTextField,new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(5, 5, 0, 5), 0, 0));
		useSSHPanel.add(sshUseNameLabel,new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		useSSHPanel.add(sshUseNameTextField,new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		useSSHPanel.add(authenMethodLabel,new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		useSSHPanel.add(authenMethodComboBox,new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		useSSHPanel.add(sshPasswordLabel,new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		useSSHPanel.add(sshPasswordTextField,new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		useSSHPanel.add(sshSaveCheckBox,new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, 
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 0, 5), 0, 0));
		
		
		sshPanel.add(useSSHPanel);
		
		SwingConstants.setColor(new Color(248,248,248), sshPanel);
	}
	
	/**
	 * 功能描述：初始化Http组件面板
	 */
	private void initHttpPanel() {
		httpPanel.setLayout(new BorderLayout());
		
		JPanel httpChannelPanel = new JPanel();
		JTabbedPane httpVerificatePanel = new JTabbedPane();
		
		httpChannelPanel.setLayout(new GridBagLayout());
		
		useHttpChannelCheckBox = new JCheckBox("使用 HTTP 通道");
		useHttpChannelCheckBox.addActionListener(new NewConnectionAction());
		channelAddressLabel = new JLabel("通道地址:");
		channelAddressTextField = new JTextField();
		useBase64CodeCheckBox = new JCheckBox("使用 base64 编码传出查询");
		
		setComponentEnable(false, channelAddressLabel,channelAddressTextField,useBase64CodeCheckBox);
		
		
		httpChannelPanel.add(useHttpChannelCheckBox,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 0, 5), 0, 0));
		httpChannelPanel.add(channelAddressLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		httpChannelPanel.add(channelAddressTextField,new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 
				new Insets(5, 5, 0, 25), 0, 0));
		httpChannelPanel.add(useBase64CodeCheckBox,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 0, 5), 0, 0));
		
		JPanel verificatePanel = new JPanel();
		JPanel proxyServerPanel = new JPanel();
		
		httpVerificatePanel.add("验证", verificatePanel);
		httpVerificatePanel.add("代理服务器", proxyServerPanel);
		
		
//		verificatePanel.setLayout(new FlowLayout());
//		
//		JPanel httpPasswordPanel = new JPanel();
//		httpPasswordPanel.setBorder(BorderUtils.createEmsBorder("密码"));
//		JPanel httpCertificatePanel = new JPanel();
//		httpCertificatePanel.setBorder(BorderUtils.createEmsBorder("证书"));
//		
//		verificatePanel.add(httpPasswordPanel,BorderLayout.NORTH);
//		verificatePanel.add(httpCertificatePanel,BorderLayout.CENTER);
//		
//		httpPasswordPanel.setLayout(new GridBagLayout());
//		
//		useHttpPasswordCheckBox = new JCheckBox("使用密码验证");
//		httpUserNameLabel = new JLabel("用户名:");
//		httpUserNameTextField = new JTextField(30);
//		
//		httpPasswordPanel.add(useHttpPasswordCheckBox,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.NONE,
//				new Insets(5, 5, 0, 5), 0, 0));
//		httpPasswordPanel.add(httpUserNameLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.NONE,
//				new Insets(5, 5, 0, 5), 0, 0));
//		httpPasswordPanel.add(httpUserNameTextField,new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.NONE,
//				new Insets(5, 5, 0, 5), 0, 0));
		
		verificatePanel.setLayout(new GridBagLayout());
		
		verificatePanel.setBorder(BorderUtils.createEmsBorder("密码-证书"));
		
		useHttpPasswordCheckBox = new JCheckBox("使用密码验证");
		useHttpPasswordCheckBox.addActionListener(new NewConnectionAction());
		httpUserNameLabel = new JLabel("用户名:");
		httpUserNameTextField = new JTextField(30);
		httpPasswordLabel = new JLabel("密码:");
		httpPasswordTextField = new JTextField(30);
		httpSavePasswordCheckBox = new JCheckBox("保存密码");
		
		useVerificateCheckBox = new JCheckBox("使用证书验证");
		useVerificateCheckBox.addActionListener(new NewConnectionAction());
		httpClientKeyLabel = new JLabel("客户端密匙:");
		httpClientKeyTextField = new JTextField();
		httpClientKeyButton = new JButton("..");
		httpClientCertificateLabel = new JLabel("客户端证书:");
		httpClientCertificateTextField = new JTextField();
		httpClientCertificateButton = new JButton("..");
		httpCaCertificateLabel = new JLabel("CA 证书:");
		httpCaCertificateTextField = new JTextField();
		httpCaCertificateButton = new JButton("..");
		httpPasswordShortWordLabel = new JLabel("密码短语:");
		httpPasswordShortWordTextField = new JTextField();
		
//		setComponentEnable(false,useHttpPasswordCheckBox,httpUserNameLabel,httpUserNameTextField,
//				httpPasswordLabel,httpPasswordTextField,httpSavePasswordCheckBox,useVerificateCheckBox,
//				httpClientKeyLabel,httpClientKeyTextField,httpClientKeyButton,httpClientCertificateLabel,
//				httpClientCertificateTextField,httpClientCertificateButton,httpCaCertificateLabel,
//				httpCaCertificateTextField,httpCaCertificateButton,httpPasswordShortWordLabel,
//				httpPasswordShortWordTextField);
		
		verificatePanel.add(useHttpPasswordCheckBox,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 0, 5), 0, 0));
		verificatePanel.add(httpUserNameLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		verificatePanel.add(httpUserNameTextField,new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		verificatePanel.add(httpPasswordLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		verificatePanel.add(httpPasswordTextField,new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		verificatePanel.add(httpSavePasswordCheckBox,new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 0, 5), 0, 0));
		
		verificatePanel.add(useVerificateCheckBox,new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(5, 0, 5, 5), 0, 0));
		verificatePanel.add(httpClientKeyLabel,new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		verificatePanel.add(httpClientKeyTextField,new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		verificatePanel.add(httpClientKeyButton,new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 10), 0, 0));
		verificatePanel.add(httpClientCertificateLabel,new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		verificatePanel.add(httpClientCertificateTextField,new GridBagConstraints(1, 6, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		verificatePanel.add(httpClientCertificateButton,new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 10), 0, 0));
		verificatePanel.add(httpCaCertificateLabel,new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		verificatePanel.add(httpCaCertificateTextField,new GridBagConstraints(1, 7, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		verificatePanel.add(httpCaCertificateButton,new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 10), 0, 0));
		verificatePanel.add(httpPasswordShortWordLabel,new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		verificatePanel.add(httpPasswordShortWordTextField,new GridBagConstraints(1, 8, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0));
		
		setComponentEnable(false, verificatePanel);
		
		proxyServerPanel.setLayout(new FlowLayout());
		
		JPanel proxyServerCenterPanel = new JPanel();
		
		proxyServerCenterPanel.setLayout(new GridBagLayout());
		
		
		httpUseProxyServerCheckBox = new JCheckBox("代理服务器:");
		httpUseProxyServerCheckBox.addActionListener(new NewConnectionAction());
		httpHostLabel = new JLabel("主机:");
		httpHostTextField = new JTextField(50);
		httpPortLabel = new JLabel("端口:");
		httpPortTextField = new JTextField(7);
		httpProxyUserNameLabel = new JLabel("用户名:");
		httpProxyUserNameTextField = new JTextField(35);
		httpProxyPasswordLabel = new JLabel("密码:");
		httpProxyPasswordTextField = new JTextField(35);
		httpProxySavePasswordCheckBox = new JCheckBox("保存密码");
		
//		setComponentEnable(false,httpUseProxyServerCheckBox,);
		
		proxyServerCenterPanel.add(httpUseProxyServerCheckBox,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 0, 5), 0, 0));
		proxyServerCenterPanel.add(httpHostLabel,new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		proxyServerCenterPanel.add(httpHostTextField,new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 15), 0, 0));
		proxyServerCenterPanel.add(httpPortLabel,new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, 
				new Insets(5, 5, 0, 5), 0, 0));
		proxyServerCenterPanel.add(httpPortTextField,new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		proxyServerCenterPanel.add(httpProxyUserNameLabel,new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		proxyServerCenterPanel.add(httpProxyUserNameTextField,new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		proxyServerCenterPanel.add(httpProxyPasswordLabel,new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		proxyServerCenterPanel.add(httpProxyPasswordTextField,new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 5, 0, 5), 0, 0));
		proxyServerCenterPanel.add(httpProxySavePasswordCheckBox,new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 0, 5), 0, 0));
		
		setComponentEnable(false, proxyServerCenterPanel);
		
		proxyServerPanel.add(proxyServerCenterPanel);
		
		
		httpPanel.add(httpChannelPanel,BorderLayout.NORTH);
		httpPanel.add(httpVerificatePanel,BorderLayout.CENTER);
		
		
		SwingConstants.setColor(new Color(248,248,248), httpPanel);
	}

	/**
	 * 功能描述：初始化BtnPanel面板
	 */
	private void initBtnPanel(){
		btnPanel.setLayout(new GridLayout(1, 2));
		
		JPanel connectTestPanel = new JPanel();
		connectTestPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		connectTestPanel.add(connectionTestBtn);
		btnPanel.add(connectTestPanel);
		
		JPanel savePanel = new JPanel();
		savePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		savePanel.add(saveBtn);
		
		savePanel.add(cancelBtn);
		btnPanel.add(savePanel);
		
		btnPanel.setBorder(new EmptyBorder(3, 5, 3, 5));
	}
	
	/**
	 * 为组件设置使能动作
	 * @param flag
	 * @param components
	 */
	private void setComponentEnable(boolean flag,Component...components){
		for(Component component:components){
			component.setEnabled(flag);
		}
	}
	
	/**
	 * 为组件设置使能动作
	 * @param flag
	 * @param components
	 */
	private void setComponentEnable(boolean flag,JPanel panel){
		Component[] components = panel.getComponents();
		for(Component component:components){
			component.setEnabled(flag);
		}
	}
	
	/**
	 * 初始化组件监听
	 */
	private void initListeners() {
		setSavePathBroswerBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FileOrDirectoryChooser folderChooser = new FileOrDirectoryChooser(setSavePathField.getText(),
						JFileChooser.DIRECTORIES_ONLY,setSavePathField);
				folderChooser.showOpenDialog(newConnectionDialog);
			}
		});
		//新建连接SSL界面客户端密匙浏览按钮功能
		clientSideKeyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				FileOrDirectoryChooser fileChooser = new FileOrDirectoryChooser(clientSideKeyTextField.getText(),
						JFileChooser.FILES_ONLY,clientSideKeyTextField);
				fileChooser.setAcceptAllFileFilterUsed(false);
				fileChooser.showOpenDialog(newConnectionDialog);
			}
		});
		//新建连接SSL界面客户端证书浏览按钮功能
		clientCertificateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				FileOrDirectoryChooser fileChooser = new FileOrDirectoryChooser(clientCertificateTextField.getText(),
						JFileChooser.FILES_ONLY,clientCertificateTextField);
				fileChooser.setAcceptAllFileFilterUsed(false);
				fileChooser.showOpenDialog(newConnectionDialog);
			}
		});
		//新建连接SSL界面CA证书浏览按钮功能
		caCertificateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				FileOrDirectoryChooser fileChooser = new FileOrDirectoryChooser(caCertificateTextField.getText(),
						JFileChooser.FILES_ONLY,caCertificateTextField);
				fileChooser.setAcceptAllFileFilterUsed(false);
				fileChooser.showOpenDialog(newConnectionDialog);
			}
		});
		//新建连接Http界面客户端密匙浏览功能
		httpClientKeyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				FileOrDirectoryChooser fileChooser = new FileOrDirectoryChooser(httpClientKeyTextField.getText(),
						JFileChooser.FILES_ONLY,httpClientKeyTextField);
				fileChooser.setAcceptAllFileFilterUsed(false);
				fileChooser.showOpenDialog(newConnectionDialog);
			}
		});
		//新建连接Http界面客户端证书浏览功能
		httpClientCertificateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				FileOrDirectoryChooser fileChooser = new FileOrDirectoryChooser(httpClientCertificateTextField.getText(),
						JFileChooser.FILES_ONLY,httpClientCertificateTextField);
				fileChooser.setAcceptAllFileFilterUsed(false);
				fileChooser.showOpenDialog(newConnectionDialog);
			}
		});
		//新建连接Http界面CA证书浏览功能
		httpCaCertificateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FileOrDirectoryChooser fileChooser = new FileOrDirectoryChooser(httpCaCertificateTextField.getText(),
						JFileChooser.FILES_ONLY,httpCaCertificateTextField);
				fileChooser.setAcceptAllFileFilterUsed(false);
				fileChooser.showOpenDialog(newConnectionDialog);
			}
		});
		saveBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					SAXBuilder builder = new SAXBuilder();
					
					String filePath = System.getProperty("user.dir")+"/config/connections.ncx";
					FileInputStream fis = new FileInputStream(filePath);
					Document document = builder.build(fis);
					Element rootElement = document.getRootElement();
					
					//如果连接名为空，则使用主机名  + 端口号的形式
					String newConnectionName = connectionNameField.getText().trim();
					if("".equals(newConnectionName)){
						newConnectionName = hostNameOrIPAddressField.getText().trim() + "_" + portTextField.getText().trim();
						connectionNameField.setText(newConnectionName);
					}
					
					Element newConnectionNode = (Element) XPath.selectSingleNode(rootElement, "/Connections/Connection[@ConnectionName='"+newConnectionName+"']");
					
					//只能创建新的连接
					if(newConnectionNode!=null){
						if(operateType==null){
							JOptionPane.showMessageDialog(newConnectionDialog,"连接名 "+newConnectionName+" 已经存在 (其他 Navicat 产品)。" +
									"请指定其他连接名。",
									"",JOptionPane.INFORMATION_MESSAGE);
						}else{
							try {
								rootElement.removeContent(newConnectionNode);
								
								Element newConnectionEle = new Element("Connection");
								newConnectionEle.setAttribute("ConnectionName",newConnectionName);
								
								List<Attribute> allNewAttributeForElement = getAllNewAttributeForElement();
								addNewAttributeForElement(newConnectionEle,allNewAttributeForElement);
								
								rootElement.addContent(newConnectionEle);
								FileOutputStream fos = new FileOutputStream(
										filePath);
								XMLOutputter output = new XMLOutputter();
								Format f = Format.getPrettyFormat();
								output.setFormat(f);
								output.output(document, fos);
								fos.close();
							} catch (Exception e2) {
								e2.printStackTrace();
							} finally{
								newConnectionDialog.dispose();
							}
						}
					}else{
						try {
							Element newConnectionEle = new Element("Connection");
							newConnectionEle.setAttribute("ConnectionName",newConnectionName);
							
							List<Attribute> allNewAttributeForElement = getAllNewAttributeForElement();
							addNewAttributeForElement(newConnectionEle,allNewAttributeForElement);
							
							rootElement.addContent(newConnectionEle);
							FileOutputStream fos = new FileOutputStream(
									filePath);
							XMLOutputter output = new XMLOutputter();
							Format f = Format.getPrettyFormat();
							output.setFormat(f);
							output.output(document, fos);
							fos.close();
						} catch (Exception e2) {
							e2.printStackTrace();
						} finally{
							newConnectionDialog.dispose();
						}
					}
				    fis.close();
				} catch (JDOMException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

		});
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				newConnectionDialog.dispose();
			}
		});
	}
	
	/**
	 * 获取需要添加的属性
	 */
	private List<Attribute> getAllNewAttributeForElement() {
		List<Attribute> attributes = new ArrayList<Attribute>();
		
		attributes.add(new Attribute("ConnType", "MYSQL"));
		attributes.add(new Attribute("OraConnType", "BASIC"));
		attributes.add(new Attribute("Host", hostNameOrIPAddressField.getText().trim()));
		attributes.add(new Attribute("Port", portTextField.getText().trim()));
		attributes.add(new Attribute("Database", ""));
		attributes.add(new Attribute("OraServiceNameType", "SERVICENAME"));
		attributes.add(new Attribute("TNS", ""));
		attributes.add(new Attribute("MSSQLAuthenMode", "SQLSERVER"));
		attributes.add(new Attribute("DatabaseFileName", ""));
		attributes.add(new Attribute("UserName", userNameTextField.getText().trim()));
//		attributes.add(new Attribute("Password", EncryptUtils.encodeBase64String(passwordTextField.getText().trim())));
		attributes.add(new Attribute("Password", passwordTextField.getText()));
		attributes.add(new Attribute("SavePassword", String.valueOf(savePasswordCheckBox.isSelected())));
		attributes.add(new Attribute("SettingsSavePath", setSavePathField.getText().trim()));
		attributes.add(new Attribute("Encoding", (String) codeComboBox.getSelectedItem()));
		attributes.add(new Attribute("Keepalive", "false"));
		attributes.add(new Attribute("KeepaliveInterval", keepConnectionTimeTextField.getText().trim()));
		attributes.add(new Attribute("MySQLCharacterSet", String.valueOf(useMySQLCharSet.isSelected())));
		attributes.add(new Attribute("Compression", String.valueOf(useCompreCheckBox.isSelected())));
		attributes.add(new Attribute("AutoConnect", String.valueOf(autoConnectCheckBox.isSelected())));
		attributes.add(new Attribute("NamedPipe", String.valueOf(usePipeSocketCheckBox.isSelected())));
		attributes.add(new Attribute("NamedPipeSocket", usePipeSocketTextField.getText().trim()));
		attributes.add(new Attribute("OraRole", "DEFAULT"));
		attributes.add(new Attribute("OraOSAuthen", "false"));
		attributes.add(new Attribute("SQLiteEncryption", "false"));
		attributes.add(new Attribute("MSSQLEncryption", "false"));
		attributes.add(new Attribute("UseAdvanced", String.valueOf(useAdvanConnCheckBox.isSelected())));
		attributes.add(new Attribute("SSL", String.valueOf(useSSLCheckBox.isSelected())));
		attributes.add(new Attribute("SSL_Authen", String.valueOf(useAuthenCheckBox.isSelected())));
		attributes.add(new Attribute("SSL_PGSSLMode", "REQUIRE"));
		attributes.add(new Attribute("SSL_ClientKey", clientSideKeyTextField.getText().trim()));
		attributes.add(new Attribute("SSL_ClientCert", clientCertificateTextField.getText().trim()));
		attributes.add(new Attribute("SSL_CACert", caCertificateTextField.getText().trim()));
		attributes.add(new Attribute("SSL_Clpher", assignPasswordTextField.getText().trim()));
		attributes.add(new Attribute("SSL_PGSSLCRL", ""));
		attributes.add(new Attribute("SSH", String.valueOf(useSSHChannelCheckBox.isSelected())));
		attributes.add(new Attribute("SSH_Host", sshHostOrIPTextField.getText().trim()));
		attributes.add(new Attribute("SSH_Port", sshPortTextField.getText().trim()));
		attributes.add(new Attribute("SSH_UserName", sshUseNameTextField.getText().trim()));
		attributes.add(new Attribute("SSH_AuthenMethod", (String) authenMethodComboBox.getSelectedItem()));
		attributes.add(new Attribute("SSH_Password", sshPasswordTextField.getText().trim()));
		attributes.add(new Attribute("SSH_SavePassword", String.valueOf(sshSaveCheckBox.isSelected())));
		attributes.add(new Attribute("SSH_PrivateKey", ""));
		attributes.add(new Attribute("HTTP", String.valueOf(useHttpChannelCheckBox.isSelected())));
		attributes.add(new Attribute("HTTP_URL", channelAddressTextField.getText().trim()));
		attributes.add(new Attribute("HTTP_EQ", "false"));
		attributes.add(new Attribute("HTTP_PA", String.valueOf(useHttpPasswordCheckBox.isSelected())));
		attributes.add(new Attribute("HTTP_PA_UserName", httpUserNameTextField.getText().trim()));
		attributes.add(new Attribute("HTTP_PA_Password", httpPasswordTextField.getText().trim()));
		attributes.add(new Attribute("HTTP_PA_SavePassword", String.valueOf(httpSavePasswordCheckBox.isSelected())));
		attributes.add(new Attribute("HTTP_CA", String.valueOf(useVerificateCheckBox.isSelected())));
		attributes.add(new Attribute("HTTP_CA_ClientKey", httpClientKeyTextField.getText().trim()));
		attributes.add(new Attribute("HTTP_CA_ClientCert", httpClientCertificateTextField.getText().trim()));
		attributes.add(new Attribute("HTTP_CA_CACert", httpCaCertificateTextField.getText().trim()));
		attributes.add(new Attribute("HTTP_CA_Passphrase", httpPasswordShortWordTextField.getText().trim()));
		attributes.add(new Attribute("HTTP_Proxy", String.valueOf(httpUseProxyServerCheckBox.isSelected())));
		attributes.add(new Attribute("HTTP_Proxy_Host", httpHostTextField.getText().trim()));
		attributes.add(new Attribute("HTTP_Proxy_Port", httpPortTextField.getText().trim()));
		attributes.add(new Attribute("HTTP_Proxy_UserName", httpProxyUserNameTextField.getText().trim()));
		attributes.add(new Attribute("HTTP_Proxy_Password", httpProxyPasswordTextField.getText().trim()));
		attributes.add(new Attribute("HTTP_Proxy_SavePassword", String.valueOf(httpProxySavePasswordCheckBox.isSelected())));
		
		return attributes;
	}
	
	/**
	 * 为Jdom中元素添加属性
	 * @param newConnectionEle
	 * @param allNewAttributeForElement 
	 * @return 
	 */
	private void addNewAttributeForElement(Element newConnectionEle, List<Attribute> allNewAttributes) {
		for(Attribute attribute:allNewAttributes){
			newConnectionEle.setAttribute(attribute);
		}
	}
	
	
	private final class NewConnectionAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getSource()==useMySQLCharSet){
				if(useMySQLCharSet.isSelected()){
					codeLabel.setEnabled(false); 
					codeComboBox.setEnabled(false);
				}else{
					codeLabel.setEnabled(true); 
					codeComboBox.setEnabled(true);
				}
			}else if(e.getSource()==useSSLCheckBox){
				if(useSSLCheckBox.isSelected() && !useAuthenCheckBox.isSelected()){
					useAuthenCheckBox.setEnabled(true);
				}else if(useSSLCheckBox.isSelected() && useAuthenCheckBox.isSelected()){
					useAuthenCheckBox.setEnabled(true);
					setComponentEnable(true,clientSideKeyLabel,clientSideKeyTextField,clientSideKeyButton,
							clientCertificateLabel,clientCertificateTextField,clientCertificateButton,
							caCertificateLabel,caCertificateTextField,caCertificateButton,assignPasswordLabel,
							assignPasswordTextField);
				}else{
					useAuthenCheckBox.setEnabled(false);
					setComponentEnable(false,clientSideKeyLabel,clientSideKeyTextField,clientSideKeyButton,
							clientCertificateLabel,clientCertificateTextField,clientCertificateButton,
							caCertificateLabel,caCertificateTextField,caCertificateButton,assignPasswordLabel,
							assignPasswordTextField);
				}
			}else if(e.getSource()==useAuthenCheckBox){
				if(useAuthenCheckBox.isSelected()){
					setComponentEnable(true,clientSideKeyLabel,clientSideKeyTextField,clientSideKeyButton,
							clientCertificateLabel,clientCertificateTextField,clientCertificateButton,
							caCertificateLabel,caCertificateTextField,caCertificateButton,assignPasswordLabel,
							assignPasswordTextField);
				}else{
					setComponentEnable(false,clientSideKeyLabel,clientSideKeyTextField,clientSideKeyButton,
							clientCertificateLabel,clientCertificateTextField,clientCertificateButton,
							caCertificateLabel,caCertificateTextField,caCertificateButton,assignPasswordLabel,
							assignPasswordTextField);
				}
			}else if(e.getSource()==useSSHChannelCheckBox){
				if(useSSHChannelCheckBox.isSelected()){
					setComponentEnable(true,sshHostOrIPLabel,sshHostOrIPTextField,sshPortLabel,
							sshPortTextField,sshUseNameLabel,sshUseNameTextField,authenMethodLabel,
							authenMethodComboBox,authenMethodComboBox,sshPasswordLabel,sshPasswordTextField,
							sshSaveCheckBox);
				}else{
					setComponentEnable(false,sshHostOrIPLabel,sshHostOrIPTextField,sshPortLabel,
							sshPortTextField,sshUseNameLabel,sshUseNameTextField,authenMethodLabel,
							authenMethodComboBox,authenMethodComboBox,sshPasswordLabel,sshPasswordTextField,
							sshSaveCheckBox);
				}
			}else if(e.getSource()==useHttpChannelCheckBox){
				if(useHttpChannelCheckBox.isSelected()){
					setComponentEnable(true, channelAddressLabel,channelAddressTextField,useBase64CodeCheckBox,
							useHttpPasswordCheckBox,useVerificateCheckBox,httpUseProxyServerCheckBox);
					if(useHttpPasswordCheckBox.isSelected()){
						setComponentEnable(true, httpUserNameLabel,httpUserNameTextField,httpPasswordLabel,
								httpPasswordTextField,httpSavePasswordCheckBox);
					}
					if(useVerificateCheckBox.isSelected()){
						setComponentEnable(true, httpClientKeyLabel,httpClientKeyTextField,httpClientKeyButton,
								httpClientCertificateLabel,httpClientCertificateTextField,httpClientCertificateButton,
								httpCaCertificateLabel,httpCaCertificateTextField,httpCaCertificateButton,
								httpPasswordShortWordLabel,httpPasswordShortWordTextField);
					}
					if(httpUseProxyServerCheckBox.isSelected()){
						setComponentEnable(true, httpHostLabel,httpHostTextField,httpPortLabel,httpPortTextField,
								httpProxyUserNameLabel,httpProxyUserNameTextField,httpProxyPasswordLabel,
								httpProxyPasswordTextField,httpProxySavePasswordCheckBox);
					}
				}else{
					setComponentEnable(false, channelAddressLabel,channelAddressTextField,useBase64CodeCheckBox,
							useHttpPasswordCheckBox,useVerificateCheckBox,httpUseProxyServerCheckBox);
					
					setComponentEnable(false, httpUserNameLabel,httpUserNameTextField,httpPasswordLabel,
							httpPasswordTextField,httpSavePasswordCheckBox);
					
					setComponentEnable(false, httpClientKeyLabel,httpClientKeyTextField,httpClientKeyButton,
							httpClientCertificateLabel,httpClientCertificateTextField,httpClientCertificateButton,
							httpCaCertificateLabel,httpCaCertificateTextField,httpCaCertificateButton,
							httpPasswordShortWordLabel,httpPasswordShortWordTextField);
					setComponentEnable(false, httpHostLabel,httpHostTextField,httpPortLabel,httpPortTextField,
							httpProxyUserNameLabel,httpProxyUserNameTextField,httpProxyPasswordLabel,
							httpProxyPasswordTextField,httpProxySavePasswordCheckBox);
				}
			}else if(e.getSource()==useHttpPasswordCheckBox){
				if(useHttpPasswordCheckBox.isSelected()){
					setComponentEnable(true, httpUserNameLabel,httpUserNameTextField,httpPasswordLabel,
							httpPasswordTextField,httpSavePasswordCheckBox);
				}else{
					setComponentEnable(false, httpUserNameLabel,httpUserNameTextField,httpPasswordLabel,
							httpPasswordTextField,httpSavePasswordCheckBox);
				}
			}else if(e.getSource()==useVerificateCheckBox){
				if(useVerificateCheckBox.isSelected()){
					setComponentEnable(true, httpClientKeyLabel,httpClientKeyTextField,httpClientKeyButton,
							httpClientCertificateLabel,httpClientCertificateTextField,httpClientCertificateButton,
							httpCaCertificateLabel,httpCaCertificateTextField,httpCaCertificateButton,
							httpPasswordShortWordLabel,httpPasswordShortWordTextField);
				}else{
					setComponentEnable(false, httpClientKeyLabel,httpClientKeyTextField,httpClientKeyButton,
							httpClientCertificateLabel,httpClientCertificateTextField,httpClientCertificateButton,
							httpCaCertificateLabel,httpCaCertificateTextField,httpCaCertificateButton,
							httpPasswordShortWordLabel,httpPasswordShortWordTextField);
				}
			}else if(e.getSource()==httpUseProxyServerCheckBox){
				if(httpUseProxyServerCheckBox.isSelected()){
					setComponentEnable(true, httpHostLabel,httpHostTextField,httpPortLabel,httpPortTextField,
							httpProxyUserNameLabel,httpProxyUserNameTextField,httpProxyPasswordLabel,
							httpProxyPasswordTextField,httpProxySavePasswordCheckBox);
				}else{
					setComponentEnable(false, httpHostLabel,httpHostTextField,httpPortLabel,httpPortTextField,
							httpProxyUserNameLabel,httpProxyUserNameTextField,httpProxyPasswordLabel,
							httpProxyPasswordTextField,httpProxySavePasswordCheckBox);
				}
			}else if(e.getSource()==useAdvanConnCheckBox){
				if(useAdvanConnCheckBox.isSelected()){
					setComponentEnable(true,selectAllButton,databaseLabel,deselectAllButton,allDatabaseList,allDatabaseList,
							addDatabaseToListButton,removeDatabaseToListButton,advanceUserNameLabel,advanceUserNameTextField,
							advancePasswordLabel,advancePasswordTextField);
				}else{
					setComponentEnable(false,selectAllButton,databaseLabel,deselectAllButton,allDatabaseList,allDatabaseList,
							addDatabaseToListButton,removeDatabaseToListButton,advanceUserNameLabel,advanceUserNameTextField,
							advancePasswordLabel,advancePasswordTextField);
				}
			}
		}
	}

	
}
